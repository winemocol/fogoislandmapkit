//
//  FogoOverlay.h
//  FogoIslandMapKit
//
//  Created by sy2036 on 2017-01-23.
//  Copyright © 2017 winemocol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface FogoOverlay : NSObject <MKOverlay>

@property (nonatomic) float possValue;
@property (nonatomic) UIImage *bgImage;

- (instancetype)initWithPossibility:(float)possValue
                     andCoordinates:(CLLocationCoordinate2D)fCoordinate
                 andBoundingMapRect:(MKMapRect)fBoundingMapRect;

@end
