//
//  FogoOverlayRenderer.h
//  FogoIslandMapKit
//
//  Created by Sipan Ye on 2017/3/9.
//  Copyright © 2017年 winemocol. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface FogoOverlayRenderer : MKOverlayRenderer

@property (nonatomic) UIImage *bgImage;

- (instancetype)initWithOverlay:(id<MKOverlay>)overlay andOverlayImage:(UIImage *)overlayImage;

@end
