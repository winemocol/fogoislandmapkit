//
//  FogoOverlayRenderer.m
//  FogoIslandMapKit
//
//  Created by Sipan Ye on 2017/3/9.
//  Copyright © 2017年 winemocol. All rights reserved.
//

#import "FogoOverlayRenderer.h"

#define FOGO_NW_LAT 49.78354343767712
#define FOGO_NW_LNG -54.32582266065412
#define FOGO_SW_LAT 49.531878309117076
#define FOGO_SW_LNG -54.33958794258664
#define FOGO_NE_LAT 49.7745818637998
#define FOGO_NE_LNG -53.96249945939786
#define FOGO_SE_LAT 49.52299562709053
#define FOGO_SE_LNG -53.97812839986856

@implementation FogoOverlayRenderer

- (instancetype)initWithOverlay:(id<MKOverlay>)overlay andOverlayImage:(UIImage *)overlayImage {
    self = [super initWithOverlay:overlay];
    if (self) {
        _bgImage = overlayImage;
    }
    
    return self;
}

- (void)drawMapRect:(MKMapRect)mapRect zoomScale:(MKZoomScale)zoomScale inContext:(CGContextRef)context {
    CGImageRef imageReference = self.bgImage.CGImage;
    
    MKMapRect theMapRect = self.overlay.boundingMapRect;
    CGRect theRect = [self rectForMapRect:theMapRect];
    
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextTranslateCTM(context, 0.0, -theRect.size.height);
    
    //  cosA = (b^2 + c^2 - a^2)/2bc
    CLLocation *aLocation = [[CLLocation alloc] initWithLatitude:FOGO_NW_LAT longitude:FOGO_NW_LNG];
    CLLocation *bLocation = [[CLLocation alloc] initWithLatitude:FOGO_NW_LAT longitude:FOGO_NE_LNG];
    CLLocation *cLocation = [[CLLocation alloc] initWithLatitude:FOGO_NE_LAT longitude:FOGO_NE_LNG];
    
    double offsetAngleCos = (pow([aLocation distanceFromLocation:cLocation],2) + pow([aLocation distanceFromLocation:bLocation],2) - pow([bLocation distanceFromLocation:cLocation],2))/(2*[aLocation distanceFromLocation:cLocation]*[aLocation distanceFromLocation:bLocation]);
//    CGContextRotateCTM(context, -0.03);
    
    CGContextDrawImage(context, theRect, imageReference);
}

@end
