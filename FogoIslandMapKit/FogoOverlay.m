//
//  FogoOverlay.m
//  FogoIslandMapKit
//
//  Created by sy2036 on 2017-01-23.
//  Copyright © 2017 winemocol. All rights reserved.
//

#import "FogoOverlay.h"

@implementation FogoOverlay

@synthesize coordinate;
@synthesize boundingMapRect;

- (instancetype)initWithPossibility:(float)possValue
                     andCoordinates:(CLLocationCoordinate2D)fCoordinate
                 andBoundingMapRect:(MKMapRect)fBoundingMapRect {
    self = [super init];
    if (self) {
        self.possValue = possValue;
        coordinate = fCoordinate;
        boundingMapRect = fBoundingMapRect;
    }
    return self;
}

@end
