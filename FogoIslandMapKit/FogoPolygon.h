//
//  FogoPolygon.h
//  FogoIslandMapKit
//
//  Created by Sipan Ye on 2017/3/14.
//  Copyright © 2017年 winemocol. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface FogoPolygon : MKPolygon

@property (nonatomic) float possValue;
@property (nonatomic) UIImage *bgImage;

- (instancetype)initWithPossibility:(float)possValue
                         andBGImage:(UIImage *)bgImage;

@end
