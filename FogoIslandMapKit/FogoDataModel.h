//
//  FogoDataModel.h
//  FogoIslandMapKit
//
//  Created by Sipan Ye on 2017/1/25.
//  Copyright © 2017年 winemocol. All rights reserved.
//

#import <Foundation/Foundation.h>
/*
typedef struct FogoNodeData {
    double x;
    double y;
    double data;
} FogoNodeData;
FogoNodeData FogoNodeDataMake(double x, double y, double data);


typedef struct FogoBoundingBox {
    double xMin;
    double yMin;
    double xMax;
    double yMax;
} FogoBoundingBox;
FogoBoundingBox FogoBoundingBoxMake(double xMin, double yMin, double xMax, double yMax);
*/

@interface FogoBoundingBox : NSObject

@property (nonatomic) double xMin;
@property (nonatomic) double yMin;
@property (nonatomic) double xMax;
@property (nonatomic) double yMax;

- (instancetype)initWithMinX:(double)xMin andMinY:(double)yMin andMaxX:(double)xMax andMaxY:(double)yMax;

@end

@interface FogoNodeData : NSObject

//@property (nonatomic) FogoBoundingBox *fBBox;
@property (nonatomic) double xMin;
@property (nonatomic) double yMin;
@property (nonatomic) double data;

//FogoNodeDataMake(FogoBoundingBox *fBBox, double data);
//- (instancetype)initWithBoundingBox:(FogoBoundingBox *)fBBox andPossibility:(double)data;
- (instancetype)initWithMinX:(double)xMin andMinY:(double)yMin andPossibility:(double)data;

@end

@interface FogoDataModel : NSObject

@end
