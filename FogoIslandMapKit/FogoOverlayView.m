//
//  FogoOverlayView.m
//  FogoIslandMapKit
//
//  Created by sy2036 on 2017-01-23.
//  Copyright © 2017 winemocol. All rights reserved.
//

#import "FogoOverlayView.h"

#define BLUE_HUE 0.67
#define RED_HUE 0

@interface FogoOverlayView () {
}

@property (nonatomic, strong) UIImage *overlayImage;

@end

@implementation FogoOverlayView

- (instancetype)initWithPosValue:(double)possValue andBGImage:(UIImage *)bgImage {
    self = [super init];
    if(self) {
        _overlayImage = bgImage;
    }
    
    return self;
}
/*
- (instancetype)initWithPosValue:(double)possValue {
    self = [super init];
    if(self) {
//        UIColor *color1;
//        UIColor *color2;
        float correspondingHue;
        if (0.1 <= possValue && 0.7 > possValue) {
            correspondingHue = BLUE_HUE - ((0.7-possValue)/0.4)*(BLUE_HUE-RED_HUE);
        } else if (possValue < 0.1) {
            correspondingHue = BLUE_HUE;
        } else {
            correspondingHue = RED_HUE;
        }

        UIColor *bgColor = [UIColor colorWithHue:correspondingHue saturation:1.f brightness:1.f alpha:1.f];

        
//        NSLog(@"Poss: %f", possValue);
        [self setBackgroundColor:bgColor];
        //possValueLabel = [[UILabel alloc] initWithFrame:self.frame];
        //[possValueLabel setFont:[UIFont systemFontOfSize:20.f]];
        //[possValueLabel setText:[NSString stringWithFormat:@"@%f", possValue]];
        //[self addSubview:possValueLabel];
    }
    
    return self;
}
*/

- (void)drawMapRect:(MKMapRect)mapRect zoomScale:(MKZoomScale)zoomScale inContext:(CGContextRef)context {
    CGImageRef imageReference = self.overlayImage.CGImage;
    
    MKMapRect theMapRect = self.overlay.boundingMapRect;
    CGRect theRect = [self rectForMapRect:theMapRect];
    
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextTranslateCTM(context, 0.0, -theRect.size.height);
    CGContextDrawImage(context, theRect, imageReference);
}

- (UIColor *)getColorBetweenFirstColor:(UIColor *)color1 andSecondColor:(UIColor *)color2 andPossibility:(double)pV {
    const CGFloat *components1 = CGColorGetComponents(color1.CGColor);
    const CGFloat *components2 = CGColorGetComponents(color2.CGColor);
    return [UIColor colorWithRed:components1[0]*pV + components2[0]*(1-pV)
                           green:components1[1]*pV + components2[1]*(1-pV)
                            blue:components1[2]*pV + components2[2]*(1-pV) alpha:1.f];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
