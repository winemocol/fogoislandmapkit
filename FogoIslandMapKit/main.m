//
//  main.m
//  FogoIslandMapKit
//
//  Created by sy2036 on 2017-01-13.
//  Copyright © 2017 winemocol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
