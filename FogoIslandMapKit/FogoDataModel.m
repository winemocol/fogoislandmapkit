//
//  FogoDataModel.m
//  FogoIslandMapKit
//
//  Created by Sipan Ye on 2017/1/25.
//  Copyright © 2017年 winemocol. All rights reserved.
//

#import "FogoDataModel.h"
/*
FogoNodeData FogoNodeDataMake(double x, double y, double data) {
    FogoNodeData fNData;
    fNData.x = x;
    fNData.y = y;
    fNData.data = data;
    return fNData;
}

FogoBoundingBox FogoBoundingBoxMake(double xMin, double yMin, double xMax, double yMax) {
    FogoBoundingBox fBBox;
    fBBox.xMin = xMin;
    fBBox.yMin = yMin;
    fBBox.xMax = xMax;
    fBBox.yMax = yMax;
    return fBBox;
}
*/


@implementation FogoBoundingBox

- (instancetype)initWithMinX:(double)xMin andMinY:(double)yMin andMaxX:(double)xMax andMaxY:(double)yMax {
    self = [super init];
    if (self) {
        self.xMin = xMin;
        self.yMin = yMin;
        self.xMax = xMax;
        self.yMax = yMax;
    }
    return self;
}

@end

@implementation FogoNodeData
/*
- (instancetype)initWithBoundingBox:(FogoBoundingBox *)fBBox andPossibility:(double)data {
    self  = [super init];
    if (self) {
        self.fBBox = fBBox;
        self.data = data;
    }
    return self;
}
*/
- (instancetype)initWithMinX:(double)xMin andMinY:(double)yMin andPossibility:(double)data {
    self  = [super init];
    if (self) {
        self.xMin = xMin;
        self.yMin = yMin;
        self.data = data;
    }
    return self;
}

@end

@implementation FogoDataModel

@end
