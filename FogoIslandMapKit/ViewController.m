//
//  ViewController.m
//  FogoIslandMapKit
//
//  Created by sy2036 on 2017-01-13.
//  Copyright © 2017 winemocol. All rights reserved.
//

#import "ViewController.h"
#import <MapKit/MapKit.h>
#import "MKMapView+ZoomLevel.h"
#import "FogoDataProcess.h"
#import "FogoOverlayRenderer.h"
#import "FogoOverlay.h"
#import "FogoOverlayView.h"
#import "FogoPolygon.h"
#import "FogoPolygonRenderer.h"

#define FOGO_NW_LAT 49.78354343767712
#define FOGO_NW_LNG -54.32582266065412
#define FOGO_SW_LAT 49.531878309117076
#define FOGO_SW_LNG -54.33958794258664
#define FOGO_NE_LAT 49.7745818637998
#define FOGO_NE_LNG -53.96249945939786
#define FOGO_SE_LAT 49.52299562709053
#define FOGO_SE_LNG -53.97812839986856




@interface ViewController () <MKMapViewDelegate> {
    NSInteger tileWidth;
    NSInteger tileHeight;
    FogoDataProcess *fogoDProcess;
    
    float offsetAngleCos;
    float offsetAngleSin;
    
    MKPolygon *fPolygon;
    FogoOverlay *fOverlay;
}

@property (weak, nonatomic) IBOutlet MKMapView *fogoIslandMap;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
//    [self.fogoIslandMap setCenterCoordinate:CLLocationCoordinate2DMake(49.652898, -54.180356)];

    [self.fogoIslandMap setMapRegionAnimated:YES];
    fogoDProcess = [[FogoDataProcess alloc] init];
//    NSArray *overlays = [fogoDProcess refreshDataWithMapView:_fogoIslandMap];
//    [self updateMapViewOverlaysWith:overlays];
    
    
    
//    CLLocation *aLocation = [[CLLocation alloc] initWithLatitude:FOGO_NW_LAT longitude:FOGO_NW_LNG];
//    CLLocation *bLocation = [[CLLocation alloc] initWithLatitude:FOGO_NW_LAT longitude:FOGO_NE_LNG];
//    CLLocation *cLocation = [[CLLocation alloc] initWithLatitude:FOGO_NE_LAT longitude:FOGO_NE_LNG];
    
    CGPoint aPos = [_fogoIslandMap convertCoordinate:CLLocationCoordinate2DMake(FOGO_NW_LAT, FOGO_NW_LNG) toPointToView:_fogoIslandMap];
    CGPoint bPos = [_fogoIslandMap convertCoordinate:CLLocationCoordinate2DMake(FOGO_NW_LAT, FOGO_NE_LNG) toPointToView:_fogoIslandMap];
    CGPoint cPos = [_fogoIslandMap convertCoordinate:CLLocationCoordinate2DMake(FOGO_NE_LAT, FOGO_NE_LNG) toPointToView:_fogoIslandMap];
    
    double ab = sqrt(pow(aPos.x - bPos.x, 2) + pow(aPos.y - bPos.y, 2));
    double ac = sqrt(pow(aPos.x - cPos.x, 2) + pow(aPos.y - cPos.y, 2));
    double bc = sqrt(pow(bPos.x - cPos.x, 2) + pow(bPos.y - cPos.y, 2));
    
    offsetAngleCos = (powf(ab,2) + powf(ac,2) - powf(bc,2))/(2*ab*ac);
    offsetAngleSin = sqrtf(1 - powf(offsetAngleCos, 2));
//    offsetAngleCos = (pow([aLocation distanceFromLocation:cLocation],2) + pow([aLocation distanceFromLocation:bLocation],2) - pow([bLocation distanceFromLocation:cLocation],2))/(2*[aLocation distanceFromLocation:cLocation]*[aLocation distanceFromLocation:bLocation]);
    float angle = acos(offsetAngleCos);
    MKMapCamera *newCamera = [_fogoIslandMap camera];
    [newCamera setHeading:angle];
//    [_fogoIslandMap setCamera:newCamera];
    
    
//    FogoOverlay *fOverlay = [fogoDProcess refreshDataWithMapView:_fogoIslandMap];
//    [self updateMapViewOverlaysWith:[NSArray arrayWithObject:fOverlay]];
    
    /*
    double totalTilesAtMaxZoom = MKMapSizeWorld.width / 256.0;
    NSInteger zoomLevelAtMaxZoom = log2(totalTilesAtMaxZoom);
    NSInteger zoomLevel = MAX(0, zoomLevelAtMaxZoom + floor(log2f(_fogoIslandMap.bounds.size.width / _fogoIslandMap.visibleMapRect.size.width) + 0.5));
    */
//    NSLog(@"MKMapSizeWorld: %f", _fogoIslandMap.frame.size.width);
//    NSLog(@"MKMapSizeWorld: %f", _fogoIslandMap.visibleMapRect.size.width);
    
//    [self drawRectangleForFourCorners];
    
    
    [_fogoIslandMap setAlpha:0.5f];
}

- (void)drawRectangleForFourCornersWithBgImage:(UIImage *)bgImage {
    
    CLLocationCoordinate2D coordinateArray[5];
    coordinateArray[0] = CLLocationCoordinate2DMake(FOGO_NW_LAT, FOGO_NW_LNG);
    coordinateArray[1] = CLLocationCoordinate2DMake(FOGO_NE_LAT, FOGO_NE_LNG);
    coordinateArray[3] = CLLocationCoordinate2DMake(FOGO_SW_LAT, FOGO_SW_LNG);
    coordinateArray[2] = CLLocationCoordinate2DMake(FOGO_SE_LAT, FOGO_SE_LNG);
    coordinateArray[4] = CLLocationCoordinate2DMake(FOGO_NW_LAT, FOGO_NW_LNG);
//    MKPolyline *originalRectangle = [MKPolyline polylineWithCoordinates:coordinateArray count:5];
    fPolygon = [MKPolygon polygonWithCoordinates:coordinateArray count:5];
    [_fogoIslandMap addOverlay:fPolygon];
}


#pragma mark -
#pragma mark - MapkitDelegate
- (void)updateMapViewOverlaysWith:(NSArray *)fOverlays {
    NSMutableSet *before = [NSMutableSet setWithArray:self.fogoIslandMap.overlays];
    NSSet *after = [NSSet setWithArray:fOverlays];
    
    NSMutableSet *toKeep = [NSMutableSet setWithSet:before];
    [toKeep intersectSet:after];
    
    NSMutableSet *toAdd = [NSMutableSet setWithSet:after];
    [toAdd minusSet:toKeep];
    
    NSMutableSet *toRemove = [NSMutableSet setWithSet:before];
    [toRemove minusSet:after];
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
//        [self drawRectangleForFourCornersWithBgImage:((FogoOverlay *)[fOverlays firstObject]).bgImage];
        [self.fogoIslandMap addOverlays:[toAdd allObjects]];
        [self.fogoIslandMap removeOverlays:[toRemove allObjects]];
    }];
}
/*
- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay {
    if([overlay isKindOfClass:[FogoOverlay class]]) {
        
        FogoOverlay *fOverlay = overlay;
        FogoOverlayView *fOverlayView = [[FogoOverlayView alloc] initWithPosValue:fOverlay.possValue andBGImage:fOverlay.bgImage];
    
        [fOverlayView setAlpha:0.2f];
        [fOverlayView setTransform:CGAffineTransformMakeRotation(acos(offsetAngleCos) * M_PI/180.0)];
        return fOverlayView;
    } else
        return nil;
}
*/
- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {
    
    
    
     if([overlay isKindOfClass:[MKPolyline class]]) {
        MKPolylineRenderer *originalRectangleLineView = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
        originalRectangleLineView.fillColor = [UIColor redColor];
        originalRectangleLineView.strokeColor = [UIColor redColor];
        originalRectangleLineView.lineWidth = 5;
        
        
        return originalRectangleLineView;
     } else if([overlay isKindOfClass:[FogoOverlay class]]) {
//         FogoOverlayRenderer *fOverlayRenderer = [[FogoOverlayRenderer alloc] initWithOverlay:overlay andOverlayImage:((FogoOverlay *)overlay).bgImage];
//         [fOverlayRenderer setAlpha:.0f];
         
         float correspondingHue;
         float possValue = ((FogoOverlay *)overlay).possValue;
         if (0.1 <= possValue && 0.7 > possValue) {
             correspondingHue = 0.67 - ((0.7-possValue)/0.4)*(0.67-0);
         } else if (possValue < 0.1) {
             correspondingHue = 0.67;
         } else {
             correspondingHue = 0;
         }
         
         UIColor *bgColor = [UIColor colorWithHue:correspondingHue saturation:1.f brightness:1.f alpha:1.f];
         
         
         MKPolylineRenderer *fOverlayRenderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
         fOverlayRenderer.fillColor = bgColor;
         fOverlayRenderer.strokeColor = [UIColor redColor];
         fOverlayRenderer.lineWidth = 5;
         
         return fOverlayRenderer;
     } else if ([overlay isKindOfClass:[FogoPolygon class]]) {
         float correspondingHue;
         float possValue = ((FogoPolygon *)overlay).possValue;
         if (0.1 <= possValue && 0.7 > possValue) {
             correspondingHue = 0.67 - ((0.7-possValue)/0.4)*(0.67-0);
         } else if (possValue < 0.1) {
             correspondingHue = 0.67;
         } else {
             correspondingHue = 0;
         }
         
         UIColor *bgColor = [UIColor colorWithHue:correspondingHue saturation:1.f brightness:1.f alpha:1.f];
         
         
         MKPolygonRenderer *fOverlayRenderer = [[MKPolygonRenderer alloc] initWithOverlay:overlay];
         fOverlayRenderer.fillColor = bgColor;
//         fOverlayRenderer.strokeColor = [UIColor redColor];
//         fOverlayRenderer.lineWidth = 1;
         [fOverlayRenderer setAlpha:.3f];
         return fOverlayRenderer;
     } else {
         FogoPolygonRenderer *fPolygonRenderer = [[FogoPolygonRenderer alloc] initWithOverlay:overlay andOverlayImage:[UIImage imageNamed:@"saveFore"]];
//         fPolygonRenderer.fillColor = [UIColor colorWithPatternImage:fOverlay.bgImage];
//         fPolygonRenderer.strokeColor = [UIColor redColor];
//         fPolygonRenderer.lineWidth = 5;
         return fPolygonRenderer;
     }
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
//    [[NSOperationQueue new] addOperationWithBlock:^{
        NSArray *overlays = [fogoDProcess refreshDataWithMapView:mapView andRotateAngleCos:offsetAngleCos andSin:offsetAngleSin];
    [self updateMapViewOverlaysWith:overlays];
//        [self updateMapViewOverlaysWith:overlays];
//        [[mapView viewWithTag:1000] removeFromSuperview];
//        fOverlay = [fogoDProcess refreshDataWithMapView:mapView];
    
//        if (fOverlay != nil)
//            [self updateMapViewOverlaysWith:[NSArray arrayWithObject:fOverlay]];
  
    
//    }];
    
    /*
     *  Please check "RefreshDataWith.." fucntion with block in FogoDataProcess.h for detail.
     
    [mapView removeOverlays:[mapView overlays]];
    [fogoDProcess refreshDataWithMapView:mapView withEndBlock:^(BOOL result, NSString *resultMessage, id object) {
        [mapView addOverlay:object];
    }];
     */
}

#pragma mark -
#pragma mark - Get the Geo coordirnates from the MKMapRect
- (CLLocationCoordinate2D)getCoordinateFromMapRectanglePoint:(double)x y:(double)y{
    MKMapPoint swMapPoint = MKMapPointMake(x, y);
    return MKCoordinateForMapPoint(swMapPoint);
}

-( CLLocationCoordinate2D)getNECoordinate:(MKMapRect)mRect {
    return [self getCoordinateFromMapRectanglePoint:MKMapRectGetMaxX(mRect) y:mRect.origin.y];
}
- (CLLocationCoordinate2D)getNWCoordinate:(MKMapRect)mRect {
    return [self getCoordinateFromMapRectanglePoint:MKMapRectGetMinX(mRect) y:mRect.origin.y];
}
- (CLLocationCoordinate2D)getSECoordinate:(MKMapRect)mRect {
    return [self getCoordinateFromMapRectanglePoint:MKMapRectGetMaxX(mRect) y:MKMapRectGetMaxY(mRect)];
}
- (CLLocationCoordinate2D)getSWCoordinate:(MKMapRect)mRect {
    return [self getCoordinateFromMapRectanglePoint:mRect.origin.x y:MKMapRectGetMaxY(mRect)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
