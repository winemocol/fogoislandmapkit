//
//  FogoDataProcess.m
//  FogoIslandMapKit
//
//  Created by Sipan Ye on 2017/1/25.
//  Copyright © 2017年 winemocol. All rights reserved.
//

#import "FogoDataProcess.h"
#import "FogoIslandDBManager.h"
#import "FogoPolygon.h"
//#import "FogoOverlay.h"
#import "MKMapView+ZoomLevel.h"

// As designed, the total grids of the map view is 20*15.
#define FOGO_CELL_SIZE 51.2

#define TOP_LEFT_Y -54.32582266065412
#define BOTTOM_RIGHT_Y -53.96249945939786
#define TOP_LEFT_X 49.78354343767712
#define BOTTOM_RIGHT_X 49.531878309117076

#define FOGO_NW_LAT 49.78354343767712
#define FOGO_NW_LNG -54.32582266065412
#define FOGO_SW_LAT 49.531878309117076
#define FOGO_SW_LNG -54.33958794258664
#define FOGO_NE_LAT 49.7745818637998
#define FOGO_NE_LNG -53.96249945939786
#define FOGO_SE_LAT 49.52299562709053
#define FOGO_SE_LNG -53.97812839986856


#define GRID_COL_COUNT 20
#define GRID_ROW_COUNT 15

//81 163 327 654 1309 2618

@interface FogoDataProcess () {
    MKMapPoint topLeftPoint;
    MKMapPoint topRightPoint;
    MKMapPoint bottomLeftPoint;
    MKMapPoint bottomRightPoint;
    double dataOffset;
    double dataOffsetRow;
    
    float offsetAngleCos;
    
    FogoIslandDBManager *fDBManager;
}

@end

@implementation FogoDataProcess

- (instancetype)init {
    self = [super init];
    if (self) {
        topLeftPoint = MKMapPointForCoordinate(CLLocationCoordinate2DMake(FOGO_NW_LAT, FOGO_NW_LNG));
        topRightPoint = MKMapPointForCoordinate(CLLocationCoordinate2DMake(FOGO_NE_LAT, FOGO_NE_LNG));
        bottomLeftPoint = MKMapPointForCoordinate(CLLocationCoordinate2DMake(FOGO_SW_LAT, FOGO_SW_LNG));
        bottomRightPoint = MKMapPointForCoordinate(CLLocationCoordinate2DMake(FOGO_SE_LAT, FOGO_SE_LNG));
        dataOffset = (topLeftPoint.x - topRightPoint.x) / 2619;
        dataOffsetRow = (topLeftPoint.y - bottomLeftPoint.y)/2801;
        
        //  cosA = (b^2 + c^2 - a^2)/2bc
        CLLocation *aLocation = [[CLLocation alloc] initWithLatitude:FOGO_NW_LAT longitude:FOGO_NW_LNG];
        CLLocation *bLocation = [[CLLocation alloc] initWithLatitude:FOGO_NW_LAT longitude:FOGO_NE_LNG];
        CLLocation *cLocation = [[CLLocation alloc] initWithLatitude:FOGO_NE_LAT longitude:FOGO_NE_LNG];
        
        offsetAngleCos = (pow([aLocation distanceFromLocation:cLocation],2) + pow([aLocation distanceFromLocation:bLocation],2) - pow([bLocation distanceFromLocation:cLocation],2))/(2*[aLocation distanceFromLocation:cLocation]*[aLocation distanceFromLocation:bLocation]);
        
        
        //        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        fDBManager = [[FogoIslandDBManager alloc] initWithOption:2];
        //        if (![[userDefault objectForKey:@"isNotFirstTime"] boolValue]) {
        //            [self transferFogoDataMatrixToDBWithData];
        //            [userDefault setObject:@"YES" forKey:@"isNotFirstTime"];
        //        }
    }
    return self;
}


- (void)transferFogoDataMatrixToDBWithData {
    for (int i = 1; i <= 8; i++) {
        [fDBManager insertFogoIslandDataMatrix:[self FogoDataParseFromFileLevel:i] andZoomLevel:i];
    }
}

- (void)obtainImagesForDifferentLevel {
    for (int i = 1; i <= 8; i++) {
        NSArray *dataSize = [fDBManager querySizeofDataBYZoomLevel:i];
        int dataColCount = [[dataSize objectAtIndex:0] intValue];
        int dataRowCount = [[dataSize objectAtIndex:1] intValue];
        
    }
}

- (void)loadDataWithFileLevel:(int)fileLevel withXStartIndex:(int)xSIndex andYStartIndex:(int)ySIndex andXEndCount:(int)xECount andYEndCount:(int)yECount {
    self.fDataArray = [NSArray arrayWithArray:[fDBManager queryFogoIlandDataMatrixByZoomLevel:fileLevel withXStartIndex:xSIndex andYStartIndex:ySIndex andXEndCount:xECount andYEndCount:yECount]];
}


- (NSDictionary *)FogoDataParseFromFileLevel:(int)fileLevel {
    NSString *fileName = [NSString stringWithFormat:@"FogoIslandMatrix@%dx", fileLevel];
    NSString *data = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:fileName ofType:@"csv"] encoding:NSASCIIStringEncoding error:nil];
    NSArray *rows = [data componentsSeparatedByString:@"\n"];
    NSInteger rCount = rows.count - 1;
    NSInteger cCount = 0;
    NSMutableArray *dataArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < rCount; i++) {
        NSMutableArray *columns = [NSMutableArray arrayWithArray:[rows[i] componentsSeparatedByString:@" "]];
        //  Remove the last object, because it is empty.
        [columns removeLastObject];
        [dataArray addObjectsFromArray:columns];
        cCount = [columns count];
    }
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d", cCount], @"fXCount", [NSString stringWithFormat:@"%ld", (long)rCount], @"fYCount", dataArray, @"data", nil];
    return  dic;
}

- (NSArray *)getZoomLevelWithScale:(int)scale {
    //  Index 0: Zoom Level.
    //  Index 1: Cell Size.
    //  The cell size = 1 means that the cell is the original point in the FogoIsland dataset. 2 means that current cell is combined by 2*2 of the original point.
    NSMutableArray *zoomArray = [[NSMutableArray alloc] init];
    int zoomLevel = 1;
    [zoomArray addObject:@"1"];
    [zoomArray addObject:@"1"];
    if (scale > 1 && scale <= 1.5) {
        zoomArray[0] = @"2";
        zoomArray[1] = @"2";
        zoomLevel = 2;
    } else if (scale > 1.5 && scale <= 3) {
        zoomArray[0] = @"3";
        zoomArray[1] = @"4";
        zoomLevel = 3;
    } else if (scale > 3 && scale <= 6) {
        zoomArray[0] = @"4";
        zoomArray[1] = @"8";
        zoomLevel = 4;
    } else if (scale > 6 && scale <= 12) {
        zoomArray[0] = @"5";
        zoomArray[1] = @"16";
        zoomLevel = 5;
    } else if (scale > 12 && scale <= 40) {
        zoomArray[0] = @"6";
        zoomArray[1] = @"32";
        zoomLevel = 6;
    } else if (scale > 40 && scale <= 96) {
        zoomArray[0] = @"7";
        zoomArray[1] = @"64";
        zoomLevel = 7;
    } else if (scale > 96) {
        zoomArray[0] = @"8";
        zoomArray[1] = @"128";
        zoomLevel = 8;
    }
    return [NSArray arrayWithArray:zoomArray];
}

- (void)getOverlayDataFromZoomLevel:(int)zoomLevel withXStartIndex:(int)xSIndex andYStartIndex:(int)ySIndex andXEndCount:(int)xECount andYEndCount:(int)yECount {
    [self loadDataWithFileLevel:zoomLevel withXStartIndex:xSIndex andYStartIndex:ySIndex andXEndCount:xECount andYEndCount:yECount];
}

/*
- (FogoOverlay *)refreshDataWithMapView:(MKMapView *)mapView {
    //  If the FogoIsland is in view, the value will be YES, and draw the map.
    BOOL isDraw = YES;
    //  Get the bound of the MapView.
    CLLocationCoordinate2D topLeftMapCoord = [mapView convertPoint:CGPointMake(0, 0) toCoordinateFromView:mapView];
    MKMapPoint topLeftMapPoint = MKMapPointForCoordinate(topLeftMapCoord);
    CLLocationCoordinate2D bottomRightMapCoord = [mapView convertPoint:CGPointMake(mapView.frame.size.width, mapView.frame.size.height) toCoordinateFromView:mapView];
    MKMapPoint bottomRightMapPoint = MKMapPointForCoordinate(bottomRightMapCoord);
    
    //  Divide the current MapRect to 20*15 grids.
    double xOffset = fabs(topLeftMapPoint.x - bottomRightMapPoint.x)/GRID_COL_COUNT;
    //    double yOffset = fabs(topLeftMapPoint.y - bottomRightMapPoint.y)/GRID_ROW_COUNT;
    //  Obtain the zoom scale.
    int zoomScale = fabs(xOffset / dataOffset);
    
    
    //  Get zoom level and cell count.
    NSArray *zoomInfoArray = [self getZoomLevelWithScale:zoomScale];
    int zoomLevel = [[zoomInfoArray objectAtIndex:0] intValue];
    //    int cellCount = [[zoomInfoArray objectAtIndex:1] intValue];
    
    int xStartIndex = 0;
    int yStartIndex = 0;
    
    NSArray *dataSize = [fDBManager querySizeofDataBYZoomLevel:zoomLevel];
    int dataColCount = [[dataSize objectAtIndex:0] intValue];
    int dataRowCount = [[dataSize objectAtIndex:1] intValue];
    double currentDataWidth = fabs(topLeftPoint.x - bottomRightPoint.x)/dataColCount;
    double currentDataHeight = fabs(topLeftPoint.y - bottomRightPoint.y)/dataRowCount;
    
    
    double xLeftBoundaryDifference = topLeftMapPoint.x - topLeftPoint.x;
    double yLeftBoundaryDifference = topLeftMapPoint.y - topLeftPoint.y;
    //    double xRightBoundaryDifference = bottomRightMapPoint.x - bottomRightPoint.x;
    double yRightBoundaryDifference = bottomRightMapPoint.y - bottomRightPoint.y;
    NSLog(@"Zoom Level: %d", zoomLevel);
    
    MKMapPoint overlayStartPoint;
    NSArray *startPointOffsetByLevel = [self updateOffsetForLevel:zoomLevel];
    float xStartPointOffset = [[startPointOffsetByLevel objectAtIndex:0] floatValue];
    float yStartPointOffset = [[startPointOffsetByLevel objectAtIndex:1] floatValue];
    int colCount = (int)fabs(topLeftMapPoint.x - bottomRightMapPoint.x)/currentDataWidth +1;
    int rowCount = (int)fabs(topLeftMapPoint.y - bottomRightMapPoint.y)/currentDataHeight +1;
    int xEndCount = colCount;
    int yEndCount = rowCount;
    if (8 == zoomLevel) {
        //  Solved
        xStartIndex = 0;
        yStartIndex = 0;
        xEndCount = 20;
        yEndCount = 22;
        overlayStartPoint = MKMapPointMake(topLeftPoint.x, topLeftPoint.y);
        colCount = 20;
        rowCount = 22;
    } else if (xLeftBoundaryDifference <= 0) {
        if (yLeftBoundaryDifference <= 0) {
            //  While the top left corner of the given FogoIsland map is in the range of the visible mapview.
            //  Solved
            colCount = (int)fabs(bottomRightMapPoint.x - topLeftPoint.x) / currentDataWidth + 1;
            rowCount = (int)fabs(bottomRightMapPoint.y - topLeftPoint.y) / currentDataHeight + 1;
            xEndCount = colCount;
            yEndCount = rowCount;
            overlayStartPoint = MKMapPointMake(topLeftPoint.x, topLeftPoint.y);
        } else if (yRightBoundaryDifference <= 0) {
            //  While only the top left corner x is in the range of the visible mapview, the top left corner y is above the visible mapview.
            //  Solved
            yStartIndex = (int)fabs(yLeftBoundaryDifference / currentDataHeight);
            colCount = (int)fabs(bottomRightMapPoint.x - topLeftPoint.x) / currentDataWidth + 1;
            rowCount += 2;
            xEndCount = colCount;
            yEndCount = rowCount;
            overlayStartPoint = MKMapPointMake(topLeftPoint.x, topLeftPoint.y + yStartIndex*currentDataHeight);
        } else {
            //  While the bottom left corner of the given FogoIsland map is in the range of the visible mapview.
            //  Solved
            yStartIndex = (int)fabs(yLeftBoundaryDifference / currentDataHeight);
            
            rowCount = dataRowCount - yStartIndex - 1;
            yEndCount = rowCount;
            if (rowCount > 0)
                NSLog(@"123");
            else
                isDraw = NO;
            
            overlayStartPoint = MKMapPointMake(topLeftPoint.x, topLeftPoint.y + yStartIndex*currentDataHeight);
        }
    } else {
        if (yLeftBoundaryDifference <= 0) {
            NSLog(@"444");
            //  While the top right corner of the given FogoIsland map is in the range of the visible mapview.
            //  Solved
            xStartIndex = (int)fabs(xLeftBoundaryDifference / currentDataWidth);
            yEndCount = rowCount;
            xEndCount = colCount;
            if (colCount > 0)
                NSLog(@"123");
            else
                isDraw = NO;
            overlayStartPoint = MKMapPointMake(topLeftPoint.x + xStartIndex*currentDataWidth, topLeftPoint.y);
        } else if (yRightBoundaryDifference <= 0) {
            NSLog(@"555");
            //  Solved
            if (bottomRightMapPoint.x <= bottomRightPoint.x) {
                NSLog(@"123");
                
                xStartIndex = (int)fabs(xLeftBoundaryDifference / currentDataWidth);
                yStartIndex = (int)fabs(yLeftBoundaryDifference / currentDataHeight);
                rowCount += 2;
                colCount += 4;
                yEndCount = rowCount;
                xEndCount = colCount;
                
            } else if (fabs(bottomRightPoint.x-bottomRightMapPoint.x) < fabs(topLeftMapPoint.x - bottomRightMapPoint.x)) {
                xStartIndex = (int)fabs(xLeftBoundaryDifference / currentDataWidth);
                yStartIndex = (int)fabs(yLeftBoundaryDifference / currentDataHeight);
                colCount = (int)fabs(bottomRightPoint.x-topLeftMapPoint.x)/currentDataWidth;
                colCount += 2;
                yEndCount = rowCount;
                xEndCount = colCount;
            } else
                isDraw = NO;
            
            overlayStartPoint = MKMapPointMake(topLeftPoint.x + xStartIndex*currentDataWidth, topLeftPoint.y + yStartIndex*currentDataHeight);
        } else {
            //  Solved
            NSLog(@"666");
            xStartIndex = (int)fabs(xLeftBoundaryDifference / currentDataWidth);
            yStartIndex = (int)fabs(yLeftBoundaryDifference / currentDataHeight);
            
            colCount = (int)fabs(bottomRightPoint.x-topLeftMapPoint.x)/currentDataWidth + 1;
            rowCount = (int)fabs(bottomRightPoint.y-topLeftMapPoint.y)/currentDataHeight + 1;
            yEndCount = rowCount;
            xEndCount = colCount;
            overlayStartPoint = MKMapPointMake(topLeftPoint.x + xStartIndex*currentDataWidth, topLeftPoint.y + yStartIndex*currentDataHeight);
        }
    }
    NSLog(@"XSIndex: %d, YSIndex: %d", xStartIndex, yStartIndex);
    
    MKMapSize cellSize = MKMapSizeMake(currentDataWidth, currentDataHeight);
    [self updateXStartIndex:&xStartIndex andYStartIndex:&yStartIndex andXEndCount:&colCount andYEndCount:&rowCount withXStartPointOffset:xStartPointOffset withYStartPointOffset:yStartPointOffset withDataColCount:dataColCount withDataRowCount:dataRowCount];
    [self getOverlayDataFromZoomLevel:zoomLevel withXStartIndex:xStartIndex andYStartIndex:yStartIndex andXEndCount:colCount andYEndCount:rowCount];
    
    if (isDraw)
        return [self buildFogoDataViewWith:overlayStartPoint andOverySize:cellSize andRowCount:rowCount andColCount:colCount andFDataArray:_fDataArray andMapView:mapView];
    else
        return nil;
}
*/

- (NSArray *)refreshDataWithMapView:(MKMapView *)mapView andRotateAngleCos:(float)rotatedCos andSin:(float)rotatedSin {
    //  If the FogoIsland is in view, the value will be YES, and draw the map.
    BOOL isDraw = YES;
    //  Get the bound of the MapView.
    CLLocationCoordinate2D topLeftMapCoord = [mapView convertPoint:CGPointMake(0, 0) toCoordinateFromView:mapView];
    MKMapPoint topLeftMapPoint = MKMapPointForCoordinate(topLeftMapCoord);
    CLLocationCoordinate2D bottomRightMapCoord = [mapView convertPoint:CGPointMake(mapView.frame.size.width, mapView.frame.size.height) toCoordinateFromView:mapView];
    MKMapPoint bottomRightMapPoint = MKMapPointForCoordinate(bottomRightMapCoord);
    
    //  Divide the current MapRect to 20*15 grids.
    double xOffset = fabs(topLeftMapPoint.x - bottomRightMapPoint.x)/GRID_COL_COUNT;
    //    double yOffset = fabs(topLeftMapPoint.y - bottomRightMapPoint.y)/GRID_ROW_COUNT;
    //  Obtain the zoom scale.
    int zoomScale = fabs(xOffset / dataOffset);
    
    
    //  Get zoom level and cell count.
    NSArray *zoomInfoArray = [self getZoomLevelWithScale:zoomScale];
    int zoomLevel = [[zoomInfoArray objectAtIndex:0] intValue];
    //    int cellCount = [[zoomInfoArray objectAtIndex:1] intValue];
    
    int xStartIndex = 0;
    int yStartIndex = 0;
    
    NSArray *dataSize = [fDBManager querySizeofDataBYZoomLevel:zoomLevel];
    int dataColCount = [[dataSize objectAtIndex:0] intValue];
    int dataRowCount = [[dataSize objectAtIndex:1] intValue];
    double currentDataWidth = fabs(topLeftPoint.x - bottomRightPoint.x)/dataColCount;
    double currentDataHeight = fabs(topLeftPoint.y - bottomRightPoint.y)/dataRowCount;
    
    
    double xLeftBoundaryDifference = topLeftMapPoint.x - topLeftPoint.x;
    double yLeftBoundaryDifference = topLeftMapPoint.y - topLeftPoint.y;
    //    double xRightBoundaryDifference = bottomRightMapPoint.x - bottomRightPoint.x;
    double yRightBoundaryDifference = bottomRightMapPoint.y - bottomRightPoint.y;
    NSLog(@"Zoom Level: %d", zoomLevel);
    
    MKMapPoint overlayStartPoint;
    NSArray *startPointOffsetByLevel = [self updateOffsetForLevel:zoomLevel];
    float xStartPointOffset = [[startPointOffsetByLevel objectAtIndex:0] floatValue];
    float yStartPointOffset = [[startPointOffsetByLevel objectAtIndex:1] floatValue];
    int colCount = (int)fabs(topLeftMapPoint.x - bottomRightMapPoint.x)/currentDataWidth +1;
    int rowCount = (int)fabs(topLeftMapPoint.y - bottomRightMapPoint.y)/currentDataHeight +1;
    int xEndCount = colCount;
    int yEndCount = rowCount;
    if (8 == zoomLevel) {
        //  Solved
        xStartIndex = 0;
        yStartIndex = 0;
        xEndCount = 21;
        yEndCount = 22;
        overlayStartPoint = MKMapPointMake(topLeftPoint.x, topLeftPoint.y);
        colCount = 21;
        rowCount = 22;
    } else if (xLeftBoundaryDifference <= 0) {
        if (yLeftBoundaryDifference <= 0) {
            //  While the top left corner of the given FogoIsland map is in the range of the visible mapview.
            //  Solved
            colCount = (int)fabs(bottomRightMapPoint.x - topLeftPoint.x) / currentDataWidth + 1;
            rowCount = (int)fabs(bottomRightMapPoint.y - topLeftPoint.y) / currentDataHeight + 1;
            xEndCount = colCount;
            yEndCount = rowCount;
            overlayStartPoint = MKMapPointMake(topLeftPoint.x, topLeftPoint.y);
        } else if (yRightBoundaryDifference <= 0) {
            //  While only the top left corner x is in the range of the visible mapview, the top left corner y is above the visible mapview.
            //  Solved
            yStartIndex = (int)fabs(yLeftBoundaryDifference / currentDataHeight);
            colCount = (int)fabs(bottomRightMapPoint.x - topLeftPoint.x) / currentDataWidth + 1;
            rowCount += 2;
            xEndCount = colCount;
            yEndCount = rowCount;
            overlayStartPoint = MKMapPointMake(topLeftPoint.x, topLeftPoint.y + yStartIndex*currentDataHeight);
        } else {
            //  While the bottom left corner of the given FogoIsland map is in the range of the visible mapview.
            //  Solved
            yStartIndex = (int)fabs(yLeftBoundaryDifference / currentDataHeight);
            
            rowCount = dataRowCount - yStartIndex - 1;
            yEndCount = rowCount;
            if (rowCount > 0)
                NSLog(@"123");
            else
                isDraw = NO;
            
            overlayStartPoint = MKMapPointMake(topLeftPoint.x, topLeftPoint.y + yStartIndex*currentDataHeight);
        }
    } else {
        if (yLeftBoundaryDifference <= 0) {
            NSLog(@"444");
            //  While the top right corner of the given FogoIsland map is in the range of the visible mapview.
            //  Solved
            xStartIndex = (int)fabs(xLeftBoundaryDifference / currentDataWidth);
            yEndCount = rowCount;
            xEndCount = colCount;
            if (colCount > 0)
                NSLog(@"123");
            else
                isDraw = NO;
            overlayStartPoint = MKMapPointMake(topLeftPoint.x + xStartIndex*currentDataWidth, topLeftPoint.y);
        } else if (yRightBoundaryDifference <= 0) {
            NSLog(@"555");
            //  Solved
            if (bottomRightMapPoint.x <= bottomRightPoint.x) {
                NSLog(@"123");
                
                xStartIndex = (int)fabs(xLeftBoundaryDifference / currentDataWidth);
                yStartIndex = (int)fabs(yLeftBoundaryDifference / currentDataHeight);
                rowCount += 2;
                colCount += 4;
                yEndCount = rowCount;
                xEndCount = colCount;
                
                //                [self updateXStartIndex:&xStartIndex andYStartIndex:&yStartIndex withXStartPointOffset:xStartPointOffset withYStartPointOffset:yStartPointOffset];
                
            } else if (fabs(bottomRightPoint.x-bottomRightMapPoint.x) < fabs(topLeftMapPoint.x - bottomRightMapPoint.x)) {
                xStartIndex = (int)fabs(xLeftBoundaryDifference / currentDataWidth);
                yStartIndex = (int)fabs(yLeftBoundaryDifference / currentDataHeight);
                colCount = (int)fabs(bottomRightPoint.x-topLeftMapPoint.x)/currentDataWidth;
                colCount += 2;
                yEndCount = rowCount;
                xEndCount = colCount;
            } else
                isDraw = NO;
            
            overlayStartPoint = MKMapPointMake(topLeftPoint.x + xStartIndex*currentDataWidth, topLeftPoint.y + yStartIndex*currentDataHeight);
        } else {
            //  Solved
            NSLog(@"666");
            xStartIndex = (int)fabs(xLeftBoundaryDifference / currentDataWidth);
            yStartIndex = (int)fabs(yLeftBoundaryDifference / currentDataHeight);
            
            colCount = (int)fabs(bottomRightPoint.x-topLeftMapPoint.x)/currentDataWidth + 1;
            rowCount = (int)fabs(bottomRightPoint.y-topLeftMapPoint.y)/currentDataHeight + 1;
            yEndCount = rowCount;
            xEndCount = colCount;
            overlayStartPoint = MKMapPointMake(topLeftPoint.x + xStartIndex*currentDataWidth, topLeftPoint.y + yStartIndex*currentDataHeight);
        }
    }
    NSLog(@"XSIndex: %d, YSIndex: %d", xStartIndex, yStartIndex);
    
    MKMapSize cellSize = MKMapSizeMake(currentDataWidth, currentDataHeight);
    //    [self updateXStartIndex:&xStartIndex andYStartIndex:&yStartIndex withXStartPointOffset:xStartPointOffset withYStartPointOffset:yStartPointOffset];
//    [self updateXStartIndex:&xStartIndex andYStartIndex:&yStartIndex andXEndCount:&colCount andYEndCount:&rowCount withXStartPointOffset:xStartPointOffset withYStartPointOffset:yStartPointOffset withDataColCount:dataColCount withDataRowCount:dataRowCount];
    [self getOverlayDataFromZoomLevel:zoomLevel withXStartIndex:xStartIndex andYStartIndex:yStartIndex andXEndCount:colCount andYEndCount:rowCount];
    
    if (isDraw) {
        return [self buildFogoDataOverleyArrayWith:overlayStartPoint andOverySize:cellSize andRowCount:rowCount andColCount:colCount andFDataArray:_fDataArray andRotateAngleCos:rotatedCos andSin:rotatedSin];
    } else
        return [NSArray array];
}


- (void)refreshDataWithMapView:(MKMapView *)mapView withEndBlock:(SPBlockRequestEnd)endBlock {
    //  If the FogoIsland is in view, the value will be YES, and draw the map.
    BOOL isDraw = YES;
    //  Get the bound of the MapView.
    CLLocationCoordinate2D topLeftMapCoord = [mapView convertPoint:CGPointMake(0, 0) toCoordinateFromView:mapView];
    MKMapPoint topLeftMapPoint = MKMapPointForCoordinate(topLeftMapCoord);
    CLLocationCoordinate2D bottomRightMapCoord = [mapView convertPoint:CGPointMake(mapView.frame.size.width, mapView.frame.size.height) toCoordinateFromView:mapView];
    MKMapPoint bottomRightMapPoint = MKMapPointForCoordinate(bottomRightMapCoord);
    
    //  Divide the current MapRect to 20*15 grids.
    double xOffset = fabs(topLeftMapPoint.x - bottomRightMapPoint.x)/GRID_COL_COUNT;
    //  Obtain the zoom scale.
    int zoomScale = fabs(xOffset / dataOffset);
    
    
    //  Get zoom level and cell count.
    NSArray *zoomInfoArray = [self getZoomLevelWithScale:zoomScale];
    int zoomLevel = [[zoomInfoArray objectAtIndex:0] intValue];
    
    int xStartIndex = 0;
    int yStartIndex = 0;
    
    NSArray *dataSize = [fDBManager querySizeofDataBYZoomLevel:zoomLevel];
    int dataColCount = [[dataSize objectAtIndex:0] intValue];
    int dataRowCount = [[dataSize objectAtIndex:1] intValue];
    double currentDataWidth = fabs(topLeftPoint.x - bottomRightPoint.x)/dataColCount;
    double currentDataHeight = fabs(topLeftPoint.y - bottomRightPoint.y)/dataRowCount;
    
    
    double xLeftBoundaryDifference = topLeftMapPoint.x - topLeftPoint.x;
    double yLeftBoundaryDifference = topLeftMapPoint.y - topLeftPoint.y;
    double yRightBoundaryDifference = bottomRightMapPoint.y - bottomRightPoint.y;
    NSLog(@"Zoom Level: %d", zoomLevel);
    
    MKMapPoint overlayStartPoint;
    NSArray *startPointOffsetByLevel = [self updateOffsetForLevel:zoomLevel];
    float xStartPointOffset = [[startPointOffsetByLevel objectAtIndex:0] floatValue];
    float yStartPointOffset = [[startPointOffsetByLevel objectAtIndex:1] floatValue];
    int colCount = (int)fabs(topLeftMapPoint.x - bottomRightMapPoint.x)/currentDataWidth +1;
    int rowCount = (int)fabs(topLeftMapPoint.y - bottomRightMapPoint.y)/currentDataHeight +1;
    int xEndCount = colCount;
    int yEndCount = rowCount;
    if (8 == zoomLevel) {
        //  Solved
        xStartIndex = 0;
        yStartIndex = 0;
        xEndCount = 20;
        yEndCount = 22;
        overlayStartPoint = MKMapPointMake(topLeftPoint.x, topLeftPoint.y-currentDataHeight/2);
        colCount = 20;
        rowCount = 22;
    } else if (xLeftBoundaryDifference <= 0) {
        if (yLeftBoundaryDifference <= 0) {
            //  While the top left corner of the given FogoIsland map is in the range of the visible mapview.
            //  Solved
            colCount = (int)fabs(bottomRightMapPoint.x - topLeftPoint.x) / currentDataWidth + 1;
            rowCount = (int)fabs(bottomRightMapPoint.y - topLeftPoint.y) / currentDataHeight + 1;
            xEndCount = colCount;
            yEndCount = rowCount;
            overlayStartPoint = MKMapPointMake(topLeftPoint.x, topLeftPoint.y);
        } else if (yRightBoundaryDifference <= 0) {
            //  While only the top left corner x is in the range of the visible mapview, the top left corner y is above the visible mapview.
            //  Solved
            yStartIndex = (int)fabs(yLeftBoundaryDifference / currentDataHeight);
            colCount = (int)fabs(bottomRightMapPoint.x - topLeftPoint.x) / currentDataWidth + 1;
            rowCount += 2;
            xEndCount = colCount;
            yEndCount = rowCount;
            overlayStartPoint = MKMapPointMake(topLeftPoint.x, topLeftPoint.y + yStartIndex*currentDataHeight);
        } else {
            //  While the bottom left corner of the given FogoIsland map is in the range of the visible mapview.
            //  Solved
            yStartIndex = (int)fabs(yLeftBoundaryDifference / currentDataHeight);
            
            rowCount = dataRowCount - yStartIndex - 1;
            yEndCount = rowCount;
            if (rowCount > 0)
                NSLog(@"123");
            else
                isDraw = NO;
            overlayStartPoint = MKMapPointMake(topLeftPoint.x, topLeftPoint.y + yStartIndex*currentDataHeight);
        }
    } else {
        if (yLeftBoundaryDifference <= 0) {
            NSLog(@"444");
            //  While the top right corner of the given FogoIsland map is in the range of the visible mapview.
            //  Solved
            xStartIndex = (int)fabs(xLeftBoundaryDifference / currentDataWidth);
            yEndCount = rowCount;
            xEndCount = colCount;
            if (colCount > 0)
                NSLog(@"123");
            else
                isDraw = NO;
            overlayStartPoint = MKMapPointMake(topLeftPoint.x + xStartIndex*currentDataWidth, topLeftPoint.y);
        } else if (yRightBoundaryDifference <= 0) {
            NSLog(@"555");
            //  Solved
            if (bottomRightMapPoint.x <= bottomRightPoint.x) {
                NSLog(@"123");
                
                xStartIndex = (int)fabs(xLeftBoundaryDifference / currentDataWidth);
                yStartIndex = (int)fabs(yLeftBoundaryDifference / currentDataHeight);
                rowCount += 2;
                colCount += 4;
                yEndCount = rowCount;
                xEndCount = colCount;
                
            } else if (fabs(bottomRightPoint.x-bottomRightMapPoint.x) < fabs(topLeftMapPoint.x - bottomRightMapPoint.x)) {
                xStartIndex = (int)fabs(xLeftBoundaryDifference / currentDataWidth);
                yStartIndex = (int)fabs(yLeftBoundaryDifference / currentDataHeight);
                colCount = (int)fabs(bottomRightPoint.x-topLeftMapPoint.x)/currentDataWidth;
                colCount += 2;
                yEndCount = rowCount;
                xEndCount = colCount;
            } else
                isDraw = NO;
            
            overlayStartPoint = MKMapPointMake(topLeftPoint.x + xStartIndex*currentDataWidth, topLeftPoint.y + yStartIndex*currentDataHeight);
        } else {
            //  Solved
            NSLog(@"666");
            xStartIndex = (int)fabs(xLeftBoundaryDifference / currentDataWidth);
            yStartIndex = (int)fabs(yLeftBoundaryDifference / currentDataHeight);
            
            colCount = (int)fabs(bottomRightPoint.x-topLeftMapPoint.x)/currentDataWidth + 1;
            rowCount = (int)fabs(bottomRightPoint.y-topLeftMapPoint.y)/currentDataHeight + 1;
            yEndCount = rowCount;
            xEndCount = colCount;
            overlayStartPoint = MKMapPointMake(topLeftPoint.x + xStartIndex*currentDataWidth, topLeftPoint.y + yStartIndex*currentDataHeight);
        }
    }
    NSLog(@"XSIndex: %d, YSIndex: %d", xStartIndex, yStartIndex);
    
    MKMapSize cellSize = MKMapSizeMake(currentDataWidth, currentDataHeight);
    [self updateXStartIndex:&xStartIndex andYStartIndex:&yStartIndex andXEndCount:&colCount andYEndCount:&rowCount withXStartPointOffset:xStartPointOffset withYStartPointOffset:yStartPointOffset withDataColCount:dataColCount withDataRowCount:dataRowCount];
    __block int i = 0;
    __block int j = 0;
    self.fDataArray = [NSArray arrayWithArray:[fDBManager queryFogoIlandDataMatrixByZoomLevel:zoomLevel withXStartIndex:xStartIndex andYStartIndex:yStartIndex andXEndCount:colCount andYEndCount:rowCount withEndBlock:^(BOOL result, NSString *resultMessage, NSDictionary *dictionary) {
        if (i < rowCount) {
            if (j < colCount) {
                if (isDraw) {
                    FogoOverlay *fOverlay =  [self buildFogoDataOverleyArrayWith:overlayStartPoint andOverySize:cellSize andRowIndex:i andColIndex:j andFDataValue:[[dictionary objectForKey:@"fData"] doubleValue]];
                    if (endBlock)
                        endBlock(YES, @"Success", fOverlay);
                }
                j++;
            } else {
                j = 0;
                i++;
            }
        }
    }]];
}

/**
 Set offset for different level in order to matching data on the map.
 
 @param zoomLevel Map zoom level
 @return Array of Offset, Index 0 is for X, Index 1 is for Y
 */
- (NSArray *)updateOffsetForLevel:(int)zoomLevel {
    NSMutableArray *offsetArray = [[NSMutableArray alloc] init];
    float xStartPointOffset = 0;
    float yStartPointOffset = 0;
    switch (zoomLevel) {
        case 1: {
            xStartPointOffset = 8;
            yStartPointOffset = 16;
        }
            break;
        case 2: {
            xStartPointOffset = 6;
            yStartPointOffset = 16;
        }
            break;
        case 3: {
            xStartPointOffset = 8;
            yStartPointOffset = 13;
        }
            break;
        case 4: {
            xStartPointOffset = 3;
            yStartPointOffset = 5;
        }
            break;
        case 5: {
            xStartPointOffset = 2;
            yStartPointOffset = 3;
        }
            break;
        case 6: {
            xStartPointOffset = 0.5;
            yStartPointOffset = -1.5;
        }
            break;
        case 7: {
//            xStartPointOffset = -1;
        }
            break;
        case 8: {
            yStartPointOffset = 0.5;
        }
            break;
        default:
            break;
    }
    [offsetArray addObject:[NSString stringWithFormat:@"%f", xStartPointOffset]];
    [offsetArray addObject:[NSString stringWithFormat:@"%f", yStartPointOffset]];
    return [NSArray arrayWithArray:offsetArray];
}

- (void)updateXStartIndex:(int *)xStartIndex andYStartIndex:(int *)yStartIndex andXEndCount:(int *)xEndCount andYEndCount:(int *)yEndCount withXStartPointOffset:(int)xStartPointOffset withYStartPointOffset:(int)yStartPointOffset withDataColCount:(int)dataColCount withDataRowCount:(int)dataRowCount {
    *xStartIndex += (int)xStartPointOffset;
    if (*xStartIndex < 0) {
        xStartPointOffset = abs(*xStartIndex - 0);
        *xStartIndex = 0;
    }
    *yStartIndex -= yStartPointOffset;
    if (*yStartIndex < 0) {
        yStartPointOffset = abs(*yStartIndex - 0);
        *yStartIndex = 0;
    }
    /*
     if (*xStartIndex + xStartPointOffset < 0) {
     xStartPointOffset = abs(*xStartIndex - 0);
     *xStartIndex = 0;
     } else
     *xStartIndex += (int)xStartPointOffset;
     
     if (yStartIndex + yStartPointOffset < 0) {
     yStartPointOffset = abs(*yStartIndex - 0);
     *yStartIndex = 0;
     } else
     *yStartIndex -= yStartPointOffset;
     */
    if (*xStartIndex + *xEndCount > dataColCount)
        *xEndCount = dataColCount - *xStartIndex - 1;
    if (*yStartIndex + *yEndCount > dataRowCount)
        *yEndCount = dataRowCount - *yStartIndex - 1;
}

/**
 Build Overlays for FogoIsland data
 
 @param overlayStartPoint The point of the left top start overlay.
 @param cellSize Size of the overlay
 @param rowCount Number of overlays on a single line
 @param colCount Number of overlays on a column
 @param fDataArray Array of FogoIslnd data
 @return Overlays array
 */
- (NSArray *)buildFogoDataOverleyArrayWith:(MKMapPoint)overlayStartPoint andOverySize:(MKMapSize)cellSize andRowCount:(int)rowCount andColCount:(int)colCount andFDataArray:(NSArray *)fDataArray  andRotateAngleCos:(float)rotatedCos andSin:(float)rotatedSin {
    NSMutableArray *fOverlayArray = [[NSMutableArray alloc] init];
    MKMapPoint topleft = overlayStartPoint;
    MKMapRect boundRect =  MKMapRectMake(topleft.x, topleft.y, cellSize.width, cellSize.height);
    MKMapPoint topRight = MKMapPointMake(MKMapRectGetMaxX(boundRect), topleft.y);
    MKMapPoint bottomleft = MKMapPointMake(topleft.x, MKMapRectGetMaxY(boundRect));
    MKMapPoint bottomRight = MKMapPointMake(topRight.x, bottomleft.y);
    
    
    MKMapPoint rowStartPoint = topleft;
    for (NSInteger i = 0; i < rowCount; i++) {
        for (NSInteger j = 0; j < colCount; j++) {
//            MKMapPoint startPoint = MKMapPointMake(overlayStartPoint.x + cellSize.width * j, overlayStartPoint.y + cellSize.height * i);
            double dataValue = [[fDataArray objectAtIndex:j + colCount*i] doubleValue];
            //                NSLog(@"%f", dataValue);
//            MKMapRect boundRect =  MKMapRectMake(startPoint.x, startPoint.y, cellSize.width, cellSize.height);
//            CLLocationCoordinate2D fCenterCoordinate  = MKCoordinateRegionForMapRect(boundRect).center;;
//            FogoOverlay *fOverlay = [[FogoOverlay alloc] initWithPossibility:dataValue andCoordinates:fCenterCoordinate andBoundingMapRect:boundRect];
            
            
//            (x1，y1)为要转的点，（x2,y2）为中心点，如果是顺时针角度为-，
//            x=(x1-x2)cosθ-(y1-y2)sinθ+x2
//            y=(y1-y2)cosθ+(x1-x2)sinθ+y2
            MKMapPoint mapPoints[5];
            mapPoints[0] = topleft;
            mapPoints[1] = MKMapPointMake((topRight.x-topleft.x)*rotatedCos - (topRight.y-topleft.y)*rotatedSin + topleft.x, (topRight.y-topleft.y)*rotatedCos + (topRight.x-topleft.x)*rotatedSin + topleft.y);
            mapPoints[2] = MKMapPointMake((bottomRight.x-topleft.x)*rotatedCos - (bottomRight.y-topleft.y)*rotatedSin + topleft.x, (bottomRight.y-topleft.y)*rotatedCos + (bottomRight.x-topleft.x)*rotatedSin + topleft.y);
            mapPoints[3] = MKMapPointMake((bottomleft.x-topleft.x)*rotatedCos - (bottomleft.y-topleft.y)*rotatedSin + topleft.x, (bottomleft.y-topleft.y)*rotatedCos + (bottomleft.x-topleft.x)*rotatedSin + topleft.y);
            mapPoints[4] = topleft;
            FogoPolygon *fOverly = [FogoPolygon polygonWithPoints:mapPoints count:5];
            fOverly.possValue = dataValue;
            [fOverlayArray addObject:fOverly];
            
            if (0 == j) {
                rowStartPoint = mapPoints[3];
            }
            
            topleft = mapPoints[1];
            if (j == colCount - 1) {
                topleft = rowStartPoint;
            }
            boundRect =  MKMapRectMake(topleft.x, topleft.y, cellSize.width, cellSize.height);
            topRight = MKMapPointMake(MKMapRectGetMaxX(boundRect), topleft.y);
            bottomleft = MKMapPointMake(topleft.x, MKMapRectGetMaxY(boundRect));
            bottomRight = MKMapPointMake(topRight.x, bottomleft.y);
        }
    }
    return [NSArray arrayWithArray:fOverlayArray];
}


/**
 Build Overlays for FogoIsland data
 
 Be careful!
 This function will only return a single FogoOverlay object.
 
 @param overlayStartPoint overlayStartPoint The point of the left top start overlay.
 @param cellSize cellSize Size of the overlay
 @param rowIndex rowCount Number of overlays on a single line
 @param colIndex colCount Number of overlays on a column
 @param fDataValue fDataArray Array of FogoIslnd data
 @return FogoOverlay
 */
- (FogoOverlay *)buildFogoDataOverleyArrayWith:(MKMapPoint)overlayStartPoint andOverySize:(MKMapSize)cellSize andRowIndex:(int)rowIndex andColIndex:(int)colIndex andFDataValue:(double)fDataValue {
    
    MKMapPoint startPoint = MKMapPointMake(overlayStartPoint.x + cellSize.width * colIndex, overlayStartPoint.y + cellSize.height * rowIndex);
    
    MKMapRect boundRect =  MKMapRectMake(startPoint.x, startPoint.y, cellSize.width, cellSize.height);;
    CLLocationCoordinate2D fCenterCoordinate  = MKCoordinateRegionForMapRect(boundRect).center;;
    
    FogoOverlay *fOverlay = [[FogoOverlay alloc] initWithPossibility:fDataValue andCoordinates:fCenterCoordinate andBoundingMapRect:boundRect];
    
    return fOverlay;
}

- (FogoOverlay *)buildFogoDataViewWith:(MKMapPoint)overlayStartPoint andOverySize:(MKMapSize)cellSize andRowCount:(int)rowCount andColCount:(int)colCount andFDataArray:(NSArray *)fDataArray andMapView:(MKMapView *)mapView {
    UIView *bgView = [[UIView alloc] initWithFrame:mapView.frame];
    [bgView setBackgroundColor:[UIColor clearColor]];
    [bgView setUserInteractionEnabled:NO];
    [bgView setTag:1000];
    for (NSInteger i = 0; i < rowCount; i++) {
        for (NSInteger j = 0; j < colCount; j++) {
            MKMapPoint startPoint = MKMapPointMake(overlayStartPoint.x + cellSize.width * j, overlayStartPoint.y + cellSize.height * i);
            double dataValue = [[fDataArray objectAtIndex:j + colCount*i] doubleValue];
            MKMapRect boundRect =  MKMapRectMake(startPoint.x, startPoint.y, cellSize.width, cellSize.height);;
            
            CGRect fViewRect = [mapView convertRegion:MKCoordinateRegionForMapRect(boundRect) toRectToView:mapView];
            UIView *fView = [[UIView alloc] initWithFrame:fViewRect];
            
            float BLUE_HUE =  0.67;
            float RED_HUE = 0;
            
            float correspondingHue;
            if (0.1 <= dataValue && 0.7 > dataValue) {
                correspondingHue = BLUE_HUE - ((0.7-dataValue)/0.4)*(BLUE_HUE-RED_HUE);
            } else if (dataValue < 0.1) {
                correspondingHue = BLUE_HUE;
            } else {
                correspondingHue = RED_HUE;
            }
            
            UIColor *bgColor = [UIColor colorWithHue:correspondingHue saturation:1.f brightness:1.f alpha:1.f];
            [fView setBackgroundColor:bgColor];
//            [fView setAlpha:0.3f];
            [bgView addSubview:fView];
//            FogoOverlay *fOverlay = [[FogoOverlay alloc] initWithPossibility:dataValue andCoordinates:fCenterCoordinate andBoundingMapRect:boundRect];
////            [fOverlayArray addObject:fOverlay];
        }
    }
//    [bgView setTransform:CGAffineTransformMakeRotation(5)];
    UIGraphicsBeginImageContext(bgView.bounds.size);
    [bgView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *bgImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    MKCoordinateRegion fOverlayRegion = [mapView convertRect:bgView.frame toRegionFromView:bgView];
    MKMapRect fOverlayRect = [self MKMapRectForCoordinateRegion:fOverlayRegion];
    FogoOverlay *fOverlay = [[FogoOverlay alloc] initWithPossibility:0.f andCoordinates:fOverlayRegion.center andBoundingMapRect:fOverlayRect];
    fOverlay.bgImage = bgImage;
    
//    NSData *imagedata=UIImageJPEGRepresentation(bgImage,1.0);
//    NSArray*paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
//    NSString *documentsDirectory=[paths objectAtIndex:0];
//    NSString *savedImagePath=[documentsDirectory stringByAppendingPathComponent:@"saveFore.jpg"];
//    [imagedata writeToFile:savedImagePath atomically:YES];
    
//    [mapView addSubview:bgView];
    return fOverlay;
}

- (MKMapRect)MKMapRectForCoordinateRegion:(MKCoordinateRegion)region {
    MKMapPoint a = MKMapPointForCoordinate(CLLocationCoordinate2DMake(
                                                                      region.center.latitude + region.span.latitudeDelta / 2,
                                                                      region.center.longitude - region.span.longitudeDelta / 2));
    MKMapPoint b = MKMapPointForCoordinate(CLLocationCoordinate2DMake(
                                                                      region.center.latitude - region.span.latitudeDelta / 2,
                                                                      region.center.longitude + region.span.longitudeDelta / 2));
    return MKMapRectMake(MIN(a.x,b.x), MIN(a.y,b.y), ABS(a.x-b.x), ABS(a.y-b.y));
}


@end
