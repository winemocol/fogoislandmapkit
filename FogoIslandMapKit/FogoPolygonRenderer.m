//
//  FogoPolygonRenderer.m
//  FogoIslandMapKit
//
//  Created by Sipan Ye on 2017/3/14.
//  Copyright © 2017年 winemocol. All rights reserved.
//

#import "FogoPolygonRenderer.h"

@implementation FogoPolygonRenderer

- (instancetype)initWithOverlay:(id<MKOverlay>)overlay andOverlayImage:(UIImage *)overlayImage {
    self = [super initWithOverlay:overlay];
    if (self) {
        _bgImage = overlayImage;
    }
    
    return self;
}

- (void)drawMapRect:(MKMapRect)mapRect zoomScale:(MKZoomScale)zoomScale inContext:(CGContextRef)context {
//    [super drawMapRect:mapRect zoomScale:zoomScale inContext:context];
    MKMapRect theMapRect = [self.overlay boundingMapRect];
    CGRect theRect = [self rectForMapRect:theMapRect];
    
    UIGraphicsPushContext(context);
    UIBezierPath *bpath = [UIBezierPath bezierPath];
    MKPolygon *polyGon = self.polygon;
    MKMapPoint *points = polyGon.points;
    NSUInteger pointCount = polyGon.pointCount;
    CGPoint point = [self pointForMapPoint:points[0]];
    [bpath moveToPoint:point];
    
    for (int i = 1; i < pointCount; i++) {
        point = [self pointForMapPoint:points[i]];
        [bpath addLineToPoint:point];
    }
    [bpath closePath];
    [bpath addClip];
//    [bpath stroke];
    [_bgImage drawInRect:theRect blendMode:kCGBlendModeOverlay alpha:0.4];
    UIGraphicsPopContext();
    
}


@end
