//
//  FogoOverlayView.h
//  FogoIslandMapKit
//
//  Created by sy2036 on 2017-01-23.
//  Copyright © 2017 winemocol. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "FogoOverlay.h"

@interface FogoOverlayView : MKOverlayView

//- (instancetype)initWithPosValue:(double)possValue;
- (instancetype)initWithPosValue:(double)possValue andBGImage:(UIImage *)bgImage;

@end
