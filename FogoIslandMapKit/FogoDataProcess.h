//
//  FogoDataProcess.h
//  FogoIslandMapKit
//
//  Created by Sipan Ye on 2017/1/25.
//  Copyright © 2017年 winemocol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "FogoDataModel.h"
#import "SPBlockTypeDef.h"
#import "FogoOverlay.h"

@interface FogoDataProcess : NSObject

@property (nonatomic, strong) NSArray *fDataArray;

@property (nonatomic, assign) float latOffset;
@property (nonatomic, assign) float lngOffset;

- (instancetype)init;


//- (NSArray *)refreshDataWithMapRect:(MKMapRect)mRect;


/**
 Refresh the Map overlays data.

 @param mapView MapView
 @return Array of FogoOverlays objects.
 */
//- (NSArray *)refreshDataWithMapView:(MKMapView *)mapView;
- (NSArray *)refreshDataWithMapView:(MKMapView *)mapView andRotateAngleCos:(float)rotatedCos andSin:(float)rotatedSin;
//- (FogoOverlay *)refreshDataWithMapView:(MKMapView *)mapView;


/**
 Refresh the Map overlay data.

 @param mapView MapView
 @param endBlock The block will return "Status", "Describe of status" and "A single object of FogoOvelay" at each time.
 */
- (void)refreshDataWithMapView:(MKMapView *)mapView withEndBlock:(SPBlockRequestEnd)endBlock;

@end
