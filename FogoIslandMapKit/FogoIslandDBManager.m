//
//  FogoIslandDBManager.m
//  FogoIslandMapKit
//
//  Created by sy2036 on 2017-01-30.
//  Copyright © 2017 winemocol. All rights reserved.
//

#import "FogoIslandDBManager.h"
#import <FMDB/FMDatabase.h>
#import <FMDB/FMDatabaseQueue.h>
#import "NSString+Addtions.h"

//#define DB_FOLDER       (@"DB")
//#define DB_FILEPATH     (@"DB/FogoDataMatrix.db")

@interface FogoIslandDBManager () {
    int fOption;
}

@property (nonatomic, copy) NSString *fogoDataDBPath;
@property (nonatomic, strong) FMDatabase *fogoDataDB;
@property (nonatomic, strong) FMDatabaseQueue *fogoDataDBQueue;

@end

@implementation FogoIslandDBManager

- (id)initWithOption:(int)option {
    if ((self = [super init])) {
        fOption = option;
//        self.fogoDataDBPath = [[NSString getDocumentDirectory] stringByAppendingPathComponent:DB_FILEPATH];
        self.fogoDataDBPath = [[NSBundle mainBundle] pathForResource:@"FogoDataMatrix" ofType:@"db"];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if ([fileManager fileExistsAtPath:_fogoDataDBPath]) {
            self.fogoDataDB = [FMDatabase databaseWithPath:_fogoDataDBPath];
            if ([_fogoDataDB open]) {
                [_fogoDataDB setShouldCacheStatements:YES];
            }else{
                NSLog(@"Fail to open DB FogoDataMatrix.db!!");
            }
        }
        /*
         *  Beacuse the database is already imported into the project as source file,
         *  thus, this is no longer needed.
         
        else {
            if (![fileManager fileExistsAtPath:[[NSString getDocumentDirectory] stringByAppendingPathComponent:DB_FOLDER]]) {
                [fileManager createDirectoryAtPath:[[NSString getDocumentDirectory] stringByAppendingPathComponent:DB_FOLDER]
                       withIntermediateDirectories:YES attributes:nil error:nil];
            }
            [self createFogoDataMatrixDB];
        }
        */
        self.fogoDataDBQueue = [FMDatabaseQueue databaseQueueWithPath:_fogoDataDBPath];
    }
    return self;
}

/*
- (id)init {
    if ((self = [super init])) {
        self.fogoDataDBPath = [[NSString getDocumentDirectory] stringByAppendingPathComponent:DB_FILEPATH];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if ([fileManager fileExistsAtPath:_fogoDataDBPath]) {
            self.fogoDataDB = [FMDatabase databaseWithPath:_fogoDataDBPath];
            if ([_fogoDataDB open]) {
                [_fogoDataDB setShouldCacheStatements:YES];
            }else{
                NSLog(@"Fail to open DB FogoDataMatrix.db!!");
            }
        } else {
            if (![fileManager fileExistsAtPath:[[NSString getDocumentDirectory] stringByAppendingPathComponent:DB_FOLDER]]) {
                [fileManager createDirectoryAtPath:[[NSString getDocumentDirectory] stringByAppendingPathComponent:DB_FOLDER]
                       withIntermediateDirectories:YES attributes:nil error:nil];
            }
            [self createFogoDataMatrixDB];
        }
        self.fogoDataDBQueue = [FMDatabaseQueue databaseQueueWithPath:_fogoDataDBPath];
    }
    return self;
}
*/
- (BOOL)deleteTable:(NSString *)tableName {
    NSString *sqlstr = [NSString stringWithFormat:@"DROP TABLE %@", tableName];
    if (![self.fogoDataDB executeUpdate:sqlstr]) {
        NSLog(@"Delete table error!");
        return NO;
    }
    return YES;
}

- (void)createFogoDataMatrixDB {
    self.fogoDataDB = [FMDatabase databaseWithPath:_fogoDataDBPath];
    if (![_fogoDataDB open]) return;
    [_fogoDataDB setShouldCacheStatements:YES];
    switch (fOption) {
        case 0: {
            NSString *sql = @"CREATE TABLE IF NOT EXISTS FogoDataMatrix (fZoomLevel int, fXIndex int, fYIndex int, fData float)";
            [_fogoDataDB executeUpdate:sql];
        }
            break;
        case 1:
            [self createMultipleTables];
            break;
        case 2:
            [self createMultipleTables];
            break;
        default:
            break;
    }
}

- (void)createMultipleTables {
    NSString *sql;
    switch (fOption) {
        case 1:
            for (int i = 1; i <= 8; i++) {
                sql = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS FogoDataMatrix%d (fZoomLevel int, fXIndex int, fYIndex int, fData float)", i];
                [_fogoDataDB executeUpdate:sql];
            }
            break;
        case 2:
            for (int i = 1; i <= 8; i++) {
                sql = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS FogoDataMatrix%d (fZoomLevel int, fIndex int, fColCount int, fData float)", i];
                [_fogoDataDB executeUpdate:sql];
            }
            break;
        default:
            break;
    }
    
}

#pragma -
#pragma Database Manipulation
- (NSArray *)querySizeofDataBYZoomLevel:(int)fZoomLevel {
    __block NSMutableArray *sizeArr = [[NSMutableArray alloc] init];
    if ([_fogoDataDB open]) {
        int width = 0;
        int height = 0;
        NSString *sql = [NSString stringWithFormat:@"SELECT fColCount FROM FogoDataMatrix%d WHERE fIndex = 0", fZoomLevel];
        FMResultSet *resultSet = [_fogoDataDB executeQuery:sql];
        while([resultSet next]) {
            width = [[resultSet stringForColumn:@"fColCount"] intValue];
        }
        sql = [NSString stringWithFormat:@"SELECT COUNT(*) FROM FogoDataMatrix%d", fZoomLevel];
        resultSet = [_fogoDataDB executeQuery:sql];
        while([resultSet next]) {
            height = [[resultSet stringForColumn:@"COUNT(*)"] intValue]/width;
        }
        [sizeArr addObject:[NSString stringWithFormat:@"%d", width]];
        [sizeArr addObject:[NSString stringWithFormat:@"%d", height]];
    }
    return [NSArray arrayWithArray:sizeArr];
}


- (NSArray *)queryFogoIlandDataMatrixByZoomLevel:(int)fZoomLevel withXStartIndex:(int)xSIndex andYStartIndex:(int)ySIndex andXEndCount:(int)xECount andYEndCount:(int)yECount {
    __block NSMutableArray *fDataArray = [[NSMutableArray alloc] init];
    if ([_fogoDataDB open]) {
        NSString *sql;
        FMResultSet *resultSet;
        switch (fOption) {
            case 0: {
                sql = @"SELECT fData FROM FogoDataMatrix WHERE fZoomLevel = ? AND fXIndex >= ? AND fXIndex < ? AND fYIndex >= ? AND fYIndex < ?";
                resultSet = [_fogoDataDB executeQuery:sql, [NSString stringWithFormat:@"%d", fZoomLevel], [NSString stringWithFormat:@"%d", xSIndex], [NSString stringWithFormat:@"%d", xSIndex+xECount], [NSString stringWithFormat:@"%d", ySIndex], [NSString stringWithFormat:@"%d", ySIndex+yECount]];
                while([resultSet next]) {
                    [fDataArray addObject:[resultSet stringForColumn:@"fData"]];
                }
            }
                break;
            case 1: {
                sql = [NSString stringWithFormat:@"SELECT fData FROM FogoDataMatrix%d WHERE fXIndex >= ? AND fXIndex < ? AND fYIndex >= ? AND fYIndex < ?", fZoomLevel];
                resultSet = [_fogoDataDB executeQuery:sql, [NSString stringWithFormat:@"%d", fZoomLevel], [NSString stringWithFormat:@"%d", xSIndex], [NSString stringWithFormat:@"%d", xSIndex+xECount], [NSString stringWithFormat:@"%d", ySIndex], [NSString stringWithFormat:@"%d", ySIndex+yECount]];
                while([resultSet next]) {
                    [fDataArray addObject:[resultSet stringForColumn:@"fData"]];
                }
            }
                break;
            case 2: {
                sql = [NSString stringWithFormat:@"SELECT fColCount FROM FogoDataMatrix%d WHERE fIndex = 0", fZoomLevel];
                
                FMResultSet *colResult = [_fogoDataDB executeQuery:sql];
                int colCount = 0;
                while([colResult next]) {
                    colCount = [[colResult stringForColumn:@"fColCount"] intValue];
                }
                [colResult close];
                NSDate *date1 = [NSDate date];
                /*
                for (int i = ySIndex; i < yECount+ySIndex; i++) {
                    sql = [NSString stringWithFormat:@"SELECT fData FROM FogoDataMatrix%d WHERE fIndex >= ? AND fIndex < ?", fZoomLevel];
                    resultSet = [_fogoDataDB executeQuery:sql, [NSString stringWithFormat:@"%d", xSIndex+colCount*i], [NSString stringWithFormat:@"%d", xSIndex+xECount+colCount*i]];
                    while([resultSet next]) {
                        [fDataArray addObject:[resultSet stringForColumnIndex:0]];
                    }
                }*/
                sql = @"";
                for (int i = ySIndex; i <= yECount+ySIndex; i++) {
                    NSString *sql1 = [NSString stringWithFormat:@"SELECT fData FROM FogoDataMatrix%d WHERE fIndex >= %d AND fIndex < %d;", fZoomLevel, xSIndex+colCount*i, xSIndex+xECount+colCount*i];
                    sql = [sql stringByAppendingString:sql1];
                    
                }
                BOOL success;
                success = [_fogoDataDB executeStatements:sql withResultBlock:^int(NSDictionary *dictionary) {
                    [fDataArray addObject:[dictionary objectForKey:@"fData"]];
                    /*
                     * This is only be used while the Block function is utilized.
                    if (endBlock) {
                        endBlock(YES, @"Success", dictionary);
                    }*/
                    return 0;
                }];
                
                NSDate *date2 = [NSDate date];
                NSTimeInterval a = [date2 timeIntervalSince1970] - [date1 timeIntervalSince1970];
                NSLog(@"查询数据用时%.3f秒",a);
                
                /*
                 BOOL success;
                 sql = @"SELECT fData FROM FogoDataMatrix1 WHERE fIndex >= ? AND fIndex < ?"
                 @"SELECT fData FROM FogoDataMatrix1 WHERE fIndex >= ? AND fIndex < ?"
                 @"SELECT fData FROM FogoDataMatrix1 WHERE fIndex >= ? AND fIndex < ?"
                 @"SELECT fData FROM FogoDataMatrix1 WHERE fIndex >= ? AND fIndex < ?"
                 @"SELECT fData FROM FogoDataMatrix1 WHERE fIndex >= ? AND fIndex < ?"
                 @"SELECT fData FROM FogoDataMatrix1 WHERE fIndex >= ? AND fIndex < ?"
                 @"SELECT fData FROM FogoDataMatrix1 WHERE fIndex >= ? AND fIndex < ?"
                 @"SELECT fData FROM FogoDataMatrix8 WHERE fIndex >= ? AND fIndex < ?";
                 
                 [NSString stringWithFormat:@"SELECT fData FROM FogoDataMatrix%d WHERE fIndex >= %d AND fIndex < %d", fZoomLevel, xSIndex+colCount*ySIndex, xSIndex+xECount+colCount*ySIndex]
                 [NSString stringWithFormat:@"SELECT fData FROM FogoDataMatrix%d WHERE fIndex >= %d AND fIndex < %d", fZoomLevel, xSIndex+colCount*(ySIndex+1), xSIndex+xECount+colCount*ySIndex]
                 [NSString stringWithFormat:@"SELECT fData FROM FogoDataMatrix%d WHERE fIndex >= %d AND fIndex < %d", fZoomLevel, xSIndex+colCou nt*ySIndex, xSIndex+xECount+colCount*ySIndex]
                 [NSString stringWithFormat:@"SELECT fData FROM FogoDataMatrix%d WHERE fIndex >= %d AND fIndex < %d", fZoomLevel, xSIndex+colCount*ySIndex, xSIndex+xECount+colCount*ySIndex]
                 [NSString stringWithFormat:@"SELECT fData FROM FogoDataMatrix%d WHERE fIndex >= %d AND fIndex < %d", fZoomLevel, xSIndex+colCount*ySIndex, xSIndex+xECount+colCount*ySIndex]
                 [NSString stringWithFormat:@"SELECT fData FROM FogoDataMatrix%d WHERE fIndex >= %d AND fIndex < %d", fZoomLevel, xSIndex+colCount*ySIndex, xSIndex+xECount+colCount*ySIndex]
                 [NSString stringWithFormat:@"SELECT fData FROM FogoDataMatrix%d WHERE fIndex >= %d AND fIndex < %d", fZoomLevel, xSIndex+colCount*ySIndex, xSIndex+xECount+colCount*ySIndex]
                 [NSString stringWithFormat:@"SELECT fData FROM FogoDataMatrix%d WHERE fIndex >= %d AND fIndex < %d", fZoomLevel, xSIndex+colCount*ySIndex, xSIndex+xECount+colCount*ySIndex];
                 success = [_fogoDataDB executeStatements:sql withResultBlock:^int(NSDictionary *dictionary) {
                 
                 return 0;
                 }];
                 */
                
                /*
                 [_fogoDataDBQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
                 [db shouldCacheStatements];
                 for (int i = ySIndex; i < yECount+ySIndex; i++) {
                 sql = [NSString stringWithFormat:@"SELECT fData FROM FogoDataMatrix%d WHERE fIndex >= ? AND fIndex < ?", fZoomLevel];
                 resultSet = [db executeQuery:sql, [NSString stringWithFormat:@"%d", xSIndex+colCount*i], [NSString stringWithFormat:@"%d", xSIndex+xECount+colCount*i]];
                 while([resultSet next]) {
                 [fDataArray addObject:[resultSet stringForColumn:@"fData"]];
                 }
                 }
                 }];
                 */
            }
                break;
            default:
                break;
        }
        
        [resultSet close];
//        [_fogoDataDB close];
    }
    return [NSArray arrayWithArray:fDataArray];
}


/**
 Insert Data Matrix by the Option

 @param dataDic Data dictionary.
 @param fZoomLevel Zoom-In Level from 1 to 8. 1 means the maximun zoom in.
 @return Insert Success or Fail.
 */
- (BOOL)insertFogoIslandDataMatrix:(NSDictionary *)dataDic andZoomLevel:(int)fZoomLevel {
    bool result = NO;
    if ([_fogoDataDB open]) {
        [_fogoDataDB beginTransaction];
        NSArray *dataArray = [dataDic objectForKey:@"data"];
        int fXCount = [[dataDic objectForKey:@"fXCount"] intValue];
        int fYCount = [[dataDic objectForKey:@"fYCount"] intValue];
        for (int i = 0; i < fYCount; i++) {
            for (int j = 0; j < fXCount; j++) {
                NSString *sql;
                switch (fOption) {
                    case 0: {
                        sql = @"INSERT INTO FogoDataMatrix (fZoomLevel, fXIndex, fYIndex, fData) values(?, ?, ?, ?)";
                        [_fogoDataDB executeUpdate:sql, [NSString stringWithFormat:@"%d", fZoomLevel], [NSString stringWithFormat:@"%d", i], [NSString stringWithFormat:@"%d", j], [dataArray objectAtIndex:j+i*fXCount]];
                    }
                        break;
                    case 1: {
                        sql = [NSString stringWithFormat:@"INSERT INTO FogoDataMatrix%d (fZoomLevel, fXIndex, fYIndex, fData) values(?, ?, ?, ?)", fZoomLevel];
                        [_fogoDataDB executeUpdate:sql, [NSString stringWithFormat:@"%d", fZoomLevel], [NSString stringWithFormat:@"%d", i], [NSString stringWithFormat:@"%d", j], [dataArray objectAtIndex:j+i*fXCount]];
                    }
                        break;
                    case 2: {
                        sql = [NSString stringWithFormat:@"INSERT INTO FogoDataMatrix%d (fZoomLevel, fIndex, fData, fColCount) values(?, ?, ?, ?)", fZoomLevel];
                        int fIndex = j+i*fXCount;
                        [_fogoDataDB executeUpdate:sql, [NSString stringWithFormat:@"%d", fZoomLevel], [NSString stringWithFormat:@"%d", fIndex], [dataArray objectAtIndex:fIndex], [NSString stringWithFormat:@"%d", fXCount]];
                    }
                        break;
                    default:
                        break;
                }
                
                if ([_fogoDataDB hadError]) {
                    NSLog(@"Fail to insert FogoDataMatrix to FogoDataMatrix.db, %d, %@", [_fogoDataDB lastErrorCode], [_fogoDataDB lastErrorMessage]);
                    result = NO;
                    return result;
                }
                result = YES;
            }
        }
        
        [_fogoDataDB commit];
        [_fogoDataDB close];
    }
    return result;
}

@end
