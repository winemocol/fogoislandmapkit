//
//  FogoIslandDBManager.m
//  FogoIslandMapKit
//
//  Created by sy2036 on 2017-01-30.
//  Copyright © 2017 winemocol. All rights reserved.
//

#import "FogoIslandDBManager.h"
#import <FMDB/FMDatabase.h>
#import <FMDB/FMDatabaseQueue.h>




@interface FogoIslandDBManager () {
    int fOption;
}

@property (nonatomic, copy) NSString *fogoDataDBPath;
@property (nonatomic, strong) FMDatabase *fogoDataDB;
@property (nonatomic, strong) FMDatabaseQueue *fogoDataDBQueue;

@end

@implementation FogoIslandDBManager

- (id)initWithOption:(int)option {
    if ((self = [super init])) {
        fOption = option;
        self.fogoDataDBPath = [[NSString getDocumentDirectory] stringByAppendingPathComponent:DB_FILEPATH];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if ([fileManager fileExistsAtPath:_fogoDataDBPath]) {
            self.fogoDataDB = [FMDatabase databaseWithPath:_fogoDataDBPath];
            if ([_fogoDataDB open]) {
                [_fogoDataDB setShouldCacheStatements:YES];
            } else
                NSLog(@"Fail to open DB FogoDataMatrix.db!!");
            self.fogoDataDBQueue = [FMDatabaseQueue databaseQueueWithPath:_fogoDataDBPath];
        } else
            [self initDatabase];
        [self attachDBFromDownload];
    }
    return self;
}

- (void)initDatabase {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:_fogoDataDBPath]) {
        self.fogoDataDB = [FMDatabase databaseWithPath:_fogoDataDBPath];
        if (![_fogoDataDB open]) return;
        [_fogoDataDB setShouldCacheStatements:YES];
        [self createFogoImagePost];
    }
}

- (void)attachDBFromDownload {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *defaultDBPath =  [[NSString getCacheDocument] stringByAppendingString:@"FogoDataMatrix.db"];
    if (![fileManager fileExistsAtPath:defaultDBPath]) {
        NSString *sql1 = @"ATTACH ?";
        [_fogoDataDB executeUpdate:sql1, defaultDBPath];
    }
}

- (void)createFogoImagePost {
    NSString *sql1 = @"CREATE TABLE IF NOT EXISTS FogoImagePost (fLat float, fLng float, fImageAddr text, fID INTEGER PRIMARY KEY AUTOINCREMENT)";
    [_fogoDataDB executeUpdate:sql1];
    NSString *sql2 = @"CREATE TABLE IF NOT EXISTS FogoImageReview (fID int, fAuthor text, fPostDate date, fContext)";
    [_fogoDataDB executeUpdate:sql2];
}

#pragma -
#pragma Database Manipulation
#pragma Query Color Block
- (NSArray *)querySizeofDataBYZoomLevel:(int)fZoomLevel {
    __block NSMutableArray *sizeArr = [[NSMutableArray alloc] init];
    if ([_fogoDataDB open]) {
        int width = 0;
        int height = 0;
        NSString *sql = [NSString stringWithFormat:@"SELECT fColCount FROM FogoDataMatrix%d WHERE fIndex = 0", fZoomLevel];
        FMResultSet *resultSet = [_fogoDataDB executeQuery:sql];
        while([resultSet next]) {
            width = [[resultSet stringForColumn:@"fColCount"] intValue];
        }
        sql = [NSString stringWithFormat:@"SELECT COUNT(*) FROM FogoDataMatrix%d", fZoomLevel];
        resultSet = [_fogoDataDB executeQuery:sql];
        while([resultSet next]) {
            height = [[resultSet stringForColumn:@"COUNT(*)"] intValue]/width;
        }
        [sizeArr addObject:[NSString stringWithFormat:@"%d", width]];
        [sizeArr addObject:[NSString stringWithFormat:@"%d", height]];
    }
    return [NSArray arrayWithArray:sizeArr];
}


- (NSArray *)queryFogoIlandDataMatrixByZoomLevel:(int)fZoomLevel withXStartIndex:(int)xSIndex andYStartIndex:(int)ySIndex andXEndCount:(int)xECount andYEndCount:(int)yECount {
    __block NSMutableArray *fDataArray = [[NSMutableArray alloc] init];
    if ([_fogoDataDB open]) {
        NSString *sql;
        FMResultSet *resultSet;
        switch (fOption) {
            case 0: {
                sql = @"SELECT fData FROM FogoDataMatrix WHERE fZoomLevel = ? AND fXIndex >= ? AND fXIndex < ? AND fYIndex >= ? AND fYIndex < ?";
                resultSet = [_fogoDataDB executeQuery:sql, [NSString stringWithFormat:@"%d", fZoomLevel], [NSString stringWithFormat:@"%d", xSIndex], [NSString stringWithFormat:@"%d", xSIndex+xECount], [NSString stringWithFormat:@"%d", ySIndex], [NSString stringWithFormat:@"%d", ySIndex+yECount]];
                while([resultSet next]) {
                    [fDataArray addObject:[resultSet stringForColumn:@"fData"]];
                }
            }
                break;
            case 1: {
                sql = [NSString stringWithFormat:@"SELECT fData FROM FogoDataMatrix%d WHERE fXIndex >= ? AND fXIndex < ? AND fYIndex >= ? AND fYIndex < ?", fZoomLevel];
                resultSet = [_fogoDataDB executeQuery:sql, [NSString stringWithFormat:@"%d", fZoomLevel], [NSString stringWithFormat:@"%d", xSIndex], [NSString stringWithFormat:@"%d", xSIndex+xECount], [NSString stringWithFormat:@"%d", ySIndex], [NSString stringWithFormat:@"%d", ySIndex+yECount]];
                while([resultSet next]) {
                    [fDataArray addObject:[resultSet stringForColumn:@"fData"]];
                }
            }
                break;
            case 2: {
                sql = [NSString stringWithFormat:@"SELECT fColCount FROM FogoDataMatrix%d WHERE fIndex = 0", fZoomLevel];
                
                FMResultSet *colResult = [_fogoDataDB executeQuery:sql];
                int colCount = 0;
                while([colResult next]) {
                    colCount = [[colResult stringForColumn:@"fColCount"] intValue];
                }
                [colResult close];
                NSDate *date1 = [NSDate date];
                /*
                for (int i = ySIndex; i < yECount+ySIndex; i++) {
                    sql = [NSString stringWithFormat:@"SELECT fData FROM FogoDataMatrix%d WHERE fIndex >= ? AND fIndex < ?", fZoomLevel];
                    resultSet = [_fogoDataDB executeQuery:sql, [NSString stringWithFormat:@"%d", xSIndex+colCount*i], [NSString stringWithFormat:@"%d", xSIndex+xECount+colCount*i]];
                    while([resultSet next]) {
                        [fDataArray addObject:[resultSet stringForColumnIndex:0]];
                    }
                }*/
                sql = [NSString stringWithFormat:@"CREATE INDEX i1 on FogoDataMatrix%d(fIndex, fData)", fZoomLevel];
                [_fogoDataDB executeQuery:sql];
                sql = @"";
                
                
                [_fogoDataDBQueue inDatabase:^(FMDatabase *db) {
                    for (int i = ySIndex; i <= yECount+ySIndex; i++) {
                        //                    NSString *sql1 = [NSString stringWithFormat:@"SELECT fData FROM FogoDataMatrix%d WHERE fIndex >= %d AND fIndex < %d;", fZoomLevel, xSIndex+colCount*i, xSIndex+xECount+colCount*i];
                        NSString *sql1 = @"";
                        
                        //  Test for separate table 1 into 28 parts. 100 rows as a single table.
                        if (1 == fZoomLevel) {
                            int fPart = i/100 + 1;
                            sql1 = [NSString stringWithFormat:@"SELECT fData FROM FogoDataMatrix%d_%d WHERE fIndex >= %d limit %d;", fZoomLevel, fPart, xSIndex+colCount*i, xECount];
                        } else if (2 == fZoomLevel) {
                            int fPart = i/230 + 1;
                            sql1 = [NSString stringWithFormat:@"SELECT fData FROM FogoDataMatrix%d_%d WHERE fIndex >= %d limit %d;", fZoomLevel, fPart, xSIndex+colCount*i, xECount];
                        } else if (3 == fZoomLevel) {
                            int fPart = i/350 + 1;
                            sql1 = [NSString stringWithFormat:@"SELECT fData FROM FogoDataMatrix%d_%d WHERE fIndex >= %d limit %d;", fZoomLevel, fPart, xSIndex+colCount*i, xECount];
                        } else {
                            sql1 = [NSString stringWithFormat:@"SELECT fData FROM FogoDataMatrix%d WHERE fIndex >= %d limit %d;", fZoomLevel, xSIndex+colCount*i, xECount];
                        }

                        FMResultSet *resultSet = [db executeQuery:sql1];
                        while ([resultSet next]) {
                            [fDataArray addObject:[resultSet stringForColumn:@"fData"]];
                        }
                    }
                }];
                
//                for (int i = ySIndex; i <= yECount+ySIndex; i++) {
////                    NSString *sql1 = [NSString stringWithFormat:@"SELECT fData FROM FogoDataMatrix%d WHERE fIndex >= %d AND fIndex < %d;", fZoomLevel, xSIndex+colCount*i, xSIndex+xECount+colCount*i];
//                    NSString *sql1 = [NSString stringWithFormat:@"SELECT fData FROM FogoDataMatrix%d WHERE fIndex >= %d limit %d;", fZoomLevel, xSIndex+colCount*i, xECount];
//                    sql = [sql stringByAppendingString:sql1];
//                    
//                }
//                BOOL success;
//                success = [_fogoDataDB executeStatements:sql withResultBlock:^int(NSDictionary *dictionary) {
//                    [fDataArray addObject:[dictionary objectForKey:@"fData"]];
//                    /*
//                     * This is only be used while the Block function is utilized.
//                    if (endBlock) {
//                        endBlock(YES, @"Success", dictionary);
//                    }*/
//                    return 0;
//                }];
                
                NSDate *date2 = [NSDate date];
                NSTimeInterval a = [date2 timeIntervalSince1970] - [date1 timeIntervalSince1970];
                NSLog(@"查询数据用时%.3f秒",a);
            }
                break;
            default:
                break;
        }
        
        [resultSet close];
//        [_fogoDataDB close];
    }
    return [NSArray arrayWithArray:fDataArray];
}

/**
 Insert Data Matrix by the Option

 @param dataDic Data dictionary.
 @param fZoomLevel Zoom-In Level from 1 to 8. 1 means the maximun zoom in.
 @return Insert Success or Fail.
 */
- (BOOL)insertFogoIslandDataMatrix:(NSDictionary *)dataDic andZoomLevel:(int)fZoomLevel {
    bool result = NO;
    if ([_fogoDataDB open]) {
        [_fogoDataDB beginTransaction];
        NSArray *dataArray = [dataDic objectForKey:@"data"];
        int fXCount = [[dataDic objectForKey:@"fXCount"] intValue];
        int fYCount = [[dataDic objectForKey:@"fYCount"] intValue];
        for (int i = 0; i < fYCount; i++) {
            for (int j = 0; j < fXCount; j++) {
                NSString *sql;
                switch (fOption) {
                    case 0: {
                        sql = @"INSERT INTO FogoDataMatrix (fZoomLevel, fXIndex, fYIndex, fData) values(?, ?, ?, ?)";
                        [_fogoDataDB executeUpdate:sql, [NSString stringWithFormat:@"%d", fZoomLevel], [NSString stringWithFormat:@"%d", i], [NSString stringWithFormat:@"%d", j], [dataArray objectAtIndex:j+i*fXCount]];
                    }
                        break;
                    case 1: {
                        sql = [NSString stringWithFormat:@"INSERT INTO FogoDataMatrix%d (fZoomLevel, fXIndex, fYIndex, fData) values(?, ?, ?, ?)", fZoomLevel];
                        [_fogoDataDB executeUpdate:sql, [NSString stringWithFormat:@"%d", fZoomLevel], [NSString stringWithFormat:@"%d", i], [NSString stringWithFormat:@"%d", j], [dataArray objectAtIndex:j+i*fXCount]];
                    }
                        break;
                    case 2: {
                        sql = [NSString stringWithFormat:@"INSERT INTO FogoDataMatrix%d (fZoomLevel, fIndex, fData, fColCount) values(?, ?, ?, ?)", fZoomLevel];
                        int fIndex = j+i*fXCount;
                        [_fogoDataDB executeUpdate:sql, [NSString stringWithFormat:@"%d", fZoomLevel], [NSString stringWithFormat:@"%d", fIndex], [dataArray objectAtIndex:fIndex], [NSString stringWithFormat:@"%d", fXCount]];
                    }
                        break;
                    default:
                        break;
                }
                
                if ([_fogoDataDB hadError]) {
                    NSLog(@"Fail to insert FogoDataMatrix to FogoDataMatrix.db, %d, %@", [_fogoDataDB lastErrorCode], [_fogoDataDB lastErrorMessage]);
                    
                    result = NO;
                    return result;
                }
                result = YES;
            }
        }
        
        [_fogoDataDB commit];
        [_fogoDataDB close];
    }
    return result;
}

#pragma Query Images
- (NSArray *)queryAllPostImages {
    __block NSMutableArray *imagesArr = [[NSMutableArray alloc] init];
    if ([_fogoDataDB open]) {
        NSString *sql = @"SELECT * FROM FogoImagePost";
        FMResultSet *resultSet = [_fogoDataDB executeQuery:sql];
        while([resultSet next]) {
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            [dic setObject:[resultSet stringForColumn:@"fLat"] forKey:@"fLat"];
            [dic setObject:[resultSet stringForColumn:@"fLng"] forKey:@"fLng"];
            [dic setObject:[resultSet stringForColumn:@"fImageAddr"] forKey:@"fImageAddr"];
            [dic setObject:[resultSet stringForColumn:@"fID"] forKey:@"fID"];
            [imagesArr addObject:dic];
        }
    }
    return [NSArray arrayWithArray:imagesArr];
}

- (NSArray *)queryImageReviewByID:(int)imageID {
    __block NSMutableArray *imagesArrID = [[NSMutableArray alloc] init];
    if ([_fogoDataDB open]) {
        NSString *sql = @"SELECT * FROM FogoImageReview WHERE fID = ?";
        FMResultSet *resultSet = [_fogoDataDB executeQuery:sql, [NSString stringWithFormat:@"%d", imageID]];
        while([resultSet next]) {
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
            [dic setObject:[resultSet stringForColumn:@"fAuthor"] forKey:@"fAuthor"];
            [dic setObject:[resultSet stringForColumn:@"fPostDate"] forKey:@"fPostDate"];
            [dic setObject:[resultSet stringForColumn:@"fContext"] forKey:@"fContext"];
            [imagesArrID addObject:dic];
        }
    }
    return [NSArray arrayWithArray:imagesArrID];
}

- (BOOL)insertPostImagesWithImagePath:(NSString *)imagePath andLat:(double)fLat andLng:(double)fLng {
    bool result = NO;
    if ([_fogoDataDB open]) {
        [_fogoDataDB beginTransaction];
        
        NSString *sql = @"INSERT INTO FogoImagePost (fLat, fLng, fImageAddr) values(?, ?, ?)";
        [_fogoDataDB executeUpdate:sql, [NSString stringWithFormat:@"%f", fLat], [NSString stringWithFormat:@"%f", fLng], imagePath];
        
        if ([_fogoDataDB hadError]) {
            NSLog(@"Fail to insert FogoDataMatrix to FogoDataMatrix.db, %d, %@", [_fogoDataDB lastErrorCode], [_fogoDataDB lastErrorMessage]);
            
            result = NO;
            return result;
        }
        result = YES;
        [_fogoDataDB commit];
    }
    return result;
}

@end
