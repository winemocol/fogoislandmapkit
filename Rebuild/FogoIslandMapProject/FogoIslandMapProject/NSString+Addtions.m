//
//  NSString+Addtions.m
//  HKToilet
//
//  Created by Roselifeye on 14-5-6.
//  Copyright (c) 2014年 Roselifeye. All rights reserved.
//

#import "NSString+Addtions.h"
//#import "GTMBase64.h"

static NSString *UUID = nil;

@implementation NSString (Addtions)

+ (NSString *)getDocumentDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentDir = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    
    return documentDir;
}

+ (NSString *)getCacheDocument
{
    NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
    
    return documentDir;
}

+ (NSString *)getBundleDirectoryWithFileName:(NSString *)fileName andFileType:(NSString *)fileType
{
    NSString *defaultDBPath =  [[NSBundle mainBundle] pathForResource:fileName ofType:fileType];
    return defaultDBPath;
}


+ (NSString *)getAppVersion
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    //return [NSString getBuildVersion];
}

+ (NSString *)getBuildVersion
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
}

//- (NSString *)encodeWithBASE64
//{
//    NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
//    NSString *encoded = [[NSString alloc] initWithData:[GTMBase64 encodeData:data] encoding:NSUTF8StringEncoding];
//    return encoded;
//}
//
//- (NSString *)decodeWithBASE64
//{
//    NSString *decoded = [[NSString alloc] initWithData:[GTMBase64 decodeString:self] encoding:NSUTF8StringEncoding];
//    return decoded;
//}

- (BOOL)isEmail
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:self];
}

- (BOOL)isPhoneNumber
{
    NSString * regex = @"^[0-9]{11,11}$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [pred evaluateWithObject:self];
}

@end
