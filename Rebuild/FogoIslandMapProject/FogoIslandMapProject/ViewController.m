//
//  ViewController.m
//  FogoIslandMapProject
//
//  Created by Sipan Ye on 2017/4/13.
//  Copyright © 2017年 Sipan. All rights reserved.
//

#import "ViewController.h"
#import <MapKit/MapKit.h>
#import <AFNetworking.h>
#import "FogoPolygon.h"
#import "FogoDataProcess.h"
#import "MKMapView+ZoomLevel.h"
#import "SPCameraViewController.h"
#import "FogoPopoverViewController.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "FogoContentViewController.h"
#import "FogoComposePopoverViewController.h"

#import "FogoCoordinateQuadTree.h"
#import "FogoClustersAnnotationView.h"
#import "FogoClusterAnnotation.h"

#import "YDWaveLoadingView.h"
#import "FogoServiceTermsNotifyView.h"

@interface ViewController () <MKMapViewDelegate, UIPopoverPresentationControllerDelegate> {
    
    FogoDataProcess *fogoDProcess;
    float offsetAngleCos;
    float offsetAngleSin;
    
    NSMutableArray *annotationPins;
    
    UIPopoverPresentationController *popover;
    
    FogoClusterAnnotation *selectedAnnotation;
    
    //  Default NO;
    BOOL isDisplayHeatMap;
}

@property (weak, nonatomic) IBOutlet MKMapView *fogoIslandMap;

@property (strong, nonatomic) FogoCoordinateQuadTree *coordinateQuadTree;

@property (nonatomic, strong) YDWaveLoadingView *loadingView;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    BOOL isAgreeTerms = [[[NSUserDefaults standardUserDefaults] objectForKey:@"IsAgreeTerms"] boolValue];
    if (!isAgreeTerms) {
        isDisplayHeatMap = NO;
        [self setupTermsNotifyView];
    }
    [self setup];
    
    [FogoGlobalInstance shareInstance].networkManager.blockDownloading = ^(CGFloat downloadPercentage){
        _loadingView.downloadingLabel.text = [NSString stringWithFormat:@"%.2f%%", downloadPercentage];
    };
    [FogoGlobalInstance shareInstance].networkManager.blockDownloadEnd = ^(BOOL result, NSString *resultMessage, NSString *comTag){
        [self updateMap];
        [_loadingView stopLoading];
    };
}

- (void)setupTermsNotifyView {
    NSString *termsPath = [NSString getBundleDirectoryWithFileName:@"terms" andFileType:@"txt"];
    NSString *contentStr = [NSString stringWithContentsOfFile:termsPath encoding:NSUTF8StringEncoding error:nil];
    [FogoServiceTermsNotifyView showSTNotificationWithTitle:@"Title" andContent:contentStr blockConfirmEnd:^(){
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"IsAgreeTerms"];
        [self updateMap];
    }];
}

- (void)setup {
    [self.fogoIslandMap setMapRegionAnimated:YES];
    [self obtainRotation];
    fogoDProcess = [[FogoDataProcess alloc] initWithRotateAngleCos:offsetAngleCos andSin:offsetAngleSin];
    
    self.coordinateQuadTree = [[FogoCoordinateQuadTree alloc] init];
    self.coordinateQuadTree.mapView = self.fogoIslandMap;
    [self.coordinateQuadTree buildTreeWithDBManager:fogoDProcess];
    
    float angle = acos(offsetAngleCos);
    MKMapCamera *newCamera = [_fogoIslandMap camera];
    [newCamera setHeading:angle];
}

/**
 Obtain rotation of the given coordiantes compared with Lat and Lng.
 */
- (void)obtainRotation {
    CGPoint aPos = [_fogoIslandMap convertCoordinate:CLLocationCoordinate2DMake(FOGO_NW_LAT, FOGO_NW_LNG) toPointToView:_fogoIslandMap];
    CGPoint bPos = [_fogoIslandMap convertCoordinate:CLLocationCoordinate2DMake(FOGO_NW_LAT, FOGO_NE_LNG) toPointToView:_fogoIslandMap];
    CGPoint cPos = [_fogoIslandMap convertCoordinate:CLLocationCoordinate2DMake(FOGO_NE_LAT, FOGO_NE_LNG) toPointToView:_fogoIslandMap];
    
    double ab = sqrt(pow(aPos.x - bPos.x, 2) + pow(aPos.y - bPos.y, 2));
    double ac = sqrt(pow(aPos.x - cPos.x, 2) + pow(aPos.y - cPos.y, 2));
    double bc = sqrt(pow(bPos.x - cPos.x, 2) + pow(bPos.y - cPos.y, 2));
    
    offsetAngleCos = (powf(ab,2) + powf(ac,2) - powf(bc,2))/(2*ab*ac);
    offsetAngleSin = sqrtf(1 - powf(offsetAngleCos, 2));
}

- (IBAction)cameraBtnClicked:(id)sender {
    [self hideContentOfAnnotation];
    SPCameraViewController *cameraView = [self.storyboard instantiateViewControllerWithIdentifier:@"SPCameraViewController"];
    cameraView.blockCameraEnd = ^(BOOL result, NSString *resultMsg, NSString *imagePath) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        if (result) {
            [fogoDProcess inserNewAnnotationWithImagePath:imagePath andCoordinate:CLLocationCoordinate2DMake(49.669868, -54.076631)];
            [_coordinateQuadTree buildTreeWithDBManager:fogoDProcess];
            [self updateMapAnnotations];
            hud.detailsLabel.text = @"Photo Added";
        } else
            hud.detailsLabel.text = @"Cancelled";
        hud.detailsLabel.font = [UIFont systemFontOfSize:18.0f];
        hud.margin = 10.f;
        hud.offset = CGPointMake(hud.offset.x, 150.f);
        hud.removeFromSuperViewOnHide = YES;
        
        [hud hideAnimated:YES afterDelay:1.0];
    };
    [self.navigationController pushViewController:cameraView animated:YES];
}

- (IBAction)mapLayerBtnClicked:(id)sender {
    if (MKMapTypeSatellite == _fogoIslandMap.mapType) {
        _fogoIslandMap.mapType = MKMapTypeStandard;
    } else
        _fogoIslandMap.mapType = MKMapTypeSatellite;
}

- (IBAction)downLoadData:(UIButton *)sender {
    NSLog(@"Download Data Clicked");
//    isDisplayHeatMap = !isDisplayHeatMap;
//    [self updateMap];
    
//    sender.selected = !sender.isSelected;
    
//    if (sender.selected) {
    [[FogoGlobalInstance shareInstance].networkManager DownloadDatabase];
    _loadingView = [YDWaveLoadingView loadingView];
//    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [_loadingView startLoading];
    });
}

#pragma mark -
#pragma mark - MKMapViewDelegate
/**
 Update the overlays of the map view.
 
 @param fOverlays New Overlays.
 */
- (void)updateMapViewOverlaysWith:(NSArray *)fOverlays {
    [self.fogoIslandMap removeOverlays:_fogoIslandMap.overlays];
    [self.fogoIslandMap addOverlays:fOverlays];
}

- (void)updateMap {
    if (isDisplayHeatMap) {
        __block NSArray *overlays;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            overlays = [fogoDProcess refreshDataWithMapView:self.fogoIslandMap];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self updateMapViewOverlaysWith:overlays];
            });
        });
    } else {
        [self.fogoIslandMap removeOverlays:_fogoIslandMap.overlays];
    }
}

/**
 Draw the overlay renderer

 @param mapView Fogo Map View
 @param overlay overlay for drawing
 @return Drawing Renderer/View
 */
- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {
    if ([overlay isKindOfClass:[MKPolyline class]]) {
        MKPolylineRenderer *originalRectangleLineView = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
        originalRectangleLineView.fillColor = [UIColor redColor];
        originalRectangleLineView.strokeColor = [UIColor redColor];
        originalRectangleLineView.lineWidth = 5;
        return originalRectangleLineView;
        
    } else if([overlay isKindOfClass:[FogoPolygon class]]) {
        MKPolygonRenderer *fOverlayRenderer = [[MKPolygonRenderer alloc] initWithOverlay:overlay];
        [fOverlayRenderer setAlpha:.5f];
        /*
         * Generate color of the each cell.
         */
//        CGFloat blueH = 0.67;
        CGFloat redH = 0.f;
        CGFloat greenH = 0.43;
        float correspondingHue;
        float possValue = ((FogoPolygon *)overlay).possValue;
        if (0 < possValue && 0.7 > possValue) {
            correspondingHue = ((0.7-possValue)/0.7)*(greenH-redH);
        } else if (possValue >= 0.7) {
            correspondingHue = redH;
        } else {
            correspondingHue = redH;
            [fOverlayRenderer setAlpha:0];
        }
        UIColor *bgColor = [UIColor colorWithHue:correspondingHue saturation:1.f brightness:1.f alpha:1.f];
        
        fOverlayRenderer.fillColor = bgColor;
        return fOverlayRenderer;
    } else
        return nil;
}

/**
 While the map is zoom or move, the data for drawing will be updated by querying the database.

 @param mapView Fogo Map View
 @param animated YES
 */
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    [self updateMap];
    [self updateMapAnnotations];
}


- (void)updateMapAnnotations {
    double scale = self.fogoIslandMap.bounds.size.width / self.fogoIslandMap.visibleMapRect.size.width;
    NSArray *annotations = [self.coordinateQuadTree clusteredAnnotationsWithinMapRect:self.fogoIslandMap.visibleMapRect withZoomScale:scale];
    [self updateMapViewAnnotationsWithAnnotations:annotations];
}

/**
 Drawing the annotaions on the map.
 * MKUserLocation: Do not process.
 * REVClusterPin: The cluster annotation. It will have two status：
    * "Single": Only one annotation.
    * "Cluster": Multiple annotaions merged.
 * CalloutViewAnnotation: Callout annotation when the REVClusterPin is clicked.

 @param MKAnnotationView Different types of annotation
 @return Drawing Annotation View
 */
#pragma MKAnnotation
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    } else if ([annotation isKindOfClass:[FogoClusterAnnotation class]]) {
        static NSString *const TBAnnotatioViewReuseID = @"TBAnnotatioViewReuseID";
        
        FogoClustersAnnotationView *annotationView = (FogoClustersAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:TBAnnotatioViewReuseID];
        
        if (!annotationView) {
            annotationView = [[FogoClustersAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:TBAnnotatioViewReuseID];
        }
        
        annotationView.canShowCallout = YES;
//        annotationView.count = [(FogoClusterAnnotation *)annotation count];
        [annotationView setClusterText:[NSString stringWithFormat:@"%lu", [(FogoClusterAnnotation *)annotation count]] andImage:[((FogoClusterAnnotation *)annotation).imagePaths lastObject]];
        
        
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        [rightButton addTarget:self action:@selector(displayContentOfAnnotation) forControlEvents:UIControlEventTouchUpInside];
        annotationView.rightCalloutAccessoryView = rightButton;
        
        return annotationView;

    }
    return nil;
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray<MKAnnotationView *> *)views {
    for (UIView *view in views)
        [self addBounceAnnimationToView:view];
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    NSLog(@"Clicked");
    selectedAnnotation = (FogoClusterAnnotation *)view.annotation;
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    [self hideContentOfAnnotation];
}


- (NSArray *)fakeDetailData {
    NSMutableArray *dataArray = [[NSMutableArray alloc] init];
    NSDictionary *detailDic1 = [[NSDictionary alloc] initWithObjectsAndKeys:@"2017/04/03 12:03:01", @"Time", @"Tom", @"Name", @"I agreed!", @"Text", nil];
    NSDictionary *detailDic2 = [[NSDictionary alloc] initWithObjectsAndKeys:@"2017/05/03 09:33:33", @"Time", @"Allen", @"Name", @"What lens did you use?", @"Text", nil];
    NSDictionary *detailDic3 = [[NSDictionary alloc] initWithObjectsAndKeys:@"2017/05/01 12:06:01", @"Time", @"Json", @"Name", @"Nice photo!", @"Text", nil];
    [dataArray addObject:detailDic1];
    [dataArray addObject:detailDic2];
    [dataArray addObject:detailDic3];
    return [NSArray arrayWithArray:dataArray];
}

- (void)displayContentOfAnnotation {
    [UIView animateWithDuration:0.5f animations:^{
        self.splitViewController.preferredDisplayMode = UISplitViewControllerDisplayModeAutomatic;
    } completion:^(BOOL finished) {
        //        self.splitViewController.preferredDisplayMode = UISplitViewControllerDisplayModeAutomatic;
        UINavigationController *nav = [self.splitViewController.viewControllers firstObject];
        FogoContentViewController *fogoContentView = (FogoContentViewController *)nav.topViewController;
        FogoClusterAnnotation *ann = selectedAnnotation;
        
        [fogoContentView setImageArr:ann.imagePaths];
        [fogoContentView setDetailArray:[self fakeDetailData]];
    }];
}

- (void)showComposePopoverViewWithImagePath:(NSString *)imagePath {
    FogoComposePopoverViewController *popoverView = [self.storyboard instantiateViewControllerWithIdentifier:@"FogoComposePopoverViewController"];
    CGRect frame = CGRectMake(0, 0, 10, 10);
    frame.origin = self.view.center;
    popoverView.modalPresentationStyle = UIModalPresentationPopover;
    popoverView.popoverPresentationController.sourceView = self.view;
    popoverView.popoverPresentationController.sourceRect = frame;
    popoverView.popoverPresentationController.permittedArrowDirections = 0;
    self.preferredContentSize = CGSizeMake(360, 300);
    [popoverView setImagePath:imagePath];
    [self presentViewController:popoverView animated:YES completion:nil];
}

- (void)hideContentOfAnnotation {
    [UIView animateWithDuration:0.5f animations:^{
        self.splitViewController.preferredDisplayMode = UISplitViewControllerDisplayModePrimaryHidden;
    } completion:^(BOOL finished) {
        //        self.splitViewController.preferredDisplayMode = UISplitViewControllerDisplayModePrimaryHidden;
        UINavigationController *nav = [self.splitViewController.viewControllers firstObject];
        [nav popToRootViewControllerAnimated:NO];
    }];
}

#pragma mark - Animation for annotations.
- (void)addBounceAnnimationToView:(UIView *)view {
    CAKeyframeAnimation *bounceAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    bounceAnimation.values = @[@(0.05), @(1.1), @(0.9), @(1)];
    bounceAnimation.duration = 0.6;
    NSMutableArray *timingFunctions = [[NSMutableArray alloc] initWithCapacity:bounceAnimation.values.count];
    for (NSUInteger i = 0; i < bounceAnimation.values.count; i++) {
        [timingFunctions addObject:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    }
    [bounceAnimation setTimingFunctions:timingFunctions.copy];
    bounceAnimation.removedOnCompletion = NO;
    [view.layer addAnimation:bounceAnimation forKey:@"bounce"];
}

- (void)updateMapViewAnnotationsWithAnnotations:(NSArray *)annotations {
    NSMutableSet *before = [NSMutableSet setWithArray:self.fogoIslandMap.annotations];
    [before removeObject:[self.fogoIslandMap userLocation]];
    NSSet *after = [NSSet setWithArray:annotations];
    
    NSMutableSet *toKeep = [NSMutableSet setWithSet:before];
    [toKeep intersectSet:after];
    
    NSMutableSet *toAdd = [NSMutableSet setWithSet:after];
    [toAdd minusSet:toKeep];
    
    NSMutableSet *toRemove = [NSMutableSet setWithSet:before];
    [toRemove minusSet:after];
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self.fogoIslandMap addAnnotations:[toAdd allObjects]];
        [self.fogoIslandMap removeAnnotations:[toRemove allObjects]];
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
