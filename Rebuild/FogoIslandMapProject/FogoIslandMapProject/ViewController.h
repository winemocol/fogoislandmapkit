//
//  ViewController.h
//  FogoIslandMapProject
//
//  Created by Sipan Ye on 2017/4/13.
//  Copyright © 2017年 Sipan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

- (void)showComposePopoverViewWithImagePath:(NSString *)imagePath;

@end

