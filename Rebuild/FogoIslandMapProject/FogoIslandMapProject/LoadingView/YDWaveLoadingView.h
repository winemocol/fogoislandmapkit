//
//  YDWaveLoadingView.h
//  WaterWave
//
//  Created by liangwei on 16/7/8.
//  Copyright © 2016 liangwei. All rights reserved.
//  Updated by Sipan on 18/2/22
//  UIWindow initial approach
//  Blur Effect
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@interface YDWaveLoadingView : UIView

@property (nonatomic, strong) UILabel *downloadingLabel;

+ (instancetype)loadingView;

- (void)startLoading;

- (void)stopLoading;

@end
NS_ASSUME_NONNULL_END
