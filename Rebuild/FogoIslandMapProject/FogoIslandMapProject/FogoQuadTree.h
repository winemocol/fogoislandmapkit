//
//  FogoQuadTree.h
//  FogoIslandMapProject
//
//  Created by Sipan Ye on 2017/4/28.
//  Copyright © 2017年 Sipan. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef struct FogoQuadTreeNodeData {
    double x;
    double y;
    void* data;
} FogoQuadTreeNodeData;
FogoQuadTreeNodeData FogoQuadTreeNodeDataMake(double x, double y, void* data);

typedef struct FogoBoundingBox {
    double x0; double y0;
    double xf; double yf;
} FogoBoundingBox;
FogoBoundingBox FogoBoundingBoxMake(double x0, double y0, double xf, double yf);

typedef struct quadTreeNode {
    struct quadTreeNode* northWest;
    struct quadTreeNode* northEast;
    struct quadTreeNode* southWest;
    struct quadTreeNode* southEast;
    FogoBoundingBox boundingBox;
    int bucketCapacity;
    FogoQuadTreeNodeData *points;
    int count;
} FogoQuadTreeNode;
FogoQuadTreeNode* FogoQuadTreeNodeMake(FogoBoundingBox boundary, int bucketCapacity);

void FogoFreeQuadTreeNode(FogoQuadTreeNode* node);

bool FogoBoundingBoxContainsData(FogoBoundingBox box, FogoQuadTreeNodeData data);
bool FogoBoundingBoxIntersectsBoundingBox(FogoBoundingBox b1, FogoBoundingBox b2);

typedef void(^FogoQuadTreeTraverseBlock)(FogoQuadTreeNode* currentNode);
void FogoQuadTreeTraverse(FogoQuadTreeNode* node, FogoQuadTreeTraverseBlock block);

typedef void(^FogoDataReturnBlock)(FogoQuadTreeNodeData data);
void FogoQuadTreeGatherDataInRange(FogoQuadTreeNode* node, FogoBoundingBox range, FogoDataReturnBlock block);

bool FogoQuadTreeNodeInsertData(FogoQuadTreeNode* node, FogoQuadTreeNodeData data);
FogoQuadTreeNode* FogoQuadTreeBuildWithData(FogoQuadTreeNodeData *data, int count, FogoBoundingBox boundingBox, int capacity);
