//
//  FogoNetworkManager.m
//  FogoIslandMapProject
//
//  Created by  sy2036 on 2018-02-24.
//  Copyright © 2018 Sipan. All rights reserved.
//

#import "FogoNetworkManager.h"
#import <AFNetworking.h>
#import "FogoURLParametersManager.h"

#define BASE_URL (@"http://127.0.0.1:8000")

@implementation FogoNetworkManager

/**
 ** User Login Mod.
 **/
- (void)LoginForTokenWithUserID:(NSString *)userID
                       password:(NSString *)password {
    //  GCD asynchronous.
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(){
        AFHTTPSessionManager *requestManager = [AFHTTPSessionManager manager];
        requestManager.responseSerializer = [AFHTTPResponseSerializer serializer];
        NSString *passwordMD5 = password;
        NSDictionary *paraDic = [FogoURLParametersManager getLoginParametersWithUserID:userID
                                                                            password:passwordMD5];
        [requestManager POST:[NSString stringWithFormat:@"%@userLoginApi",BASE_URL] parameters:paraDic success:^(NSURLSessionDataTask *task, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
            NSData *responseData = responseObject;
            //  If there is no data back.
            if (!responseData && _blockLoginEnd) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (_blockLoginEnd) {
                        _blockLoginEnd(NO, @"Network unavaliable. Please try again！", nil);
                    }
                });
                return ;
            }
            
            NSDictionary *responseDic = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:nil];
            NSInteger status = [[responseDic objectForKey:@"status"] integerValue];
            NSString *describe = [responseDic objectForKey:@"describe"];
            
            // Call back Main Queue and the block.
            dispatch_async(dispatch_get_main_queue(), ^{
                if (status == RESULT_END_FREE) {
                    //                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_END_FREE
                    //                                                                    object:describe];
                    //                return ;
                }
                if (_blockLoginEnd) {
                    _blockLoginEnd((status == RESULT_OK), describe, nil);
                }
            });
        } failure:^(NSURLSessionDataTask *operation, NSError *error) {
            NSLog(@"error");
        }];
    });
}

- (void)DownloadDatabase {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(){
        __block NSInteger fileLength = 0;
        __block NSInteger currentLength = 0;
        NSString *path = [[NSString getCacheDocument] stringByAppendingPathComponent:@"FogoDataMatrix.db"];
        currentLength = [self fileLengthForPath:path];

        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *requestManager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        
        __block NSFileHandle *fileHandle;
        NSURL *url = [NSURL URLWithString:@"http://127.0.0.1:8000/downloadfogodb"];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        NSString *range = [NSString stringWithFormat:@"bytes=%zd-", currentLength];
        [request setValue:range forHTTPHeaderField:@"Range"];
        NSURLSessionDataTask *downloadTask = [requestManager dataTaskWithRequest:request completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
            NSLog(@"dataTaskWithRequest");
            currentLength = 0;
            fileLength = 0;
            [fileHandle closeFile];
            fileHandle = nil;
            if (_blockDownloadEnd) {
                _blockDownloadEnd(YES, @"Finished", nil);
            }
        }];
        
        [requestManager setDataTaskDidReceiveResponseBlock:^NSURLSessionResponseDisposition(NSURLSession * _Nonnull session, NSURLSessionDataTask * _Nonnull dataTask, NSURLResponse * _Nonnull response) {
            fileLength = response.expectedContentLength + currentLength;
            NSLog(@"File downloaded to: %@",path);
            NSFileManager *manager = [NSFileManager defaultManager];
            if (![manager fileExistsAtPath:path]) {
                [manager createFileAtPath:path contents:nil attributes:nil];
            }
            fileHandle = [NSFileHandle fileHandleForWritingAtPath:path];
            return NSURLSessionResponseAllow;
        }];
        
        [requestManager setDataTaskDidReceiveDataBlock:^(NSURLSession * _Nonnull session, NSURLSessionDataTask * _Nonnull dataTask, NSData * _Nonnull data) {
            NSLog(@"setDataTaskDidReceiveDataBlock");
            [fileHandle seekToEndOfFile];
            [fileHandle writeData:data];
            currentLength += data.length;
            dispatch_async(dispatch_get_main_queue(), ^{
                if (_blockDownloading) {
                    _blockDownloading(100.0 * currentLength / fileLength);
                }
            });
        }];
        [downloadTask resume];
    });
}

- (NSInteger)fileLengthForPath:(NSString *)path {
    NSInteger fileLength = 0;
    NSFileManager *fileManager = [[NSFileManager alloc] init]; // default is not thread safe
    if ([fileManager fileExistsAtPath:path]) {
        NSError *error = nil;
        NSDictionary *fileDict = [fileManager attributesOfItemAtPath:path error:&error];
        if (!error && fileDict) {
            fileLength = [fileDict fileSize];
        }
    }
    return fileLength;
}

@end
