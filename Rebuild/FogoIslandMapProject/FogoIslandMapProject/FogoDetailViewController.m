//
//  FogoDetailViewController.m
//  FogoIslandMapProject
//
//  Created by Sipan Ye on 2017/4/28.
//  Copyright © 2017年 Sipan. All rights reserved.
//

#import "FogoDetailViewController.h"

@interface FogoDetailViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *detailImage;
@end

@implementation FogoDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [_detailImage setImage:[UIImage imageWithContentsOfFile:_imagePath]];
}

- (void)setImagePath:(NSString *)imagePath {
    _imagePath = imagePath;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
