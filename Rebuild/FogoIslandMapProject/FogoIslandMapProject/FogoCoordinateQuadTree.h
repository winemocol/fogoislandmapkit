//
//  FogoCoordinateQuadTree.h
//  FogoIslandMapProject
//
//  Created by Sipan Ye on 2017/4/28.
//  Copyright © 2017年 Sipan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "FogoQuadTree.h"
#import "FogoDataProcess.h"

@interface FogoCoordinateQuadTree : NSObject

@property (assign, nonatomic) FogoQuadTreeNode *root;
@property (strong, nonatomic) MKMapView *mapView;

- (void)buildTreeWithDBManager:(FogoDataProcess *)fDataProcess;
- (NSArray *)clusteredAnnotationsWithinMapRect:(MKMapRect)rect withZoomScale:(double)zoomScale;

@end
