//
//  FogoContentTableViewCell.h
//  FogoIslandMapProject
//
//  Created by  sy2036 on 2017-04-28.
//  Copyright © 2017 Sipan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FogoContentTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
@end
