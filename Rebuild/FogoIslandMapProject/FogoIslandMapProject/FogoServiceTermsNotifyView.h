//
//  FogoServiceTermsNotifyView.h
//  FogoIslandMapProject
//
//  Created by  sy2036 on 2018-02-22.
//  Copyright © 2018 Sipan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FogoServiceTermsNotifyView : UIView

+ (void)showSTNotificationWithTitle:(NSString *)title andContent:(NSString *)contentStr blockConfirmEnd:(SPBlock)blockConfirmEnd;


@end
