//
//  FogoClustersAnnotationView.m
//  FogoIslandMapProject
//
//  Created by Sipan Ye on 2017/4/28.
//  Copyright © 2017年 Sipan. All rights reserved.
//

#import "FogoClustersAnnotationView.h"

#define RGBA(r,g,b,a)   [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]

CGPoint FogoRectCenter(CGRect rect) {
    return CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect));
}

CGRect FogoCenterRect(CGRect rect, CGPoint center) {
    CGRect r = CGRectMake(center.x - rect.size.width/2.0,
                          center.y - rect.size.height/2.0,
                          rect.size.width,
                          rect.size.height);
    return r;
}

static CGFloat const FogoScaleFactorAlpha = 0.3;
static CGFloat const FogoScaleFactorBeta = 0.4;

CGFloat FogoScaledValueForValue(CGFloat value) {
    return 1.0 / (1.0 + expf(-1 * FogoScaleFactorAlpha * powf(value, FogoScaleFactorBeta)));
}

@interface FogoClustersAnnotationView () {
    UILabel *label;
    UIImageView *clusterImage;
    UIImageView *photoImage;
}

@property (strong, nonatomic) UILabel *countLabel;

@end

@implementation FogoClustersAnnotationView

- (id)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
//        [self setupLabel];
//        [self setCount:1];
        [self initPhotoImage];
        [self initTextLabel];
    }
    return self;
}

- (void)initTextLabel {
    clusterImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 26, 26)];
    clusterImage.layer.borderColor = [UIColor whiteColor].CGColor;
    clusterImage.layer.borderWidth = 2.f;
    clusterImage.layer.cornerRadius = 13.f;
    [clusterImage setBackgroundColor:RGBA(88, 186, 248, 1)];
    [self addSubview:clusterImage];
    label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 26, 26)];
    [clusterImage addSubview:label];
    label.textColor = [UIColor whiteColor];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:11];
    label.textAlignment = NSTextAlignmentCenter;
    label.shadowColor = [UIColor blackColor];
    label.shadowOffset = CGSizeMake(0,-1);
}

- (void)initPhotoImage {
    photoImage = [[UIImageView alloc] initWithFrame:CGRectMake(8, 8, 90, 90)];
    photoImage.contentMode = UIViewContentModeScaleAspectFill;
    photoImage.clipsToBounds = YES;
    photoImage.layer.borderColor = [UIColor whiteColor].CGColor;
    photoImage.layer.borderWidth = 3.f;
    photoImage.layer.cornerRadius = 10.f;
    [self addSubview:photoImage];
}



- (void)setClusterText:(NSString *)text andImage:(NSString *)imagePath {
    [photoImage setImage:[UIImage imageWithContentsOfFile:imagePath]];
    if (0 == [text integerValue]) {
        [label setHidden:YES];
        [clusterImage setHidden:YES];
    }
    
//    text = @"1111";
    label.text = text;
    self.frame = photoImage.frame;
    
    CGRect newBounds = CGRectMake(0, 0, roundf(44 * FogoScaledValueForValue([text integerValue])), 26);
    clusterImage.frame = newBounds;
    label.frame = newBounds;
    [self setNeedsDisplay];
}


@end
