//
//  FogoContentViewController.m
//  FogoIslandMapProject
//
//  Created by  sy2036 on 2017-04-27.
//  Copyright © 2017 Sipan. All rights reserved.
//

#import "FogoContentViewController.h"
#import "FogoContentTableViewCell.h"
#import "FogoDetailViewController.h"
#import "UIImage+Addtions.h"
#import "ViewController.h"

@interface FogoContentViewController () <UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource> {
}


@end

@implementation FogoContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _imageScroll.tag = 1000;
    [self addImageToScroll];
    self.title = @"Details";
}

- (void)addImageToScroll {
    for (int i = 0; i < [_imageArr count]; i++) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(i*_imageScroll.frame.size.width, 0, _imageScroll.frame.size.width, _imageScroll.frame.size.height)];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        
        [imageView setImage:[UIImage imageWithContentsOfFile:[_imageArr objectAtIndex:i]]];
        
        [_imageScroll addSubview:imageView];
        [_imageScroll setContentSize:CGSizeMake((i+1)*_imageScroll.frame.size.width, _imageScroll.frame.size.height)];
        [_pageControl setNumberOfPages:i+1];
    }
}

- (void)setImageArr:(NSArray *)imageArr {
    _imageArr = imageArr;
    [self addImageToScroll];
}

- (void)setDetailDic:(NSDictionary *)detailDic {
    _detailDic = detailDic;
    [_detailTable reloadData];
}

- (void)setDetailArray:(NSArray *)detailArray {
    _detailArray = detailArray;
    [_detailTable reloadData];
}

- (IBAction)composeBtnClicked:(id)sender {
    UINavigationController *nav = [self.splitViewController.viewControllers lastObject];
    ViewController *fogoMapView = (ViewController *)nav.topViewController;
    [fogoMapView showComposePopoverViewWithImagePath:[_imageArr objectAtIndex:_pageControl.currentPage]];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (1000 == scrollView.tag) {
        NSInteger index = fabs(scrollView.contentOffset.x) / scrollView.frame.size.width;
        [_pageControl setCurrentPage:index];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FogoContentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    NSDictionary *dic = [_detailArray objectAtIndex:indexPath.row];
    cell.nameLabel.text = [dic objectForKey:@"Name"];
    cell.timeLabel.text = [dic objectForKey:@"Text"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormatter dateFromString:[dic objectForKey:@"Time"]];
    NSDictionary *durationDic = [self compare:date to:[NSDate date]];
    cell.durationLabel.text = [NSString stringWithFormat:@"%@ %@ ago", [durationDic objectForKey:@"Time"], [durationDic objectForKey:@"TimeName"]];
    return cell;
}

- (NSDictionary *)compare:(NSDate *)startTime to:(NSDate *)endTime {
    
    // 当前日历
    NSCalendar *calendar = [NSCalendar currentCalendar];
    // 需要对比的时间数据
    NSCalendarUnit unit = NSCalendarUnitYear | NSCalendarUnitMonth
    | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    // 对比时间差
    NSDateComponents *dateCom = [calendar components:unit fromDate:startTime toDate:endTime options:0];
    if (0 < dateCom.year)
        return @{@"TimeName":@"years", @"Time":[NSString stringWithFormat:@"%ld", dateCom.year]};
    else if (0 < dateCom.month)
        return @{@"TimeName":@"month", @"Time":[NSString stringWithFormat:@"%ld", dateCom.month]};
    else if (0 < dateCom.day)
        return @{@"TimeName":@"days", @"Time":[NSString stringWithFormat:@"%ld", dateCom.day]};
    else if (0 < dateCom.hour)
        return @{@"TimeName":@"hours", @"Time":[NSString stringWithFormat:@"%ld", dateCom.hour]};
    else
        return @{@"TimeName":@"minutes", @"Time":[NSString stringWithFormat:@"%ld", dateCom.minute]};
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_detailArray count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Cell did selected.");
    FogoDetailViewController *detailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FogoDetailViewController"];
    [detailVC setImagePath:[_imageArr objectAtIndex:_pageControl.currentPage]];
    [self.navigationController pushViewController:detailVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
