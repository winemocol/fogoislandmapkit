//
//  FogoDetailViewController.h
//  FogoIslandMapProject
//
//  Created by Sipan Ye on 2017/4/28.
//  Copyright © 2017年 Sipan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FogoDetailViewController : UIViewController

@property (retain, nonatomic) NSString *imagePath;

@end
