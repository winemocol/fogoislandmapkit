//
//  FogoGlobalInstance.m
//  FogoIslandMapProject
//
//  Created by  sy2036 on 2018-02-24.
//  Copyright © 2018 Sipan. All rights reserved.
//

#import "FogoGlobalInstance.h"

@implementation FogoGlobalInstance

+ (FogoGlobalInstance *)shareInstance {
    static FogoGlobalInstance *globalData = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        globalData = [[FogoGlobalInstance alloc] init];
    });
    return globalData;
}

- (id)init {
    self = [super init];
    if (self) {
        _networkManager = [[FogoNetworkManager alloc] init];
    }
    return self;
}

@end
