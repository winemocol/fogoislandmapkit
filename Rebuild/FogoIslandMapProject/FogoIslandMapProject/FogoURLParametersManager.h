//
//  FogoURLParametersManager.h
//  FogoIslandMapProject
//
//  Created by  sy2036 on 2018-02-24.
//  Copyright © 2018 Sipan. All rights reserved.
//

#import <Foundation/Foundation.h>


/**
 ** By Sipan
 ** The type number need to be discussed with the server side.
 **/
typedef NS_ENUM(NSInteger, RequestResultType) {
    RESULT_ERROE_MUTILOGIN = -1,
    RESULT_ERROE = 0,
    RESULT_OK = 1,
    
    RESULT_END_FREE = 11,
};

@interface FogoURLParametersManager : NSObject

// User login URL Parameter API
+ (NSDictionary *)getLoginParametersWithUserID:(NSString *)userID
                                      password:(NSString *)password;

// Annotations Data Query URL Parameter API:
+ (NSDictionary *)getAnnotationsURLParameterWithUserID:(NSString *)userID;

// User infomation Update URL Parameter API:
+ (NSDictionary *)getEditPersonInfoURLParameterWithUserID:(NSString *)userID;


@end
