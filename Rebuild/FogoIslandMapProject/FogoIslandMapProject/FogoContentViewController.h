//
//  FogoContentViewController.h
//  FogoIslandMapProject
//
//  Created by  sy2036 on 2017-04-27.
//  Copyright © 2017 Sipan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FogoContentViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIScrollView *imageScroll;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UITableView *detailTable;

@property (retain, nonatomic) NSArray *imageArr;
@property (retain, nonatomic) NSDictionary *detailDic;
@property (retain, nonatomic) NSArray *detailArray;


@end
