//
//  FogoIslandDBManager.h
//  FogoIslandMapKit
//
//  Created by sy2036 on 2017-01-30.
//  Copyright © 2017 winemocol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FogoIslandDBManager : NSObject

- (NSArray *)queryFogoIlandDataMatrixByZoomLevel:(int)fZoomLevel withXStartIndex:(int)xSIndex andYStartIndex:(int)ySIndex andXEndCount:(int)xECount andYEndCount:(int)yECount;

- (NSArray *)querySizeofDataBYZoomLevel:(int)fZoomLevel;

- (BOOL)insertFogoIslandDataMatrix:(NSDictionary *)dataDic andZoomLevel:(int)fZoomLevel;


- (NSArray *)queryAllPostImages;
- (NSArray *)queryImageReviewByID:(int)imageID;
- (BOOL)insertPostImagesWithImagePath:(NSString *)imagePath andLat:(double)fLat andLng:(double)fLng;

- (void)attachDBFromDownload;

/**
 Init Option

 @param option Option 0 means that all data will store in a single table, with 2D-Array.
               Option 1 means that all data will store in multiple tables via their zoom-in level, with 2D-Array.
               Option 2 means that all data will store in multiple tables via their zoom-in level, with 1D-Array.
 @return self
 */
- (id)initWithOption:(int)option;

@end
