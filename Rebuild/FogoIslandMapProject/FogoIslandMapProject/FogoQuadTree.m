//
//  FogoQuadTree.m
//  FogoIslandMapProject
//
//  Created by Sipan Ye on 2017/4/28.
//  Copyright © 2017年 Sipan. All rights reserved.
//

#import "FogoQuadTree.h"

FogoQuadTreeNodeData FogoQuadTreeNodeDataMake(double x, double y, void* data) {
    FogoQuadTreeNodeData d; d.x = x; d.y = y; d.data = data;
    return d;
}

FogoBoundingBox FogoBoundingBoxMake(double x0, double y0, double xf, double yf) {
    FogoBoundingBox bb; bb.x0 = x0; bb.y0 = y0; bb.xf = xf; bb.yf = yf;
    return bb;
}

FogoQuadTreeNode* FogoQuadTreeNodeMake(FogoBoundingBox boundary, int bucketCapacity) {
    FogoQuadTreeNode* node = malloc(sizeof(FogoQuadTreeNode));
    node->northWest = NULL;
    node->northEast = NULL;
    node->southWest = NULL;
    node->southEast = NULL;
    
    node->boundingBox = boundary;
    node->bucketCapacity = bucketCapacity;
    node->count = 0;
    node->points = malloc(sizeof(FogoQuadTreeNodeData) * bucketCapacity);
    
    return node;
}

#pragma mark - Bounding Box Functions

bool FogoBoundingBoxContainsData(FogoBoundingBox box, FogoQuadTreeNodeData data) {
    bool containsX = box.x0 <= data.x && data.x <= box.xf;
    bool containsY = box.y0 <= data.y && data.y <= box.yf;
    
    return containsX && containsY;
}

bool FogoBoundingBoxIntersectsBoundingBox(FogoBoundingBox b1, FogoBoundingBox b2) {
    return (b1.x0 <= b2.xf && b1.xf >= b2.x0 && b1.y0 <= b2.yf && b1.yf >= b2.y0);
}

#pragma mark - Quad Tree Functions

void FogoQuadTreeNodeSubdivide(FogoQuadTreeNode* node) {
    FogoBoundingBox box = node->boundingBox;
    
    double xMid = (box.xf + box.x0) / 2.0;
    double yMid = (box.yf + box.y0) / 2.0;
    
    FogoBoundingBox northWest = FogoBoundingBoxMake(box.x0, box.y0, xMid, yMid);
    node->northWest = FogoQuadTreeNodeMake(northWest, node->bucketCapacity);
    
    FogoBoundingBox northEast = FogoBoundingBoxMake(xMid, box.y0, box.xf, yMid);
    node->northEast = FogoQuadTreeNodeMake(northEast, node->bucketCapacity);
    
    FogoBoundingBox southWest = FogoBoundingBoxMake(box.x0, yMid, xMid, box.yf);
    node->southWest = FogoQuadTreeNodeMake(southWest, node->bucketCapacity);
    
    FogoBoundingBox southEast = FogoBoundingBoxMake(xMid, yMid, box.xf, box.yf);
    node->southEast = FogoQuadTreeNodeMake(southEast, node->bucketCapacity);
}

bool FogoQuadTreeNodeInsertData(FogoQuadTreeNode* node, FogoQuadTreeNodeData data) {
    if (!FogoBoundingBoxContainsData(node->boundingBox, data))
        return false;
    
    if (node->count < node->bucketCapacity) {
        node->points[node->count++] = data;
        return true;
    }
    
    if (node->northWest == NULL)
        FogoQuadTreeNodeSubdivide(node);
    
    if (FogoQuadTreeNodeInsertData(node->northWest, data)) return true;
    if (FogoQuadTreeNodeInsertData(node->northEast, data)) return true;
    if (FogoQuadTreeNodeInsertData(node->southWest, data)) return true;
    if (FogoQuadTreeNodeInsertData(node->southEast, data)) return true;
    
    return false;
}

void FogoQuadTreeGatherDataInRange(FogoQuadTreeNode* node, FogoBoundingBox range, FogoDataReturnBlock block) {
    if (!FogoBoundingBoxIntersectsBoundingBox(node->boundingBox, range))
        return;
    
    for (int i = 0; i < node->count; i++)
        if (FogoBoundingBoxContainsData(range, node->points[i]))
            block(node->points[i]);
    
    if (node->northWest == NULL)
        return;
    
    FogoQuadTreeGatherDataInRange(node->northWest, range, block);
    FogoQuadTreeGatherDataInRange(node->northEast, range, block);
    FogoQuadTreeGatherDataInRange(node->southWest, range, block);
    FogoQuadTreeGatherDataInRange(node->southEast, range, block);
}

void FogoQuadTreeTraverse(FogoQuadTreeNode* node, FogoQuadTreeTraverseBlock block) {
    block(node);
    
    if (node->northWest == NULL)
        return;
    
    FogoQuadTreeTraverse(node->northWest, block);
    FogoQuadTreeTraverse(node->northEast, block);
    FogoQuadTreeTraverse(node->southWest, block);
    FogoQuadTreeTraverse(node->southEast, block);
}

FogoQuadTreeNode* FogoQuadTreeBuildWithData(FogoQuadTreeNodeData *data, int count, FogoBoundingBox boundingBox, int capacity) {
    FogoQuadTreeNode* root = FogoQuadTreeNodeMake(boundingBox, capacity);
    for (int i = 0; i < count; i++)
        FogoQuadTreeNodeInsertData(root, data[i]);
    
    return root;
}

void FogoFreeQuadTreeNode(FogoQuadTreeNode* node) {
    if (node->northWest != NULL) FogoFreeQuadTreeNode(node->northWest);
    if (node->northEast != NULL) FogoFreeQuadTreeNode(node->northEast);
    if (node->southWest != NULL) FogoFreeQuadTreeNode(node->southWest);
    if (node->southEast != NULL) FogoFreeQuadTreeNode(node->southEast);
    
    for (int i=0; i < node->count; i++)
        free(node->points[i].data);
    
    free(node->points);
    free(node);
}

