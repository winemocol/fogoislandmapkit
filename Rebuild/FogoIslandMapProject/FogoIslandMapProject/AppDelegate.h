//
//  AppDelegate.h
//  FogoIslandMapProject
//
//  Created by Sipan Ye on 2017/4/13.
//  Copyright © 2017年 Sipan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,strong) UISplitViewController * splitViewController;

@end

