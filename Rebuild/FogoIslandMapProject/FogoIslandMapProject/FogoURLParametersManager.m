//
//  FogoURLParametersManager.m
//  FogoIslandMapProject
//
//  Created by  sy2036 on 2018-02-24.
//  Copyright © 2018 Sipan. All rights reserved.
//

#import "FogoURLParametersManager.h"

@implementation FogoURLParametersManager

+ (NSDictionary *)getLoginParametersWithUserID:(NSString *)userID
                                      password:(NSString *)password {
    NSError *error;
    return [NSDictionary dictionaryWithObjectsAndKeys:@"json", @"output",
            userID, @"userid",
            password, @"pwd", nil];
}

// Annotations Data Query URL Parameter API:
+ (NSDictionary *)getAnnotationsURLParameterWithUserID:(NSString *)userID {
    return [NSDictionary dictionaryWithObjectsAndKeys:@"json", @"output",
            userID, @"userid", nil];
}

// User infomation Update URL Parameter API:
+ (NSDictionary *)getEditPersonInfoURLParameterWithUserID:(NSString *)userID {
    return [NSDictionary dictionaryWithObjectsAndKeys:@"json", @"output",
            userID, @"userid", nil];
}

@end
