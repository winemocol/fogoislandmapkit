//
//  FogoServiceTermsNotifyView.m
//  FogoIslandMapProject
//
//  Created by  sy2036 on 2018-02-22.
//  Copyright © 2018 Sipan. All rights reserved.
//

#import "FogoServiceTermsNotifyView.h"



#define DEFAULT_MAX_HEIGHT SCREEN_HEIGHT/3*2

const CGFloat AnimationTimeInterval = 0.6f;

@interface FogoServiceTermsNotifyView ()

@property (nonatomic, assign) NSString *title;
@property (nonatomic, assign) NSString *contentStr;

@property (nonatomic, copy) SPBlock tempBlockEnd;

@end

@implementation FogoServiceTermsNotifyView

+ (void)showSTNotificationWithTitle:(NSString *)title andContent:(NSString *)contentStr blockConfirmEnd:(SPBlock)blockConfirmEnd {
    FogoServiceTermsNotifyView *notifView = [[FogoServiceTermsNotifyView alloc] initWithTitle:title andContent:contentStr];
    [[[UIApplication sharedApplication] keyWindow] addSubview:notifView];
    notifView.tempBlockEnd = blockConfirmEnd;
}

- (instancetype)initWithTitle:(NSString *)title andContent:(NSString *)contentStr {
    self = [super init];
    if (self) {
        self.title = title;
        self.contentStr = contentStr;
        [self setup];
    }
    return self;
}

- (void)setup {
    self.frame = [UIScreen mainScreen].bounds;
    self.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.3/1.0];
    CGFloat descHeight = [self sizeofString:_contentStr font:[UIFont systemFontOfSize:16.f] maxSize:CGSizeMake(self.frame.size.width - 500 - 50, 1000)].height;
    CGFloat realHeight = descHeight + 18+80+15+28+10+20+50+10;
    CGFloat maxHeight = DEFAULT_MAX_HEIGHT;
    
    BOOL scrollEnabled = YES;
    
    if (realHeight > DEFAULT_MAX_HEIGHT) {
        scrollEnabled = YES;
        descHeight = DEFAULT_MAX_HEIGHT - 314;
    } else
        maxHeight = realHeight;
    
    UIView *bgView = [[UIView alloc] init];
    bgView.center = self.center;
    bgView.bounds = CGRectMake(0, 0, self.frame.size.width - 40, maxHeight+18);
    [self addSubview:bgView];
    
    UIView *alertView = [[UIView alloc]initWithFrame:CGRectMake(300, 18, bgView.frame.size.width - 600, maxHeight)];
    alertView.backgroundColor = [UIColor whiteColor];
    alertView.layer.masksToBounds = YES;
    alertView.layer.cornerRadius = 4.0f;
    [bgView addSubview:alertView];
    
    UIImageView *appIcon = [[UIImageView alloc]initWithFrame:CGRectMake((alertView.frame.size.width - 80)/2, 20, 80, 80)];
    appIcon.image = [UIImage imageNamed:@"caribou_circle"];
    [alertView addSubview:appIcon];

    UILabel *tittleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 15 + CGRectGetMaxY(appIcon.frame), alertView.frame.size.width, 28)];
    tittleLabel.font = [UIFont boldSystemFontOfSize:18];
    tittleLabel.textAlignment = NSTextAlignmentCenter;
    tittleLabel.text = [NSString stringWithFormat:@"%@", _title];
    [alertView addSubview:tittleLabel];
    
    UITextView *descTextView = [[UITextView alloc]initWithFrame:CGRectMake(25, 10 + CGRectGetMaxY(tittleLabel.frame), alertView.frame.size.width - 50, descHeight)];
    descTextView.font = [UIFont systemFontOfSize:16.f];
    descTextView.textContainer.lineFragmentPadding = 0;
    descTextView.textContainerInset = UIEdgeInsetsMake(0, 0, 0, 0);
    descTextView.text = _contentStr;
    descTextView.editable = NO;
    descTextView.selectable = NO;
    descTextView.scrollEnabled = scrollEnabled;
    descTextView.showsVerticalScrollIndicator = scrollEnabled;
    descTextView.showsHorizontalScrollIndicator = NO;
    [alertView addSubview:descTextView];
    
    if (scrollEnabled) {
        [descTextView flashScrollIndicators];
    }
    
    UIButton *confirmButton = [UIButton buttonWithType:UIButtonTypeSystem];
    confirmButton.backgroundColor = [UIColor colorWithRed:0.3176 green:0.5098 blue:0.2784 alpha:1.f];
    confirmButton.frame = CGRectMake(40, CGRectGetHeight(alertView.frame) - 60, alertView.frame.size.width - 80, 50);
    confirmButton.clipsToBounds = YES;
    confirmButton.layer.cornerRadius = 2.0f;
    [confirmButton addTarget:self action:@selector(dismissAlert) forControlEvents:UIControlEventTouchUpInside];
    [confirmButton setTitle:@"Confirm" forState:UIControlStateNormal];
    [confirmButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [alertView addSubview:confirmButton];
    
    [self showWithAlert:bgView];
}

- (CGSize)sizeofString:(NSString *)string font:(UIFont *)font maxSize:(CGSize)maxSize {
    return [string boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:font} context:nil].size;
}

- (void)showWithAlert:(UIView*)alert {
    CAKeyframeAnimation* animation = [CAKeyframeAnimation animationWithKeyPath:@"transform"];
    animation.duration = AnimationTimeInterval;
    NSMutableArray *values = [NSMutableArray array];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.2, 1.2, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(0.9, 0.9, 1.0)]];
    [values addObject:[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0, 1.0, 1.0)]];
    animation.values = values;
    [alert.layer addAnimation:animation forKey:nil];
}

- (void)dismissAlert {
    [UIView animateWithDuration:AnimationTimeInterval animations:^{
        self.transform = (CGAffineTransformMakeScale(1.5, 1.5));
        self.backgroundColor = [UIColor clearColor];
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"IsAgreeTerms"];
        _tempBlockEnd();
    } ];
}

@end
