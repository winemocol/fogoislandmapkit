//
//  FogoNetworkManager.h
//  FogoIslandMapProject
//
//  Created by  sy2036 on 2018-02-24.
//  Copyright © 2018 Sipan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FogoNetworkManager : NSObject

@property (nonatomic, copy) SPBlockRequestEnd blockLoginEnd;

@property (nonatomic, copy) SPBlockRequestEnd blockDownloadEnd;
@property (nonatomic, copy) SPFloatBlock blockDownloading;

/**
 ** User Login Mod.
 **/
- (void)LoginForTokenWithUserID:(NSString *)userID
                       password:(NSString *)password;

/**
 ** Download Database
 **/
- (void)DownloadDatabase;

@end
