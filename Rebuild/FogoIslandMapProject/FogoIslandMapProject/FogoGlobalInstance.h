//
//  FogoGlobalInstance.h
//  FogoIslandMapProject
//
//  Created by  sy2036 on 2018-02-24.
//  Copyright © 2018 Sipan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FogoNetworkManager.h"

@interface FogoGlobalInstance : NSObject

@property (nonatomic, copy) FogoNetworkManager *networkManager;

+ (FogoGlobalInstance *)shareInstance;

@end
