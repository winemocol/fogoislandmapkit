//
//  FogoComposePopoverViewController.h
//  FogoIslandMapProject
//
//  Created by  sy2036 on 2017-05-03.
//  Copyright © 2017 Sipan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FogoComposePopoverViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UITextField *commentTextField;

@property (retain, nonatomic) NSString *imagePath;

@end
