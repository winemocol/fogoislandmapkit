//
//  FogoDataProcess.h
//  FogoIslandMapProject
//
//  Created by Sipan Ye on 2017/4/13.
//  Copyright © 2017年 Sipan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface FogoDataProcess : NSObject

@property (nonatomic, strong) NSArray *fDataArray;

@property (nonatomic, assign) float latOffset;
@property (nonatomic, assign) float lngOffset;


/**
 Init

 
 @param rotatedC Cosin Value of the rotation angle.
 @param rotatedS Sin Value of teh rotation angle.
 @return self.
 */
- (instancetype)initWithRotateAngleCos:(float)rotatedC andSin:(float)rotatedS;

/**
 Refresh the Map overlays data.
 
 @param mapView MapView
 @return Array of FogoOverlays objects.
 */

/**
  Refresh the Map overlays data.

 @param mapView MapView
 @return Array of FogoOverlays objects.
 */
- (NSArray *)refreshDataWithMapView:(MKMapView *)mapView;


- (NSArray *)queryAnnotations;
- (NSArray *)queryAnnotationDetailWithID:(int)fID;
- (void)inserNewAnnotationWithImagePath:(NSString *)imagePath andCoordinate:(CLLocationCoordinate2D)fCoordinate;

@end
