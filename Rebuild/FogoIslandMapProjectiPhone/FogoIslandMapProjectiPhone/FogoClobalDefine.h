//
//  FogoClobalDefine.h
//  FogoIslandMapProjectiPhone
//
//  Created by Sipan Ye on 2017/5/15.
//  Copyright © 2017年 Sipan. All rights reserved.
//

#ifndef FogoClobalDefine_h
#define FogoClobalDefine_h


#define RGBA(r,g,b,a)   [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]



#define FOGO_NW_LAT 49.78354343767712
#define FOGO_NW_LNG -54.32582266065412
#define FOGO_SW_LAT 49.531878309117076
#define FOGO_SW_LNG -54.33958794258664
#define FOGO_NE_LAT 49.7745818637998
#define FOGO_NE_LNG -53.96249945939786
#define FOGO_SE_LAT 49.52299562709053
#define FOGO_SE_LNG -53.97812839986856

#endif /* FogoClobalDefine_h */
