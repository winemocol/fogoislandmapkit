//
//  FogoPolygon.m
//  FogoIslandMapKit
//
//  Created by Sipan Ye on 2017/3/14.
//  Copyright © 2017年 winemocol. All rights reserved.
//

#import "FogoPolygon.h"

@implementation FogoPolygon

- (instancetype)initWithPossibility:(float)possValue
                         andBGImage:(UIImage *)bgImage
                       andMapPoints:(MKMapPoint *)points
                           andCount:(NSUInteger)count {
    self = [super init];
    if (self) {
        self.possValue = possValue;
        self.bgImage = bgImage;
    }
    return self;
}


@end
