//
//  FogoPopoverViewController.m
//  FogoIslandMapProject
//
//  Created by  sy2036 on 2017-04-26.
//  Copyright © 2017 Sipan. All rights reserved.
//

#import "FogoPopoverViewController.h"

@interface FogoPopoverViewController () <UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource> {
    NSInteger cellIndex;
    NSString *selectedImagePath;
}

@end

@implementation FogoPopoverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.preferredContentSize = CGSizeMake(240, 360);
    _imageScroll.tag = 1000;
    cellIndex = 10000;
    [self addImageToScroll];
}

- (void)addImageToScroll {
    for (int i = 0; i < [_imageArr count]; i++) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(i*_imageScroll.frame.size.width, 0, _imageScroll.frame.size.width, _imageScroll.frame.size.height)];
        [imageView setImage:[UIImage imageWithContentsOfFile:[_imageArr objectAtIndex:i]]];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        [_imageScroll addSubview:imageView];
        [_imageScroll setContentSize:CGSizeMake((i+1)*_imageScroll.frame.size.width, _imageScroll.frame.size.height)];
        [_pageControl setNumberOfPages:i+1];
    }
}

- (void)setImageArr:(NSArray *)imageArr {
    _imageArr = imageArr;
    selectedImagePath = [_imageArr objectAtIndex:0];
}

- (void)setDetailDic:(NSDictionary *)detailDic {
    _detailDic = detailDic;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (1000 == scrollView.tag) {
        NSInteger index = fabs(scrollView.contentOffset.x) / scrollView.frame.size.width;
        [_pageControl setCurrentPage:index];
        selectedImagePath = [_imageArr objectAtIndex:index];
        [self setImageAndText];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    NSDictionary *dic = [_detailDic objectForKey:@"1"];
    cell.textLabel.text = [[dic allKeys] objectAtIndex:0];
    cell.detailTextLabel.text = [[dic allValues] objectAtIndex:0];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Cell did selected.");
    if (indexPath.row == cellIndex) {
        self.preferredContentSize = CGSizeMake(240, 360);
        cellIndex = 10000;
    } else {
        [self setImageAndText];
        self.preferredContentSize = CGSizeMake(480, 360);
        cellIndex = indexPath.row;
    }
}

- (void)setImageAndText {
    [_selectedImageView setImage:[UIImage imageWithContentsOfFile:selectedImagePath]];
    [_selectedDetailText setText:[NSString stringWithFormat:@"%@\n%@", [[[_detailDic objectForKey:@"1"] allKeys] objectAtIndex:0], [[[_detailDic objectForKey:@"1"] allValues]  objectAtIndex:0]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
