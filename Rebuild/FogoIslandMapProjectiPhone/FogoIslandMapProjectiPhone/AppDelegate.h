//
//  AppDelegate.h
//  FogoIslandMapProjectiPhone
//
//  Created by Sipan Ye on 2017/5/15.
//  Copyright © 2017年 Sipan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

