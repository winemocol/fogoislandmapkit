//
//  MKMapView+ZoomLevel.h
//  FogoIslandMapKit
//
//  Created by sy2036 on 2017-01-13.
//  Copyright © 2017 winemocol. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MKMapView (ZoomLevel)

- (void)setCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
                  zoomLevel:(NSUInteger)zoomLevel
                   animated:(BOOL)animated;

- (void)setMapRegionAnimated:(BOOL)animated;


- (double)pixelSpaceXToLongitude:(double)pixelX;

- (double)pixelSpaceYToLatitude:(double)pixelY;

- (double)longitudeToPixelSpaceX:(double)longitude;

- (double)latitudeToPixelSpaceY:(double)latitude;

@end
