//
//  SPCameraViewController.m
//  JadeArticle
//
//  Created by Roselifeye on 15/6/5.
//  Copyright (c) 2015年 Roselifeye. All rights reserved.
//

#import "SPCameraViewController.h"
#import "SPCameraView.h"

@interface SPCameraViewController () {
    SPCameraView *cameraView;
    
}
@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@end

@implementation SPCameraViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    cameraView = [[SPCameraView alloc] initWithFrame:self.view.frame positionDevice:CameraPositonBack andCameraButtonFrame:CGRectMake(CGRectGetWidth(self.view.frame)-20-80, CGRectGetHeight(self.view.frame)-20-80, 80, 80) andImageName:@"cameraBtn"];
    [self.view addSubview:cameraView];
    __weak SPCameraViewController *weakSelf = self;
    
    cameraView.blockCameraEnd = ^(BOOL result, NSString *resultMessage, id object){
        [weakSelf cameraEndWithResult:result andImagePath:object];
    };
    
    [self.view bringSubviewToFront:_backBtn];
}

- (void)cameraEndWithResult:(BOOL)result andImagePath:(NSString *)imagePath {
    if (_blockCameraEnd) {
        _blockCameraEnd(result, nil, imagePath);
    }
    [self goBack:nil];
}

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
