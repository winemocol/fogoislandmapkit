//
//  MKMapView+ZoomLevel.m
//  FogoIslandMapKit
//
//  Created by sy2036 on 2017-01-13.
//  Copyright © 2017 winemocol. All rights reserved.
//  Idea from http://troybrant.net/blog/2010/01/set-the-zoom-level-of-an-mkmapview/

#import "MKMapView+ZoomLevel.h"

#define MERCATOR_OFFSET 268435456
#define MERCATOR_RADIUS 85445659.44705395

#define FOGO_NW_LAT 49.78354343767712
#define FOGO_NW_LNG -54.32582266065412
#define FOGO_SW_LAT 49.531878309117076
#define FOGO_SW_LNG -54.33958794258664
#define FOGO_NE_LAT 49.7745818637998
#define FOGO_NE_LNG -53.96249945939786
#define FOGO_SE_LAT 49.52299562709053
#define FOGO_SE_LNG -53.97812839986856


#define TOP_LEFT_Y -54.32582266065412
#define BOTTOM_RIGHT_Y -53.96249945939786
#define TOP_LEFT_X 49.78354343767712
#define BOTTOM_RIGHT_X 49.531878309117076

@implementation MKMapView (ZoomLevel)

- (double)longitudeToPixelSpaceX:(double)longitude {
    return round(MERCATOR_OFFSET + MERCATOR_RADIUS * longitude * M_PI / 180.0);
}

- (double)latitudeToPixelSpaceY:(double)latitude {
    return round(MERCATOR_OFFSET - MERCATOR_RADIUS * logf((1 + sinf(latitude * M_PI / 180.0)) / (1 - sinf(latitude * M_PI / 180.0))) / 2.0);
}

- (double)pixelSpaceXToLongitude:(double)pixelX {
    return ((round(pixelX) - MERCATOR_OFFSET) / MERCATOR_RADIUS) * 180.0 / M_PI;
}

- (double)pixelSpaceYToLatitude:(double)pixelY {
    return (M_PI / 2.0 - 2.0 * atan(exp((round(pixelY) - MERCATOR_OFFSET) / MERCATOR_RADIUS))) * 180.0 / M_PI;
}

#pragma mark -
#pragma mark Helper methods

- (MKCoordinateSpan)coordinateSpanWithMapView:(MKMapView *)mapView
                             centerCoordinate:(CLLocationCoordinate2D)centerCoordinate
                                 andZoomLevel:(NSUInteger)zoomLevel {
    // convert center coordiate to pixel space
    double centerPixelX = [self longitudeToPixelSpaceX:centerCoordinate.longitude];
    double centerPixelY = [self latitudeToPixelSpaceY:centerCoordinate.latitude];
    
    NSLog(@"CenterPixelX: %f, CenterPixelY: %f", centerPixelX, centerPixelY);
    
    // determine the scale value from the zoom level
    NSInteger zoomExponent = 20 - zoomLevel;
    double zoomScale = pow(2, zoomExponent);
    
    // scale the map’s size in pixel space
    CGSize mapSizeInPixels = mapView.bounds.size;
    double scaledMapWidth = mapSizeInPixels.width * zoomScale;
    double scaledMapHeight = mapSizeInPixels.height * zoomScale;
    
    // figure out the position of the top-left pixel
    double topLeftPixelX = centerPixelX - (scaledMapWidth / 2);
    double topLeftPixelY = centerPixelY - (scaledMapHeight / 2);
    
    // find delta between left and right longitudes
    CLLocationDegrees minLng = [self pixelSpaceXToLongitude:topLeftPixelX];
    CLLocationDegrees maxLng = [self pixelSpaceXToLongitude:topLeftPixelX + scaledMapWidth];
    CLLocationDegrees longitudeDelta = maxLng - minLng;
    
    // find delta between top and bottom latitudes
    CLLocationDegrees minLat = [self pixelSpaceYToLatitude:topLeftPixelY];
    CLLocationDegrees maxLat = [self pixelSpaceYToLatitude:topLeftPixelY + scaledMapHeight];
    CLLocationDegrees latitudeDelta = -1 * (maxLat - minLat);
    
    // create and return the lat/lng span
    MKCoordinateSpan span = MKCoordinateSpanMake(latitudeDelta, longitudeDelta);
    return span;
}

- (MKCoordinateSpan)coordinateSpan {
    // find delta between left and right longitudes
    CLLocationDegrees longitudeDelta = BOTTOM_RIGHT_Y - TOP_LEFT_Y;
    
    // find delta between top and bottom latitudes
    CLLocationDegrees latitudeDelta = -1 * (BOTTOM_RIGHT_X - TOP_LEFT_X);
    
    // create and return the lat/lng span
    MKCoordinateSpan span = MKCoordinateSpanMake(latitudeDelta, longitudeDelta);
    return span;
}

#pragma mark -
#pragma mark Public methods

- (void)setCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
                  zoomLevel:(NSUInteger)zoomLevel
                   animated:(BOOL)animated {
    // clamp large numbers to 28
    zoomLevel = MIN(zoomLevel, 28);
    
    // use the zoom level to compute the region
    MKCoordinateSpan span = [self coordinateSpanWithMapView:self centerCoordinate:centerCoordinate andZoomLevel:zoomLevel];
    MKCoordinateRegion region = MKCoordinateRegionMake(centerCoordinate, span);
    
    // set the region like normal
    [self setRegion:region animated:animated];
}

- (void)setMapRegionAnimated:(BOOL)animated {
    MKCoordinateSpan span = [self coordinateSpan];
    MKCoordinateRegion region;
    region.center.latitude = TOP_LEFT_X + (BOTTOM_RIGHT_X - TOP_LEFT_X) * 0.5;
    region.center.longitude = TOP_LEFT_Y + (BOTTOM_RIGHT_Y - TOP_LEFT_Y) * 0.5;
    region.span = span;
    
    // set the region like normal
    [self setRegion:region animated:animated];
}

@end
