//
//  SPCameraViewController.h
//  JadeArticle
//
//  Created by Roselifeye on 15/6/5.
//  Copyright (c) 2015年 Roselifeye. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPBlockTypeDef.h"

@interface SPCameraViewController : UIViewController

@property (nonatomic, copy) SPBlockRequestEnd blockCameraEnd;

@end
