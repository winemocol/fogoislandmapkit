//
//  FogoCoordinateQuadTree.m
//  FogoIslandMapProject
//
//  Created by Sipan Ye on 2017/4/28.
//  Copyright © 2017年 Sipan. All rights reserved.
//

#import "FogoCoordinateQuadTree.h"
#import "FogoClusterAnnotation.h"
#import "NSString+Addtions.h"

typedef struct FogoPhotoInfo {
    char *imagePath;
    char *imageID;
} FogoPhotoInfo;

FogoQuadTreeNodeData FogoDataFromDic(NSDictionary *dic) {
    double latitude = [[dic objectForKey:@"fLat"] doubleValue];
    double longitude = [[dic objectForKey:@"fLng"] doubleValue];
    
    FogoPhotoInfo *imageInfo = malloc(sizeof(FogoPhotoInfo));
    
    NSString *imagePath = [[[NSString getDocumentDirectory] stringByAppendingPathComponent:[dic objectForKey:@"fImageAddr"]] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    imageInfo->imagePath = malloc(sizeof(char) * imagePath.length + 1);
    strncpy(imageInfo->imagePath, [imagePath UTF8String], imagePath.length + 1);
    
    NSString *imageID = [[dic objectForKey:@"fID"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    imageInfo->imageID = malloc(sizeof(char) * imageID.length + 1);
    strncpy(imageInfo->imageID, [imageID UTF8String], imageID.length + 1);
    
    return FogoQuadTreeNodeDataMake(latitude, longitude, imageInfo);
}

FogoBoundingBox FogoBoundingBoxForMapRect(MKMapRect mapRect) {
    CLLocationCoordinate2D topLeft = MKCoordinateForMapPoint(mapRect.origin);
    CLLocationCoordinate2D botRight = MKCoordinateForMapPoint(MKMapPointMake(MKMapRectGetMaxX(mapRect), MKMapRectGetMaxY(mapRect)));
    
    CLLocationDegrees minLat = botRight.latitude;
    CLLocationDegrees maxLat = topLeft.latitude;
    
    CLLocationDegrees minLon = topLeft.longitude;
    CLLocationDegrees maxLon = botRight.longitude;
    
    return FogoBoundingBoxMake(minLat, minLon, maxLat, maxLon);
}

MKMapRect FogoMapRectForBoundingBox(FogoBoundingBox boundingBox)
{
    MKMapPoint topLeft = MKMapPointForCoordinate(CLLocationCoordinate2DMake(boundingBox.x0, boundingBox.y0));
    MKMapPoint botRight = MKMapPointForCoordinate(CLLocationCoordinate2DMake(boundingBox.xf, boundingBox.yf));
    
    return MKMapRectMake(topLeft.x, botRight.y, fabs(botRight.x - topLeft.x), fabs(botRight.y - topLeft.y));
}

NSInteger FogoZoomScaleToZoomLevel(MKZoomScale scale)
{
    double totalTilesAtMaxZoom = MKMapSizeWorld.width / 256.0;
    NSInteger zoomLevelAtMaxZoom = log2(totalTilesAtMaxZoom);
    NSInteger zoomLevel = MAX(0, zoomLevelAtMaxZoom + floor(log2f(scale) + 0.5));
    
    return zoomLevel;
}

float FogoCellSizeForZoomScale(MKZoomScale zoomScale)
{
    NSInteger zoomLevel = FogoZoomScaleToZoomLevel(zoomScale);
    
    switch (zoomLevel) {
        case 13:
        case 14:
        case 15:
            return 64;
        case 16:
        case 17:
        case 18:
            return 32;
        case 19:
            return 16;
            
        default:
            return 88;
    }
}

@implementation FogoCoordinateQuadTree

- (void)buildTreeWithDBManager:(FogoDataProcess *)fDataProcess {
    @autoreleasepool {
        NSArray *pinData = [NSArray arrayWithArray:[fDataProcess queryAnnotations]];
        int count = pinData.count - 1;
        FogoQuadTreeNodeData *dataArray = malloc(sizeof(FogoQuadTreeNodeData) * count);
        for (NSInteger i = 0; i < count; i++) {
            dataArray[i] = FogoDataFromDic(pinData[i]);
        }
        
        FogoBoundingBox world = FogoBoundingBoxMake(19, -166, 72, -53);
        _root = FogoQuadTreeBuildWithData(dataArray, count, world, 4);
    }
}

- (NSArray *)clusteredAnnotationsWithinMapRect:(MKMapRect)rect withZoomScale:(double)zoomScale {
    double FogoCellSize = FogoCellSizeForZoomScale(zoomScale);
    double scaleFactor = zoomScale / FogoCellSize;
    
    NSInteger minX = floor(MKMapRectGetMinX(rect) * scaleFactor);
    NSInteger maxX = floor(MKMapRectGetMaxX(rect) * scaleFactor);
    NSInteger minY = floor(MKMapRectGetMinY(rect) * scaleFactor);
    NSInteger maxY = floor(MKMapRectGetMaxY(rect) * scaleFactor);
    
    NSMutableArray *clusteredAnnotations = [[NSMutableArray alloc] init];
    for (NSInteger x = minX; x <= maxX; x++) {
        for (NSInteger y = minY; y <= maxY; y++) {
            MKMapRect mapRect = MKMapRectMake(x / scaleFactor, y / scaleFactor, 1.0 / scaleFactor, 1.0 / scaleFactor);
            
            __block double totalX = 0;
            __block double totalY = 0;
            __block int count = 0;
            
            __block double maxCoordinateX = 0;
            __block double minCoordinateX = 10000;
            __block double maxCoordinateY = 0;
            __block double minCoordinateY = 10000;
            
            NSMutableArray *imagePaths = [[NSMutableArray alloc] init];
            NSMutableArray *imageIDs = [[NSMutableArray alloc] init];
            
            FogoQuadTreeGatherDataInRange(self.root, FogoBoundingBoxForMapRect(mapRect), ^(FogoQuadTreeNodeData data) {
                totalX += data.x;
                totalY += data.y;
                count++;
                
                
                maxCoordinateX = (data.x > maxCoordinateX) ? data.x:maxCoordinateX;
                minCoordinateX = (data.x < minCoordinateX) ? data.x:minCoordinateX;
                maxCoordinateY = (fabs(data.y) > fabs(maxCoordinateY)) ? data.y:maxCoordinateY;
                minCoordinateY = (fabs(data.y) < fabs(minCoordinateY)) ? data.y:minCoordinateY;
                
                FogoPhotoInfo imageInfo = *(FogoPhotoInfo *)data.data;
                [imagePaths addObject:[NSString stringWithFormat:@"%s", imageInfo.imagePath]];
                [imageIDs addObject:[NSString stringWithFormat:@"%s", imageInfo.imageID]];
            });
            
            if (count == 1) {
                CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(totalX, totalY);
                FogoClusterAnnotation *annotation = [[FogoClusterAnnotation alloc] initWithCoordinate:coordinate count:count];
//                annotation.title = [imagePaths lastObject];
//                annotation.subtitle = [imageIDs lastObject];
                annotation.imagePaths = imagePaths;
                annotation.accessibilityTraits = UIAccessibilityTraitButton;
                [clusteredAnnotations addObject:annotation];
            }
            
            if (count > 1) {
                CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(totalX / count, totalY / count);
                FogoClusterAnnotation *annotation = [[FogoClusterAnnotation alloc] initWithCoordinate:coordinate count:count];
                annotation.imagePaths = imagePaths;
                
                MKMapPoint mpTopLeft =  MKMapPointForCoordinate(CLLocationCoordinate2DMake(minCoordinateX, minCoordinateY));
                MKMapPoint mpTopRight = MKMapPointForCoordinate(CLLocationCoordinate2DMake(maxCoordinateX, minCoordinateY));
                MKMapPoint mpBottomRight = MKMapPointForCoordinate(CLLocationCoordinate2DMake(maxCoordinateX, maxCoordinateY));
                CLLocationDistance hDist = MKMetersBetweenMapPoints(mpTopLeft, mpTopRight);
                CLLocationDistance vDist = MKMetersBetweenMapPoints(mpTopRight, mpBottomRight);
                
                
//                annotation.subtitle = [NSString stringWithFormat:@"(MaxX: %f, MinX: %f, MaxY: %f, MinY: %f)", maxCoordinateX, minCoordinateX, maxCoordinateY, minCoordinateY];
                annotation.subtitle = [NSString stringWithFormat:@"in area of %.2f km^2", hDist * vDist / (1000*1000)];
                annotation.accessibilityTraits = UIAccessibilityTraitButton;
                [clusteredAnnotations addObject:annotation];
            }
        }
    }
    return [NSArray arrayWithArray:clusteredAnnotations];
}


@end
