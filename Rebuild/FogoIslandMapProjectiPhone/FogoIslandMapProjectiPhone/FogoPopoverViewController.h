//
//  FogoPopoverViewController.h
//  FogoIslandMapProject
//
//  Created by  sy2036 on 2017-04-26.
//  Copyright © 2017 Sipan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FogoPopoverViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIScrollView *imageScroll;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UITableView *detailTable;

@property (weak, nonatomic) IBOutlet UIImageView *selectedImageView;
@property (weak, nonatomic) IBOutlet UITextView *selectedDetailText;


@property (retain, nonatomic) NSArray *imageArr;
@property (retain, nonatomic) NSDictionary *detailDic;

@end
