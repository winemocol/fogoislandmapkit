//
//  FogoDataProcess.m
//  FogoIslandMapProject
//
//  Created by Sipan Ye on 2017/4/13.
//  Copyright © 2017年 Sipan. All rights reserved.
//

#import "FogoDataProcess.h"
#import "FogoIslandDBManager.h"
#import "FogoPolygon.h"

#define FOGO_NW_LAT 49.78354343767712
#define FOGO_NW_LNG -54.32582266065412
#define FOGO_SW_LAT 49.531878309117076
#define FOGO_SW_LNG -54.33958794258664
#define FOGO_NE_LAT 49.7745818637998
#define FOGO_NE_LNG -53.96249945939786
#define FOGO_SE_LAT 49.52299562709053
#define FOGO_SE_LNG -53.97812839986856

//  Minimum column and row count of cells in a map.
#define GRID_COL_COUNT 20
#define GRID_ROW_COUNT 15

@interface FogoDataProcess () {
    //  Original four corner coordinates of the given map.
    MKMapPoint topLeftPoint;
    MKMapPoint topRightPoint;
    MKMapPoint bottomLeftPoint;
    MKMapPoint bottomRightPoint;
    
    float rotatedCos;
    float rotatedSin;
    
    //  Original data width and height in the coordinate system.
    double dataOffset;
    double dataOffsetRow;
    
    FogoIslandDBManager *fDBManager;
}

@end

@implementation FogoDataProcess

- (instancetype)initWithRotateAngleCos:(float)rotatedC andSin:(float)rotatedS {
    self = [super init];
    if (self) {
        rotatedCos = rotatedC;
        rotatedSin = rotatedS;
        
        double angle = -acos(rotatedC);
        
        topLeftPoint = MKMapPointForCoordinate(CLLocationCoordinate2DMake(FOGO_NW_LAT, FOGO_NW_LNG));
        topRightPoint = MKMapPointForCoordinate(CLLocationCoordinate2DMake(FOGO_NE_LAT, FOGO_NE_LNG));
        bottomLeftPoint = MKMapPointForCoordinate(CLLocationCoordinate2DMake(FOGO_SW_LAT, FOGO_SW_LNG));
        bottomRightPoint = MKMapPointForCoordinate(CLLocationCoordinate2DMake(FOGO_SE_LAT, FOGO_SE_LNG));
        /*
         *  (x1，y1)为要转的点，（x2,y2）为中心点，如果是顺时针角度为-，
         *  x=(x1-x2)cosθ-(y1-y2)sinθ+x2
         *  y=(y1-y2)cosθ+(x1-x2)sinθ+y2
         */
        topRightPoint = [self mapPoint:topRightPoint rotationByAngleCos:cos(angle) andSin:sin(angle) atCenterPoint:topLeftPoint];
        bottomLeftPoint = [self mapPoint:bottomLeftPoint rotationByAngleCos:cos(angle) andSin:sin(angle) atCenterPoint:topLeftPoint];
        bottomRightPoint = [self mapPoint:bottomRightPoint rotationByAngleCos:cos(angle) andSin:sin(angle) atCenterPoint:topLeftPoint];
        
        dataOffset = (topLeftPoint.x - topRightPoint.x) / 2619;
        dataOffsetRow = (topLeftPoint.y - bottomLeftPoint.y)/2801;
        
        fDBManager = [[FogoIslandDBManager alloc] initWithOption:2];
    }
    return self;
}

- (MKMapPoint)mapPoint:(MKMapPoint)oPoint rotationByAngleCos:(double)aCos andSin:(double)aSin atCenterPoint:(MKMapPoint)centerPoint {
    MKMapPoint rotatedPoint = MKMapPointMake((oPoint.x-centerPoint.x)*aCos - (oPoint.y-centerPoint.y)*aSin + centerPoint.x, (oPoint.y-centerPoint.y)*aCos + (oPoint.x-centerPoint.x)*aSin + centerPoint.y);
    return rotatedPoint;
}

- (void)getOverlayDataFromZoomLevel:(int)zoomLevel withXStartIndex:(int)xSIndex andYStartIndex:(int)ySIndex andXEndCount:(int)xECount andYEndCount:(int)yECount {
    self.fDataArray = [NSArray arrayWithArray:[fDBManager queryFogoIlandDataMatrixByZoomLevel:zoomLevel withXStartIndex:xSIndex andYStartIndex:ySIndex andXEndCount:xECount andYEndCount:yECount]];
}

- (NSArray *)refreshDataWithMapView:(MKMapView *)mapView {
    //  If the FogoIsland is in view, the value will be YES, and draw the map.
    BOOL isDraw = YES;
    //  Get the bound of the MapView.
    CLLocationCoordinate2D topLeftMapCoord = [mapView convertPoint:CGPointMake(0, 0) toCoordinateFromView:mapView];
    MKMapPoint topLeftMapPoint = MKMapPointForCoordinate(topLeftMapCoord);
    topLeftMapPoint = [self mapPoint:topLeftMapPoint rotationByAngleCos:cos(-acos(rotatedCos)) andSin:sin(-acos(rotatedCos)) atCenterPoint:topLeftPoint];
    CLLocationCoordinate2D bottomRightMapCoord = [mapView convertPoint:CGPointMake(mapView.frame.size.width, mapView.frame.size.height) toCoordinateFromView:mapView];
    MKMapPoint bottomRightMapPoint = MKMapPointForCoordinate(bottomRightMapCoord);
    bottomRightMapPoint = [self mapPoint:bottomRightMapPoint rotationByAngleCos:cos(-acos(rotatedCos)) andSin:sin(-acos(rotatedCos)) atCenterPoint:topLeftPoint];
    
    
    //  Divide the current MapRect to at leaset 20*15 grids.
    double xOffset = fabs(topLeftMapPoint.x - bottomRightMapPoint.x)/GRID_COL_COUNT;
    //  Obtain the zoom scale.
    int zoomScale = fabs(xOffset / dataOffset);
    //  Get zoom level and cell count.
    NSArray *zoomInfoArray = [self getZoomLevelWithScale:zoomScale];
    int zoomLevel = [[zoomInfoArray objectAtIndex:0] intValue];
    
    //  Obtain current level's data cell width and height in the map.
    NSArray *dataSize = [fDBManager querySizeofDataBYZoomLevel:zoomLevel];
    int dataColCount = [[dataSize objectAtIndex:0] intValue];
    int dataRowCount = [[dataSize objectAtIndex:1] intValue];
    double currentDataWidth = fabs(topLeftPoint.x - bottomRightPoint.x)/dataColCount;
    double currentDataHeight = fabs(topLeftPoint.y - bottomRightPoint.y)/dataRowCount;
    
    CLLocationCoordinate2D topRightMapCoord = [mapView convertPoint:CGPointMake(mapView.frame.size.width, 0) toCoordinateFromView:mapView];
    MKMapPoint topRightMapPoint = MKMapPointForCoordinate(topRightMapCoord);
    int updateYIndex = [self obtainPointYIndexIngivenMap:topRightMapPoint andRotationAngle:acos(rotatedCos) andCenterPoint:topLeftPoint andBlockHeight:currentDataHeight];
    int updateXIndex = [self obtainPointXIndexIngivenMap:topLeftMapPoint andRotationAngle:acos(rotatedCos) andCenterPoint:topLeftPoint andBlockHeight:currentDataWidth];
    int updateXEndIndex = [self obtainPointXIndexIngivenMap:bottomRightMapPoint andRotationAngle:acos(rotatedCos) andCenterPoint:topLeftPoint andBlockHeight:currentDataWidth];
    updateXEndIndex = (bottomRightMapPoint.x - topLeftMapPoint.x)/currentDataWidth;
    //  Initial the maximum column and row count of cells in current zoom level map.
    int colCount = (int)fabs(topLeftMapPoint.x - bottomRightMapPoint.x)/currentDataWidth + 1;
    int rowCount = (int)fabs(topLeftMapPoint.y - bottomRightMapPoint.y)/currentDataHeight + 1;
    
    NSLog(@"%d, %d", colCount, rowCount);
    
    int xStartIndex = 0;
    int yStartIndex = 0;
    int xEndCount = colCount;
    int yEndCount = rowCount;
    
    //  Current drawing overlay's start point.
    MKMapPoint overlayStartPoint;
    
    //  Difference or offset between the given map coordiantes and the view rect of the map view.
    double xLeftBoundaryDifference = topLeftMapPoint.x - topLeftPoint.x;
    double yLeftBoundaryDifference = topLeftMapPoint.y - topLeftPoint.y;
    //    double xRightBoundaryDifference = bottomRightMapPoint.x - bottomRightPoint.x;
    double yRightBoundaryDifference = bottomRightMapPoint.y - bottomRightPoint.y;
    NSLog(@"Zoom Level: %d", zoomLevel);
    
    if (8 == zoomLevel) {
        //  Solved
        xStartIndex = 0;
        yStartIndex = 0;
        xEndCount = 21;
        yEndCount = 22;
        colCount = 21;
        rowCount = 22;
    } else if (xLeftBoundaryDifference <= 0) {
        if (yLeftBoundaryDifference <= 0) {
            //  While the top LEFT corner of the given FogoIsland map is in the range of the visible mapview.
            //  Use the bottom right of the map view - the top left corner of the given map.
            //  Obtain one more data in the column and row in case of gaps and empty after rotating.
            //  Solved
            colCount = (int)fabs(bottomRightMapPoint.x - topLeftPoint.x) / currentDataWidth + 1;
            rowCount = (int)fabs(bottomRightMapPoint.y - topLeftPoint.y) / currentDataHeight + 1;
            xEndCount = colCount;
            yEndCount = rowCount;
        } else if (yRightBoundaryDifference <= 0) {
            //  While only the top left corner x is in the range of the visible mapview, the top left corner y is above the visible mapview.
            //  Solved
            yStartIndex = (int)fabs(yLeftBoundaryDifference / currentDataHeight);
            colCount = (int)fabs(bottomRightMapPoint.x - topLeftPoint.x) / currentDataWidth + 1;
            rowCount += 2;
            xEndCount = colCount;
            yEndCount = rowCount;
        } else {
            //  While the bottom left corner of the given FogoIsland map is in the range of the visible mapview.
            //  Solved
            yStartIndex = (int)fabs(yLeftBoundaryDifference / currentDataHeight);
            rowCount = dataRowCount - yStartIndex - 1;
            yEndCount = rowCount;
            //  If the bottom left corner is out of the range, which it is not shown in the current map view,
            //  the overlay will not be drawn.
            if (rowCount <= 0)
                isDraw = NO;
        }
    } else {
        if (yLeftBoundaryDifference <= 0) {
            //  While the top right corner of the given FogoIsland map is in the range of the visible mapview.
            //  Solved
            xStartIndex = (int)fabs(xLeftBoundaryDifference / currentDataWidth);
            yEndCount = rowCount;
            xEndCount = colCount;
            //  If the top right corner is out of the range, which it is not shown in the current map view,
            //  the overlay will not be drawn.
            if (colCount <= 0)
                isDraw = NO;
        } else if (yRightBoundaryDifference <= 0) {
            //  Solved
            if (bottomRightMapPoint.x <= bottomRightPoint.x) {
                xStartIndex = (int)fabs(xLeftBoundaryDifference / currentDataWidth);
                yStartIndex = (int)fabs(yLeftBoundaryDifference / currentDataHeight);
                NSLog(@"%d, %d", colCount, rowCount);
                rowCount += 2;
                colCount += 4;
                yEndCount = rowCount;
                xEndCount = colCount;
            } else if (fabs(bottomRightPoint.x-bottomRightMapPoint.x) < fabs(topLeftMapPoint.x - bottomRightMapPoint.x)) {
                xStartIndex = (int)fabs(xLeftBoundaryDifference / currentDataWidth);
                yStartIndex = (int)fabs(yLeftBoundaryDifference / currentDataHeight);
                colCount = (int)fabs(bottomRightPoint.x-topLeftMapPoint.x)/currentDataWidth + 2;
                yEndCount = rowCount;
                xEndCount = colCount;
            } else
                isDraw = NO;
        } else {
            //  Solved
            NSLog(@"666");
            xStartIndex = (int)fabs(xLeftBoundaryDifference / currentDataWidth);
            yStartIndex = (int)fabs(yLeftBoundaryDifference / currentDataHeight);
            
            colCount = (int)fabs(bottomRightPoint.x-topLeftMapPoint.x)/currentDataWidth + 1;
            rowCount = (int)fabs(bottomRightPoint.y-topLeftMapPoint.y)/currentDataHeight + 1;
            yEndCount = rowCount;
            xEndCount = colCount;
        }
        if (updateYIndex < yStartIndex) {
            yStartIndex = updateYIndex;
        }
        if (updateXIndex < xStartIndex) {
            xStartIndex = updateXIndex;
        }
        if (updateXEndIndex < dataColCount) {
            colCount = updateXEndIndex;
        }
    }
    
    //  Due to the data merging, current data comes from the 2*2 data of previous level.
    //  Thus, we need to move the start point to the center.
//    overlayStartPoint = MKMapPointMake(topLeftPoint.x + (xStartIndex + 0.5)*currentDataWidth, topLeftPoint.y + (yStartIndex + 0.5)*currentDataHeight);
    overlayStartPoint = MKMapPointMake(topLeftPoint.x + (xStartIndex+0.5)*currentDataWidth, topLeftPoint.y + (yStartIndex)*currentDataHeight);
    NSLog(@"XSIndex: %d, YSIndex: %d", xStartIndex, yStartIndex);
    NSLog(@"%d, %d", colCount, rowCount);
    MKMapSize cellSize = MKMapSizeMake(currentDataWidth, currentDataHeight);
    
    [self getOverlayDataFromZoomLevel:zoomLevel withXStartIndex:xStartIndex andYStartIndex:yStartIndex andXEndCount:colCount andYEndCount:rowCount];
    if (isDraw) {
        return [self buildFogoDataOverleyArrayWith:overlayStartPoint andOverySize:cellSize andRowCount:rowCount andColCount:colCount andFDataArray:_fDataArray];
    } else
        return [NSArray array];
}

/**
 Get the zoom level by the scale
 
 Index 0: Zoom Level.
 Index 1: Cell Size.
 The zoom level = 1 means that the cell is the original point in the FogoIsland dataset. 2 means that current cell is combined by 2*2 of the original point.
 
 @param scale zoom scale
 @return Zoom level array.
 */
- (NSArray *)getZoomLevelWithScale:(int)scale {
    NSMutableArray *zoomArray = [[NSMutableArray alloc] init];
    int zoomLevel = 1;
    [zoomArray addObject:@"1"];
    [zoomArray addObject:@"1"];
    if (scale > 1 && scale <= 2) {
        zoomArray[0] = @"2";
        zoomArray[1] = @"2";
        zoomLevel = 2;
    } else if (scale > 2 && scale <= 4) {
        zoomArray[0] = @"3";
        zoomArray[1] = @"4";
        zoomLevel = 3;
    } else if (scale > 4 && scale <= 6) {
        zoomArray[0] = @"4";
        zoomArray[1] = @"8";
        zoomLevel = 4;
    } else if (scale > 6 && scale <= 12) {
        zoomArray[0] = @"5";
        zoomArray[1] = @"16";
        zoomLevel = 5;
    } else if (scale > 12 && scale <= 40) {
        zoomArray[0] = @"6";
        zoomArray[1] = @"32";
        zoomLevel = 6;
    } else if (scale > 40 && scale <= 96) {
        zoomArray[0] = @"7";
        zoomArray[1] = @"64";
        zoomLevel = 7;
    } else if (scale > 96) {
        zoomArray[0] = @"8";
        zoomArray[1] = @"128";
        zoomLevel = 8;
    }
    return [NSArray arrayWithArray:zoomArray];
}


/**
 Due to the rotation,
 First, we consider top right point of the visible map as the point in the original given map which is parallel with the latitude. Thus, we need to rotate the top right point back to the point in the "rotated back" given map. 
 Second, obtain the y index of the rotated top right point.
 Third, update the y index in the later calculation.

 @param targetPoint top right point of the visible map.
 */
- (int)obtainPointYIndexIngivenMap:(MKMapPoint)targetPoint andRotationAngle:(float)angle andCenterPoint:(MKMapPoint)centerPoint andBlockHeight:(double)blockHeight {
    MKMapPoint rotatedBackPoint = [self mapPoint:targetPoint rotationByAngleCos:cos(-angle) andSin:sin(-angle) atCenterPoint:centerPoint];
    int updateYIndex = (rotatedBackPoint.y - centerPoint.y)/blockHeight;
    if (updateYIndex > 0)
        return updateYIndex;
    else
        return 0;
}

- (int)obtainPointXIndexIngivenMap:(MKMapPoint)targetPoint andRotationAngle:(float)angle andCenterPoint:(MKMapPoint)centerPoint andBlockHeight:(double)blockHeight {
    MKMapPoint rotatedBackPoint = [self mapPoint:targetPoint rotationByAngleCos:cos(-angle) andSin:sin(-angle) atCenterPoint:centerPoint];
    int updateXIndex = (rotatedBackPoint.x - centerPoint.x)/blockHeight;
    if (updateXIndex > 0)
        return updateXIndex;
    else
        return 0;
}


/**
 Build Overlays for FogoIsland data
 
 @param overlayStartPoint The point of the left top start overlay.
 @param cellSize Size of the overlay
 @param rowCount Number of overlays on a single line
 @param colCount Number of overlays on a column
 @param fDataArray Array of FogoIslnd data
 @return Overlays array
 */
- (NSArray *)buildFogoDataOverleyArrayWith:(MKMapPoint)overlayStartPoint andOverySize:(MKMapSize)cellSize andRowCount:(int)rowCount andColCount:(int)colCount andFDataArray:(NSArray *)fDataArray {
    NSMutableArray *fOverlayArray = [[NSMutableArray alloc] init];
    MKMapPoint topleft = overlayStartPoint;
    MKMapRect boundRect =  MKMapRectMake(topleft.x, topleft.y, cellSize.width, cellSize.height);
    MKMapPoint topRight = MKMapPointMake(MKMapRectGetMaxX(boundRect), topleft.y);
    MKMapPoint bottomleft = MKMapPointMake(topleft.x, MKMapRectGetMaxY(boundRect));
    MKMapPoint bottomRight = MKMapPointMake(topRight.x, bottomleft.y);
    
    //  This is the start point of each row.
    //  Because the application draw the cells row by row.
    for (NSInteger i = 0; i < rowCount; i++) {
        for (NSInteger j = 0; j < colCount; j++) {
            double dataValue = [[fDataArray objectAtIndex:j + colCount*i] doubleValue];
            
            topleft = MKMapPointMake(overlayStartPoint.x + cellSize.width * j, overlayStartPoint.y + cellSize.height * i);
            topRight = MKMapPointMake(overlayStartPoint.x + cellSize.width * (j+1), overlayStartPoint.y + cellSize.height * i);
            bottomRight = MKMapPointMake(overlayStartPoint.x + cellSize.width * (j+1), overlayStartPoint.y + cellSize.height * (i+1));
            bottomleft = MKMapPointMake(overlayStartPoint.x + cellSize.width * j, overlayStartPoint.y + cellSize.height * (i+1));
            
            MKMapPoint mapPoints[5];
            /*
             *  (x1，y1)为要转的点，（x2,y2）为中心点，如果是顺时针角度为-，
             *  x=(x1-x2)cosθ-(y1-y2)sinθ+x2
             *  y=(y1-y2)cosθ+(x1-x2)sinθ+y2
             */
            mapPoints[0] = [self mapPoint:topleft rotationByAngleCos:rotatedCos andSin:rotatedSin atCenterPoint:topLeftPoint];
            mapPoints[1] = [self mapPoint:topRight rotationByAngleCos:rotatedCos andSin:rotatedSin atCenterPoint:topLeftPoint];
            mapPoints[2] = [self mapPoint:bottomRight rotationByAngleCos:rotatedCos andSin:rotatedSin atCenterPoint:topLeftPoint];
            mapPoints[3] = [self mapPoint:bottomleft rotationByAngleCos:rotatedCos andSin:rotatedSin atCenterPoint:topLeftPoint];
            mapPoints[4] = mapPoints[0];
            
            FogoPolygon *fOverly = [FogoPolygon polygonWithPoints:mapPoints count:5];
            fOverly.possValue = dataValue;
            [fOverlayArray addObject:fOverly];
            boundRect =  MKMapRectMake(mapPoints[0].x, mapPoints[0].y, cellSize.width, cellSize.height);
        }
    }
    return [NSArray arrayWithArray:fOverlayArray];
}


#pragma Annotations
- (NSArray *)queryAnnotations {
    return [fDBManager queryAllPostImages];
}

- (NSArray *)queryAnnotationDetailWithID:(int)fID {
    return [fDBManager queryImageReviewByID:fID];
}


- (void)inserNewAnnotationWithImagePath:(NSString *)imagePath andCoordinate:(CLLocationCoordinate2D)fCoordinate {
    [fDBManager insertPostImagesWithImagePath:imagePath andLat:fCoordinate.latitude andLng:fCoordinate.longitude];
}


@end
