//
//  FogoClustersAnnotationView.h
//  FogoIslandMapProject
//
//  Created by Sipan Ye on 2017/4/28.
//  Copyright © 2017年 Sipan. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface FogoClustersAnnotationView : MKAnnotationView

- (void)setClusterText:(NSString *)text andImage:(NSString *)imagePath;

@end
