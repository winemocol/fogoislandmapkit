//
//  FogoComposePopoverViewController.m
//  FogoIslandMapProject
//
//  Created by  sy2036 on 2017-05-03.
//  Copyright © 2017 Sipan. All rights reserved.
//

#import "FogoComposePopoverViewController.h"

@interface FogoComposePopoverViewController () <UITextFieldDelegate>

@end

@implementation FogoComposePopoverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.preferredContentSize = CGSizeMake(360, 300);
    [_imageView setImage:[UIImage imageWithContentsOfFile:_imagePath]];
}

- (void)setImagePath:(NSString *)imagePath {
    _imagePath = imagePath;
    [_imageView setImage:[UIImage imageWithContentsOfFile:_imagePath]];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    NSLog(@"C;l");
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
