import sys
import csv

def getSource(fileAddr):
    return file(fileAddr, 'rb')

def main(argv):
    reader = csv.reader(getSource(argv[1]), delimiter=',')
    #print reader
    result = ''
    #count = 0
    rlists = []
    for i, rows in enumerate(reader):
        if i > 0:
            # store the temp value
            temp = 0.0
            clists = []
            for j, columns in enumerate(rows):
                if j > 0:
                    if columns == 'NA' or columns == '0.5':
                        columns = str(0)
                    if int(argv[2]) == 1:
                        temp = float(columns)
                    if j%int(argv[2]) == 0:
                        temp = temp / int(argv[2])
                        result += str(temp)
                        result += ' '
                        clists.append(str(temp))
                        #clists.append(' ')
                    else:
                        temp += float(columns)
            result += '\n'
            rlists.append(clists)
    fileName = 'FogoIslandMatrix@' + argv[3] + 'x.csv'
    #fileName = 'test.txt'
    fl = open(fileName, 'w')
    print rlists
    for m, rlist in enumerate(rlists):
        if m%int(argv[2]) == 0:
            temp = 0.0
            for n, data in enumerate(rlist):
                if int(argv[2]) == 1:
                    temp = float(rlists[m][n])
                    #print temp
                else:
                    for z in range(0, int(argv[2])-1):
                        temp += float(rlists[m-z][n])
                        temp = temp / (int(argv[2]))
                fl.write(str(temp))
                fl.write(' ')
            fl.write('\n')
    fl.close()

if __name__ == '__main__':
    main(sys.argv)
