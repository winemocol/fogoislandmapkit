#####################################################################################
#   Please note that this is a new version of the parsing FogoIsland Data program.
#   Input:
#   argv[1] == "The path of the data file."
#   argv[2] == "The cell size which means how many original data will be merged
#               for both horizon and vertical direction."
#   argv[3] == "The level of the zoom."
#####################################################################################

import sys
import csv
#   Get the file by the address.
def getSource(fileAddr):
    return file(fileAddr, 'rb')
#   Get the width(column count) and height(row count) of the data.
def getWidthAndHeightOfData(fileData):
    width = 0
    height = 0
    for rows in fileData:
        height += 1
        width = max(width, len(rows))
    # The extra "-1" is due to the "i>0" which we ignore the first row.
    return width-1, height-1

def main(argv):
    reader = csv.reader(getSource(argv[1]), delimiter=',')
    result = ''
    #   Due to the first row and first column is the name of the data,
    #   Thus, we jump this two types.
    width, height = getWidthAndHeightOfData(csv.reader(getSource(argv[1]), delimiter=','))
    cellSize = int(argv[2])
    colCount = width/cellSize
    rowCount = height/cellSize
    #print height
    if colCount%cellSize != 0:
        colCount += 1
    if rowCount%cellSize != 0:
        rowCount += 1

    rlists = []
    for i, rows in enumerate(reader):
        if i > 0:
            clists = []
            for x in range(0, colCount):
                temp = 0
                #   The average will only depends on the none zero data value.
                noneZeroCount = 0
                for y in range(1, cellSize+1):
                    if (x+1)*cellSize >= width:
                        if rows[width-1 - y] == 'NA' or rows[width-1 - y] == '0.5':
                            rows[width-1 - y] = str(0)
                        if float(rows[width-1 - y]) != 0:
                            noneZeroCount += 1
                        temp += float(rows[width-1 - y])
                    else:
                        if rows[y+x*cellSize] == 'NA' or rows[y+x*cellSize] == '0.5':
                            rows[y+x*cellSize] = str(0)
                        if float(rows[y+x*cellSize]) != 0:
                            noneZeroCount += 1
                        temp += float(rows[y+x*cellSize])
                if noneZeroCount == 0:
                    noneZeroCount = 1
                temp /= noneZeroCount
                clists.append(str(temp))
            rlists.append(clists)
    print colCount
    fileName = 'FogoIslandMatrixNew@' + argv[3] + 'x.csv'
    fl = open(fileName, 'w')

#    for x in range(0, colCount):
#        temp = 0.0
#        for y in range(0, rowCount):
#            for j in range(9, cellSize):
#                rlists[]


    for j in range(0, rowCount):
        for x in range(0, colCount):
            temp = 0
            #   The average will only depends on the none zero data value.
            noneZeroCount = 0
            for y in range(0, cellSize):
                if (j+1)*cellSize >= height:
                    if float(rlists[height-1 - y][x]) != 0:
                        noneZeroCount += 1
                    temp += float(rlists[height-1 - y][x])
                else:
                    if float(rlists[y+j*cellSize][x]) != 0:
                        noneZeroCount += 1
                    temp += float(rlists[y+j*cellSize][x])
            if noneZeroCount == 0:
                noneZeroCount = 1
            temp /= noneZeroCount
            fl.write(str(temp))
            fl.write(' ')
        fl.write('\n')
    fl.close()


if __name__ == '__main__':
    main(sys.argv)
